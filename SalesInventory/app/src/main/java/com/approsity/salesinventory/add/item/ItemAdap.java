package com.approsity.salesinventory.add.item;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.add.models.Product;
import com.approsity.salesinventory.add.models.SubCategory;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.user.vendor.VendorDetailViewPager;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.approsity.salesinventory.utils.FreeActivity;
import com.viethoa.RecyclerViewFastScroller;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

public class ItemAdap extends RecyclerView.Adapter<ItemAdap.ViewHolder> implements RecyclerViewFastScroller.BubbleTextGetter, Filterable {

    private static final String TAG = ItemAdap.class.getSimpleName();

    private List<ItemModel> mItems,items2;

    Activity act;

    private int position;

    CommonObjects.RecyclerViewClickListener itemListener;

    private CustomFilter mFilter;

    ItemDialog itemDialog;

    RecyclerView rc;

    public ItemAdap(Activity act, List<ItemModel> items, CommonObjects.RecyclerViewClickListener itemListener, RecyclerView rc) {
        mItems = items;
        this.act = act;
        this.rc = rc;
        this.itemListener = itemListener;

        mFilter = new CustomFilter();
        this.items2 = new ArrayList<ItemModel>();
        this.items2.addAll(mItems);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int view_type) {

        View v = null;

        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.frag_itemlist_row, viewGroup, false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int pos) {

        final ItemModel aObj = mItems.get(pos);

        String product="";
        try {
            product = "<i>Product :</i>" + " " + aObj.pList.get(0).name;
        }catch(Exception e){
            e.printStackTrace();
        }
        viewHolder.text_product.setText(Html.fromHtml(product), TextView.BufferType.SPANNABLE);

        viewHolder.text_name.setText(aObj.name);
        viewHolder.text_alias.setText(aObj.alias);
        viewHolder.text_desc.setText(aObj.desc);

        String type ="<i>Type :</i>"+" "+ aObj.type;
        String size ="<i>Size :</i>"+" "+ aObj.size;
        String qty ="<i>Qty :</i>"+" "+ aObj.quantity;
        String cost ="<i>Cost :</i>"+" "+ aObj.costperunit;
        String totalPrice ="<i>Total :</i>"+" "+ aObj.totalprice;
        String salePrice ="<i>Sale : </i>"+" "+ aObj.saleprice;

        viewHolder.tv_type.setText(Html.fromHtml(type), TextView.BufferType.SPANNABLE);
        viewHolder.tv_size.setText(Html.fromHtml(size), TextView.BufferType.SPANNABLE);
        viewHolder.tv_qty.setText(Html.fromHtml(qty), TextView.BufferType.SPANNABLE);
        viewHolder.tv_cost.setText(Html.fromHtml(cost), TextView.BufferType.SPANNABLE);
        viewHolder.tv_totalprice.setText(Html.fromHtml(totalPrice), TextView.BufferType.SPANNABLE);
        viewHolder.tv_saleprice.setText(Html.fromHtml(salePrice), TextView.BufferType.SPANNABLE);

        viewHolder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(act, R.style.MyDialogTheme);
                builder1.setMessage("Do you want to delete category ?");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                deleteRecord(aObj.id);
                            }
                        });
                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        viewHolder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getEditDialog(aObj);
            }
        });

        viewHolder.btn_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(act,ItemHistoryActivity.class);
                i.putExtra("vid",aObj.id);
                act.startActivity(i);
            }
        });

        viewHolder.btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(act, FreeActivity.class);
                i.putExtra("frag", CommonObjects.OpenFragments.barcode);
                i.putExtra("itemid", aObj.id);
                act.startActivity(i);
            }
        });
    }


    @Override
    public int getItemViewType(int position) {

       return 0;
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String getTextToShowInBubble(int pos) {

        if (pos < 0 || pos >= mItems.size())
            return null;

        String name = mItems.get(pos).name;
        if (name == null || name.length() < 1)
            return null;

        return mItems.get(pos).name.substring(0, 1);
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView text_name,text_alias,text_desc,tv_type,tv_size,tv_qty,tv_cost,tv_totalprice,tv_saleprice,text_product;
        TextView btn_delete,btn_edit,btn_info,btn_print;

        View rootView;

        ViewHolder(View v) {
            super(v);

            text_name = (TextView) v.findViewById(R.id.text_name);
            text_alias = (TextView) v.findViewById(R.id.text_alias);
            text_desc = (TextView) v.findViewById(R.id.text_desc);
            tv_type = (TextView) v.findViewById(R.id.tv_type);

            tv_size = (TextView) v.findViewById(R.id.tv_size);
            tv_qty = (TextView) v.findViewById(R.id.tv_qty);
            tv_cost = (TextView) v.findViewById(R.id.tv_cost);
            tv_totalprice = (TextView) v.findViewById(R.id.tv_totalprice);
            tv_saleprice = (TextView) v.findViewById(R.id.tv_saleprice);
            text_product = (TextView) v.findViewById(R.id.text_product);

            btn_delete = (TextView) v.findViewById(R.id.btn_delete);
            btn_edit = (TextView) v.findViewById(R.id.btn_edit);
            btn_print = (TextView) v.findViewById(R.id.btn_print);

            btn_info = (TextView) v.findViewById(R.id.btn_info);

            text_name.setOnClickListener(this);
            text_alias.setOnClickListener(this);
            text_desc.setOnClickListener(this);


            v.setOnClickListener(this);


            rootView = v;
        }

        @Override
        public void onClick(View v) {

            itemListener.recyclerViewListClicked(v, getLayoutPosition(),act);

           /* selectedPosition=getLayoutPosition();
            notifyDataSetChanged();*/

        }
    }


    public ItemModel getItem(int position) {
        return mItems.get(position);
    }

    public void addData(ItemModel newModelData, int position) {
        mItems.add(position, newModelData);
        notifyItemInserted(position);
    }

    public void removeData(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    public void clearData() {
        Log.v(TAG, "clearData()");
        int size = this.mItems.size();

        int index = 0;
        if (size > index) {
            for (int i = index; i < size; i++) {
                this.mItems.remove(index);
            }

            this.notifyItemRangeRemoved(index, size - 1);
        }

    }

    public class CustomFilter extends Filter {


        private CustomFilter() {
            super();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            List<ItemModel> filteredList = new ArrayList<ItemModel>();
            FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                mItems.clear();
                mItems.addAll(items2);
                filteredList.addAll(mItems);
            } else {

                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (ItemModel mWords : items2) {
                    if (mWords.alias.toLowerCase().startsWith(filterPattern)) {
                        filteredList.add(mWords);
                    }
                }

                mItems.clear();
                mItems = filteredList;
            }

            results.values = filteredList;
            results.count = filteredList.size();


            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            notifyDataSetChanged();
        }
    }

    public void deleteRecord(int id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        ItemModel c = realm.where(ItemModel.class).equalTo("id", id).findFirst();

        int i = Integer.parseInt(System.currentTimeMillis()/1000+"");

        RealmList<ItemModel> il = new RealmList<>();
        il.add(c);

        RealmList<Category> cl = new RealmList<>();
        cl.addAll(c.catList);

        RealmList<SubCategory> scl = new RealmList<>();
        scl.addAll(c.subcatList);

        RealmList<Product> pl = new RealmList<>();
        pl.addAll(c.pList);

        Transactions tObj = new Transactions(i,cl,scl,pl,null,il,null, CommonActions.currentDateTime(),"Item deleted",c.name);
        realm.insertOrUpdate(tObj);

        if(c != null)
            c.deleteFromRealm();
        realm.commitTransaction();
        realm.close();
        notifyDataSetChanged();

        //removeData(position);

        if(c != null)
            Snackbar.make(rc,"Record Deleted Successfully!",Snackbar.LENGTH_SHORT).show();
    }

    public void getEditDialog(final ItemModel o) {

        itemDialog = new ItemDialog().newInstance(act,o);
        FragmentManager manager = ((AppCompatActivity)act).getSupportFragmentManager();
        itemDialog.show(manager,"edit");
    }

}