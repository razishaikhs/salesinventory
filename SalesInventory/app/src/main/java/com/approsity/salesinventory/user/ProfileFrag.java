package com.approsity.salesinventory.user;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.approsity.salesinventory.R;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.File;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;


public class ProfileFrag extends Fragment {

    View mView;

    RelativeLayout ll_profile;
    LinearLayout ll_photo;
    ImageView imgv_profile;
    ImageButton img_profile_drop;

    File file;
    Uri uri;
    Intent CamIntent, GalIntent, CropIntent ;
    public  static final int RequestPermissionCode  = 1;
    String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    int PERMISSION  = 1;
    int dialogChoice = 0;

    EditText edit_name,edit_email,edit_shopname, edit_address, edit_phone, edit_fax;

    Animation shake;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_profile, container, false);

        shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);

        ll_photo = (LinearLayout) mView.findViewById(R.id.ll_photo);
        ll_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureImage();
            }
        });

        imgv_profile = (ImageView) mView.findViewById(R.id.imgv_profile);
        imgv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureImage();
            }
        });

        ll_profile = (RelativeLayout) mView.findViewById(R.id.ll_profile);
        ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureImage();
            }
        });

        img_profile_drop = (ImageButton) mView.findViewById(R.id.img_profile_drop);
        img_profile_drop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captureImage();
            }
        });

        TextView link_to_forgot = (TextView) mView.findViewById(R.id.link_to_forgot);
        link_to_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Reset Password");
                builder.setMessage("Please reconfirm your email address !");

                final EditText input = new EditText(getActivity());
                input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                input.setGravity(Gravity.CENTER);
                builder.setView(input);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        hideKeyboard();

                        if(TextUtils.isEmpty(input.getText().toString()))
                        {
                            input.requestFocus();
                            input.setError("Please enter the email");
                            //input.startAnimation(shake);
                        }
                        else if(!CommonActions.isEmailValid(input.getText().toString()))
                        {
                            input.requestFocus();
                            input.setError("Please enter correct email");
                           // input.startAnimation(shake);
                        }
                        else{

                            dialog.cancel();

                            resetPassword(input.getText().toString());
                        }
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            }
        });


        edit_name = (EditText) mView.findViewById(R.id.edit_name);
        edit_email = (EditText) mView.findViewById(R.id.edit_email);
        edit_shopname = (EditText) mView.findViewById(R.id.edit_shopname);
        edit_address = (EditText) mView.findViewById(R.id.edit_address);
        edit_phone = (EditText) mView.findViewById(R.id.edit_phone);
        edit_fax = (EditText) mView.findViewById(R.id.edit_fax);

        edit_name.setText(CommonObjects.uObj.userName);
        edit_email.setText(CommonObjects.uObj.email);
        edit_shopname.setText(CommonObjects.uObj.shopName);
        edit_address.setText(CommonObjects.uObj.address);
        edit_phone.setText(CommonObjects.uObj.telephone);
        edit_fax.setText(CommonObjects.uObj.fax);

        Button btn_update = (Button) mView.findViewById(R.id.btn_update);
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hideKeyboard();

                if(TextUtils.isEmpty(edit_name.getText().toString()))
                {
                    edit_name.requestFocus();
                    edit_name.setError("Please enter the name");
                    edit_name.startAnimation(shake);
                }
                else{

                    updateProfile();
                }
            }
        });

        return mView;
    }


    public void captureImage(){
        String[] s = {"Camera", "Gallery"};

        new AlertDialog.Builder(getActivity())
                .setSingleChoiceItems(s, 0, new DialogInterface.OnClickListener () {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();

                        dialogChoice = which;

                        if(which == 0)
                        {
                            if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                                requestPermissions(PERMISSIONS, PERMISSION);
                            }
                            else
                                ClickImageFromCamera() ;
                        }
                        else
                        {
                            if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                                requestPermissions(PERMISSIONS, PERMISSION);
                            }
                            else
                                GetImageFromGallery() ;
                        }
                    }
                }).show();
    }

    public void ClickImageFromCamera() {

        CamIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        file = new File(Environment.getExternalStorageDirectory(),
                "file" + String.valueOf(System.currentTimeMillis()) + ".jpg");
        uri = Uri.fromFile(file);

        CamIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

        CamIntent.putExtra("return-data", true);

        startActivityForResult(CamIntent, 0);

    }

    public void GetImageFromGallery(){

        GalIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(Intent.createChooser(GalIntent, "Select Image From Gallery"), 2);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == RESULT_OK) {

            ImageCropFunction();

        }
        else if (requestCode == 2) {

            if (data != null) {

                uri = data.getData();

                ImageCropFunction();

            }
        }
        else if (requestCode == 1) {

            if (data != null) {

                Bundle bundle = data.getExtras();
                Bitmap bitmap = null;

                try {
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());

                    } catch (Exception e) {
                        bitmap = bundle.getParcelable("data");
                        e.printStackTrace();
                    }
                }catch (Exception e) {
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }

                imgv_profile.setImageBitmap(bitmap);
            }
        }
    }

    public void ImageCropFunction() {

        // Image Crop Code
        try {
            CropIntent = new Intent("com.android.camera.action.CROP");

            CropIntent.setDataAndType(uri, "image/*");

            CropIntent.putExtra("crop", "true");
            CropIntent.putExtra("outputX", 180);
            CropIntent.putExtra("outputY", 180);
            CropIntent.putExtra("aspectX", 3);
            CropIntent.putExtra("aspectY", 4);
            CropIntent.putExtra("scaleUpIfNeeded", true);
            CropIntent.putExtra("return-data", true);

            startActivityForResult(CropIntent, 1);

        } catch (ActivityNotFoundException e) {

        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:


                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    //CommonActions.snackMsgs(mView,"Permission Granted");

                    if(dialogChoice == 0)
                        ClickImageFromCamera();
                    else
                        GetImageFromGallery();

                } else {

                    CommonActions.snackMsgs(mView,"Permission Canceled");
                }
                break;
        }
    }

    public void resetPassword(String email){

        final ProgressDialog pDialog = CommonActions.createProgressDialog(getActivity());

        if (CommonActions.isConnected(getActivity())) {
            FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if (pDialog.isShowing())
                                pDialog.dismiss();

                            if (task.isSuccessful()) {
                                Snackbar.make(mView,"Reset password email has been sent !",Snackbar.LENGTH_SHORT).show();
                            }
                            else {

                                Snackbar.make(mView, task.getException().getMessage(), Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
        else{

            if (pDialog.isShowing())
                pDialog.dismiss();

            Snackbar.make(mView,"Please connect to the internet!",Snackbar.LENGTH_SHORT).show();
        }
    }

    public void updateProfile(){

        final ProgressDialog pDialog = CommonActions.createProgressDialog(getActivity());

        if (CommonActions.isConnected(getActivity())) {

            LoginActivity.loginUpdateCheck = true;

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

            User uObj = new User(user.getUid(), edit_name.getText().toString(), edit_shopname.getText().toString(), edit_address.getText().toString(), edit_phone.getText().toString(), edit_fax.getText().toString(), edit_email.getText().toString(),CommonObjects.pushNotificationToken);

            CommonObjects.uObj = uObj;

            FirebaseDatabase.getInstance().getReference().child("userdata").child(user.getUid()).setValue(uObj);

            if (pDialog.isShowing())
                pDialog.dismiss();

            Snackbar.make(mView,"Your profile updated successfully!",Snackbar.LENGTH_SHORT).show();
        }
        else{

            if (pDialog.isShowing())
                pDialog.dismiss();

            Snackbar.make(mView,"Please connect to the internet!",Snackbar.LENGTH_SHORT).show();
        }
    }

    public void hideKeyboard(){
        try {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mView.getWindowToken(), 0);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}