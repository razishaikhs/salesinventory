package com.approsity.salesinventory.add.item;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.sale.ExpendableSalesModel;
import com.approsity.salesinventory.sale.SaleHistoryAdapter;
import com.approsity.salesinventory.sale.SaleHistoryDetail;
import com.approsity.salesinventory.sale.SaleModel;
import com.approsity.salesinventory.sale.SalesAdap;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;


/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class PrintBarCode extends Fragment{

    View mView;

    public  static final int RequestPermissionCode  = 1;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    int PERMISSION  = 1;

    Button btn_share;

    ScrollView ll;

    int itemId;

    List<SaleModel> saleList;

    TextView tv_barcode;
    ImageView iv_barcode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_printbarcode, container, false);

        itemId = getArguments().getInt("itemid");

        tv_barcode = (TextView) mView.findViewById(R.id.tv_barcode);
        iv_barcode = (ImageView) mView.findViewById(R.id.iv_barcode);

        ll = (ScrollView) mView.findViewById(R.id.ll);

        btn_share = (Button) mView.findViewById(R.id.btn_share);
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                    requestPermissions(PERMISSIONS, PERMISSION);
                }
                else {

                    View view = ll;
                    Bitmap returnedBitmap = Bitmap.createBitmap(ll.getChildAt(0).getWidth(), ll.getChildAt(0).getHeight(),Bitmap.Config.ARGB_8888);
                    //Bind a canvas to it
                    Canvas canvas = new Canvas(returnedBitmap);
                    //Get the view's background
                    Drawable bgDrawable = view.getBackground();
                    if (bgDrawable!=null) {
                        //has background drawable, then draw it on the canvas
                        bgDrawable.draw(canvas);
                    }   else{
                        //does not have background drawable, then draw white background on the canvas
                        canvas.drawColor(Color.WHITE);
                    }
                    // draw the view on the canvas
                    view.draw(canvas);

                    share(saveBitmap(returnedBitmap));
                }
            }
        });

        if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
            requestPermissions(PERMISSIONS, PERMISSION);
        }

        getData();

        return mView;
    }

    public void getData() {

        Realm realm = Realm.getDefaultInstance();
        ItemModel iObj = realm.where(ItemModel.class).equalTo("id",itemId).findFirst();

        if(iObj != null) {
            setRc(iObj);
        }
    }

    public void setRc(ItemModel obj){

        tv_barcode.setText(obj.barcode);

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(obj.barcode, BarcodeFormat.CODE_128,600,150);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            iv_barcode.setImageBitmap(bitmap);

//            bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE,100,100);
//            barcodeEncoder = new BarcodeEncoder();
//            bitmap = barcodeEncoder.createBitmap(bitMatrix);
//            BitmapDrawable drawableEnd = new BitmapDrawable(getResources(), bitmap);
//            et_barcode.setCompoundDrawablesWithIntrinsicBounds(null,null, drawableEnd,null);

        } catch (WriterException e) {
            e.printStackTrace();
        }

    }

    public File saveBitmap(Bitmap BITMAP){

        if (BITMAP != null) {
            try {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                BITMAP.compress(Bitmap.CompressFormat.PNG, 40, bytes);

                File createFolder = new File(CommonObjects.appPath+"/barcodes");
                if(!createFolder.exists())
                    createFolder.mkdir();

                File newFile = new File(CommonObjects.appPath+"/barcodes/"+itemId+".png");

                if(newFile.exists())
                    newFile.delete();

                newFile.createNewFile();
                // write the bytes in file
                FileOutputStream fo = new FileOutputStream(newFile);
                fo.write(bytes.toByteArray());
                // remember close de FileOutput
                fo.close();

                new SingleMediaScanner(getActivity(), CommonObjects.appPath);

                return newFile;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public void share(File f){

        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        // set the type
        shareIntent.setType("image/png");
        // add a subject
        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Email with attachment");
        // build the body of the message to be shared
        String shareMessage = "\n";
        // add the message
        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareMessage);
        shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.fromFile(f));
        // start the chooser for sharing
        startActivity(Intent.createChooser(shareIntent, "Please select sharing medium"));
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    //CommonActions.snackMsgs(mView,"Permission Granted");

                } else {

                    CommonActions.snackMsgs(mView,"Permission Canceled");
                }
                break;
        }
    }

    private class SingleMediaScanner implements MediaScannerConnection.MediaScannerConnectionClient {
        private MediaScannerConnection mMs;
        private String path;

        SingleMediaScanner(Context context, String f) {
            path = f;
            mMs = new MediaScannerConnection(context, this);
            mMs.connect();
        }
        @Override
        public void onMediaScannerConnected() {
            mMs.scanFile(path, null);
        }
        @Override
        public void onScanCompleted(String path, Uri uri) {
            mMs.disconnect();
        }
    }

}
