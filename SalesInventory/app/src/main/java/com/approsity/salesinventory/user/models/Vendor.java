package com.approsity.salesinventory.user.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class Vendor extends RealmObject{

    public Vendor() {
    }

    public Vendor(int id, String name, String address, String contactno, String email) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.contactno = contactno;
        this.email = email;
    }

    @PrimaryKey
    public int id;
    public String name;
    public String address;
    public String contactno;
    public String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactno() {
        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
