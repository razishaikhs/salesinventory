package com.approsity.salesinventory.notification;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.approsity.salesinventory.R;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;

import java.util.List;

import io.realm.Realm;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {

    private static final String TAG = NotificationListAdapter.class.getSimpleName();

    private List<NotificationData> mItems;

    Context context;

    private int position;

    public NotificationListAdapter(Context context, List<NotificationData> items) {
        mItems = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int view_type) {

        View v = null;

        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_row, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int pos) {

        final NotificationData mObj = mItems.get(pos);

        /*if (pos % 2 == 1) {
            viewHolder.ll.setBackgroundColor(context.getResources().getColor(R.color.appointment_green));
        } else {
            viewHolder.ll.setBackgroundColor(context.getResources().getColor(R.color.appointment_blue));

        }*/

        if(mObj.status == CommonObjects.UNREAD)
            viewHolder.ll.setSelected(true);
        else
            viewHolder.ll.setSelected(false);

        viewHolder.text_title.setText(mObj.title);
        viewHolder.text_desc.setText(mObj.message);
        viewHolder.date.setText(CommonActions.dateToStringFormat(mObj.datetime));

        viewHolder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NotificationData toEdit = Realm.getDefaultInstance().where(NotificationData.class).equalTo("id", mObj.id).findFirst();
                Realm.getDefaultInstance().beginTransaction();
                toEdit.status = CommonObjects.READ;
                Realm.getDefaultInstance().commitTransaction();

                notifyDataSetChanged();

               // context.startActivity(new Intent(context,Appointment_Notification.class));
            }
        });

        viewHolder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("Do you want to delete notification ?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                deleteRecord(mObj.id);
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });
    }



    @Override
    public int getItemViewType(int position) {

        return 0;
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        LinearLayout ll;
        TextView text_title;
        TextView text_desc;
        TextView date;
        TextView time;
        ImageView btn_delete;

        TextView tvbadge;

        View rootView;

        ViewHolder(View v) {
            super(v);

            text_title = (TextView) v.findViewById(R.id.text_title);
            text_desc = (TextView) v.findViewById(R.id.text_desc);
            date = (TextView) v.findViewById(R.id.date);
            time = (TextView) v.findViewById(R.id.time);

            btn_delete = (ImageView) v.findViewById(R.id.btn_delete);

             ll = (LinearLayout) v.findViewById(R.id.ll);

            //v.setOnClickListener(this);

            rootView = v;
        }
    }


    public NotificationData getItem(int position) {
        return mItems.get(position);
    }

    public void addData(NotificationData newModelData, int position) {
        mItems.add(position, newModelData);
        notifyItemInserted(position);
    }

    public void removeData(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    public void clearData() {
        Log.v(TAG, "clearData()");
        int size = this.mItems.size();

        int index = 0;
        if (size > index) {
            for (int i = index; i < size; i++) {
                this.mItems.remove(index);
            }

            this.notifyItemRangeRemoved(index, size - 1);
        }
    }


    public void deleteRecord(int id) {


        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.where(NotificationData.class).equalTo("id", id).findFirst().deleteFromRealm();
        realm.commitTransaction();
        realm.close();

        notifyDataSetChanged();
       /* NotificationData rows= Realm.getDefaultInstance().where(NotificationData.class).equalTo("user_id", id).findFirst();
        rows.deleteFromRealm();*/

    }

}