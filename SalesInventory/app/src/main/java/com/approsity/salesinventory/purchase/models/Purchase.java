package com.approsity.salesinventory.purchase.models;


import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.user.models.Vendor;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class Purchase extends RealmObject{

    public Purchase() {
    }

    public Purchase(int id, RealmList<Vendor> vendorList, RealmList<ItemModel> itemList, String invoice, String quantity, String cost, String desc, Date datetime, String alias, String method, String vendorname, String itemname, String itemsize, String itemtype) {
        this.id = id;
        this.vendorList = vendorList;
        this.itemList = itemList;
        this.invoice = invoice;
        this.quantity = quantity;
        this.cost = cost;
        this.desc = desc;
        this.datetime = datetime;
        this.alias = alias;
        this.method = method;

        this.vendorname = vendorname;
        this.itemname = itemname;

        this.itemsize = itemsize;
        this.itemtype = itemtype;
    }

    @PrimaryKey
    public int id;

    public RealmList<Vendor> vendorList;
    public RealmList<ItemModel> itemList;
    public String invoice;
    public String quantity;
    public String cost;
    public String desc;
    public Date datetime;
    public String alias;
    public String method;
    public String vendorname;
    public String itemname;
    public String itemtype;
    public String itemsize;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RealmList<Vendor> getVendorList() {
        return vendorList;
    }

    public void setVendorList(RealmList<Vendor> vendorList) {
        this.vendorList = vendorList;
    }

    public RealmList<ItemModel> getItemList() {
        return itemList;
    }

    public void setItemList(RealmList<ItemModel> itemList) {
        this.itemList = itemList;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getVendorname() {
        return vendorname;
    }

    public void setVendorname(String vendorname) {
        this.vendorname = vendorname;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getItemtype() {
        return itemtype;
    }

    public void setItemtype(String itemtype) {
        this.itemtype = itemtype;
    }

    public String getItemsize() {
        return itemsize;
    }

    public void setItemsize(String itemsize) {
        this.itemsize = itemsize;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }
}
