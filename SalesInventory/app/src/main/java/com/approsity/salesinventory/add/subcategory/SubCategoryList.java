package com.approsity.salesinventory.add.subcategory;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.add.models.SubCategory;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.viethoa.RecyclerViewFastScroller;
import com.viethoa.models.AlphabetItem;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;


/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class SubCategoryList extends Fragment implements CommonObjects.RecyclerViewClickListener{

    View mView;

    RecyclerView rc;
    SubCategoryAdap adap;
    LinearLayoutManager mLayoutManager;

    List<SubCategory> cList;

    SubCategoryDialog addDialog;

    RecyclerViewFastScroller fast_scroller;

    EditText et_search;

    RelativeLayout rl_actions;
    TextView tv_norecord;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_categorylist, container, false);

        rc = (RecyclerView) mView.findViewById(R.id.rc);
        fast_scroller = (RecyclerViewFastScroller) mView.findViewById(R.id.fast_scroller);

        tv_norecord = (TextView) mView.findViewById(R.id.tv_norecord);
        tv_norecord.setVisibility(View.VISIBLE);

        rl_actions = (RelativeLayout) mView.findViewById(R.id.rl_actions);
        rl_actions.setVisibility(View.GONE);

        FloatingActionButton fab = (FloatingActionButton) mView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addDialog();
            }
        });

        et_search = (EditText) mView.findViewById(R.id.et_search);
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    adap.getFilter().filter(s.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        getData();

        return mView;
    }

    public void addDialog() {

        addDialog = SubCategoryDialog.newInstance(getActivity(), null);
        addDialog.setButtonClicked(new SubCategoryDialog.onButtonClicked() {
            @Override
            public void onClicked(final String name, final String alias, final String desc) {
                addDialog.dismiss();

                Realm db = Realm.getDefaultInstance();
                db.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        Number currentIdNum = realm.where(SubCategory.class).max("id");
                        int nextId = 0;
                        if(currentIdNum == null) {
                            nextId = 1;
                        } else {
                            nextId = currentIdNum.intValue() + 1;
                        }

                        SubCategory fvData = new SubCategory(nextId, name, alias ,desc);
                        realm.insert(fvData);

                        Snackbar.make(mView,"Record Added Successfully!",Snackbar.LENGTH_SHORT).show();

                        RealmList<SubCategory> scl = new RealmList<>();
                        scl.add(fvData);
                        int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                        Transactions tObj = new Transactions(i,null,scl,null,null,null,null, CommonActions.currentDateTime(),"Sub category created",fvData.name);
                        realm.insertOrUpdate(tObj);
                    }
                });
            }
        });

        addDialog.show(getFragmentManager(),"add");
    }

    public void getData() {
        cList = new ArrayList<>();

        Realm.getDefaultInstance().where(SubCategory.class)
                .findAllSortedAsync("name",  Sort.ASCENDING)
                .addChangeListener(new RealmChangeListener<RealmResults<SubCategory>>() {
                    @Override
                    public void onChange(RealmResults<SubCategory> notiList) {

                        cList = notiList;

                        if(notiList.size() > 0) {

                            setRc(cList);

                            rl_actions.setVisibility(View.VISIBLE);
                            tv_norecord.setVisibility(View.GONE);
                        }
                        else
                        {
                            rl_actions.setVisibility(View.GONE);
                            tv_norecord.setVisibility(View.VISIBLE);
                        }
                    }});

    }

    public void setRc(List<SubCategory> list){
        adap = new SubCategoryAdap(getActivity(), AddAlphabeticalFilter(Realm.getDefaultInstance().copyFromRealm(list)), this,rc);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rc.setLayoutManager(mLayoutManager);
        rc.setAdapter(adap);

        fast_scroller.setRecyclerView(rc);
    }

    public List<SubCategory> AddAlphabeticalFilter(List<SubCategory> catList) {
        ArrayList<AlphabetItem> mAlphabetItems = new ArrayList<>();
        List<String> strAlphabets = new ArrayList<>();
        for (int i = 0; i < catList.size(); i++) {
            String name = catList.get(i).name.toUpperCase();
            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1);
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }

        fast_scroller.setUpAlphabet(mAlphabetItems);

        return catList;
    }

    @Override
    public void recyclerViewListClicked(View v, int position, Context con) {

         /* if(v.getId() != R.id.btn_delete) {

              addDialog.show(getChildFragmentManager(),"edit");
          }*/
    }

}
