package com.approsity.salesinventory.utils;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.approsity.salesinventory.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CommonActions {

    Context con;

    public CommonActions(Context con) {
        this.con = con;
    }

    public static void snackMsgs(View v, String msg) {

        Snackbar snack = Snackbar.make(v, msg, Snackbar.LENGTH_LONG);
        View view = snack.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);

       /* snack.setAction("Refresh",new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                snack.dismiss();

            }
        });*/

        snack.show();
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    public static boolean testEmpty(String str) {
        if ((str == null) || str.matches("^\\s*$")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches()) {
            isValid = true;
        }

        return isValid;
    }

    public static ProgressDialog createProgressDialog(Context mContext) {
        ProgressDialog dialog = new ProgressDialog(mContext);
        try {
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }
//        dialog.setMessage("Please wait...");
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.progressdialog);

        dialog.show();

        return dialog;
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }

        return false;
    }

    public static boolean isGpsEnabled(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    public void changeAppLang(String lang, Context con) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            setSystemLocale(config, locale);
        } else {
            setSystemLocaleLegacy(config, locale);
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1)
            con.getApplicationContext().getResources().updateConfiguration(config, con.getResources().getDisplayMetrics());
    }

    @SuppressWarnings("deprecation")
    public Locale getSystemLocaleLegacy(Configuration config) {
        return config.locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public Locale getSystemLocale(Configuration config) {
        return config.getLocales().get(0);
    }

    @SuppressWarnings("deprecation")
    public void setSystemLocaleLegacy(Configuration config, Locale locale) {
        config.locale = locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public void setSystemLocale(Configuration config, Locale locale) {
        config.setLocale(locale);
    }


    public static Bitmap convertToBitmap(String base64Str) throws IllegalArgumentException {
        if (base64Str != null) {
            byte[] decodedBytes = Base64.decode(
                    base64Str.substring(base64Str.indexOf(",") + 1),
                    Base64.DEFAULT
            );

            return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
        }

        return null;
    }

    public static String convertToString(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }




    /**
     * @param file //     * @param requiredHeight
     * @return
     */
    public static Bitmap decodeFile(File file) {
        int requiredHeight = 1000;
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(file), null, o);

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= requiredHeight &&
                    o.outHeight / scale / 2 >= requiredHeight) {
                scale *= 2;
            }
            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(file), null, o2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void deleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteRecursive(child);
            }
        }

        fileOrDirectory.delete();
    }

    public static String formatFileSize(long size) {
        String hrSize = null;

        double b = size;
        double k = size / 1024.0;
        double m = ((size / 1024.0) / 1024.0);
        double g = (((size / 1024.0) / 1024.0) / 1024.0);
        double t = ((((size / 1024.0) / 1024.0) / 1024.0) / 1024.0);

        DecimalFormat dec = new DecimalFormat("0.00");

        if (t > 1) {
            hrSize = dec.format(t).concat(" TB");
        } else if (g > 1) {
            hrSize = dec.format(g).concat(" GB");
        } else if (m > 1) {
            hrSize = dec.format(m).concat(" MB");
        } else if (k > 1) {
            hrSize = dec.format(k).concat(" KB");
        } else {
            hrSize = dec.format(b).concat(" Bytes");
        }

        return hrSize;
    }

    public static void showKeyboard(Context ctx) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public static void hideSoftKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
/*
    //OkHttpClient for SSL
    private static OkHttpClient client = null;
    public static OkHttpClient getOkHttpClient() {

        if (client == null) {
            try {
                client = new OkHttpClient().newBuilder()
                        .readTimeout(NETWORK_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                        .connectTimeout(NETWORK_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                        .build();


            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Exception", e.getMessage());
            }

        }
        return client;
    }*/



  //////////// DATE ////////

    public static Date stringToDateFormat_onlyDate(String input) {
        // 22/12/2017 ---> input

        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Log.e("Input Date",input);
            date = format.parse(input);
            Log.e("Output Date",date+" ");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Fri Dec 22 2017  ---> output
        return date;
    }

    public static Date stringToDateFormat(String input) {
        // 22/12/2017 08:46:42 PM  ---> input

        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");
        try {
            Log.e("Input Date",input);
            date = format.parse(input);
            Log.e("Output Date",date+" ");
        } catch (ParseException e) {
            e.printStackTrace();
            date = stringToDateFormat_onlyDate(input);
        }

        // Fri Dec 22 20:46:42 GMT+05:00 2017  ---> output
        return date;
    }


    public static String dateToStringFormat(Date input)
    {
        // Fri Dec 22 20:46:42 GMT+05:00 2017  ---> input

        String date = "";
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy hh:mm aa");
        Log.e("Input Date",input+" ");
        date = format.format(input);
        Log.e("Output Date",date+" ");

        // 22/12/2017 08:46:42 PM  ---> output

        return date;
    }

    public static String dateToStringFormatWithSeconds(Date input)
    {
        // Fri Dec 22 20:46:42 GMT+05:00 2017  ---> input

        String date = "";
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy hh:mm:ss aa");
        Log.e("Input Date",input+" ");
        date = format.format(input);
        Log.e("Output Date",date+" ");

        // 22/12/2017 08:46:42 PM  ---> output

        return date;
    }

    public static String getCurrentTimeAmPm(){

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss a");
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String timeDialogToAMPM(int hourOfDay, int minute)
    {
        Calendar calendar = Calendar.getInstance();

        String AM_PM = (hourOfDay < 12) ? "AM" : "PM";

        if(hourOfDay == 0){
            hourOfDay = 12;
        }else if(hourOfDay == 12) {
            hourOfDay = 12;
        }
        else if(hourOfDay > 12)
            hourOfDay = hourOfDay - 12;

        String time = String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute)  +":" + String.format("%02d", calendar.get(Calendar.SECOND)) + " "+AM_PM;

        return time;

    }

    public static Date currentDateTime(){

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        int m = month + 1;
        String date = day+"/"+m+"/"+year;
        String time = CommonActions.getCurrentTimeAmPm();

        return CommonActions.stringToDateFormat(date +" "+time);
    }
}
