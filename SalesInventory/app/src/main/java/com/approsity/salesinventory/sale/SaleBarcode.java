package com.approsity.salesinventory.sale;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.approsity.salesinventory.utils.FreeActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.math.BigDecimal;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class SaleBarcode extends Fragment{

    View mView;

    EditText et_barcode;
    ImageView iv_barcode, iv_clearbarcode;
    Button btn_barcodesearch;

    public  static final int RequestPermissionCode  = 1;
    String[] PERMISSIONS = {Manifest.permission.CAMERA};
    int PERMISSION  = 1;

    Animation shake;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_sales_barcode, container, false);

        shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);

        et_barcode = (EditText) mView.findViewById(R.id.et_barcode);

        iv_barcode = (ImageView) mView.findViewById(R.id.iv_barcode);
        iv_clearbarcode = (ImageView) mView.findViewById(R.id.iv_clearbarcode);

        btn_barcodesearch = (Button) mView.findViewById(R.id.btn_barcodesearch);

        setOperations();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideKeyboard();
            }
        }, 250);

        return mView;
    }

    public void hideKeyboard(){
        try {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mView.getWindowToken(), 0);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setOperations() {

        iv_barcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                    requestPermissions(PERMISSIONS, PERMISSION);
                }
                else {
                    openBarCode();
                }
            }
        });

        btn_barcodesearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(TextUtils.isEmpty(et_barcode.getText().toString())) {

                    et_barcode.requestFocus();
                    et_barcode.setError("Please enter barcode");
                    et_barcode.startAnimation(shake);

                }else{

                    getData();
                }
            }
        });


        iv_clearbarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                et_barcode.setText("");
            }
        });


        if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
            requestPermissions(PERMISSIONS, PERMISSION);
        }
    }


    private void openBarCode() {

        IntentIntegrator
                .forSupportFragment(SaleBarcode.this)
                .setOrientationLocked(false)
                .setBeepEnabled(false)
                .initiateScan();
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    //CommonActions.snackMsgs(mView,"Permission Granted");

                } else {

                    CommonActions.snackMsgs(mView,"Permission Canceled");
                }
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if(result != null) {
            if(result.getContents() == null) {
                //  Snackbar.make(mView,"",Snackbar.LENGTH_SHORT).show();
                //  Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                et_barcode.setText(result.getContents()+"");
                et_barcode.setSelection(et_barcode.getText().length());
                getData();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void getData() {

        Realm.getDefaultInstance().where(SaleModel.class)
                .equalTo("barcode", et_barcode.getText().toString())
                .findAllAsync()
                .addChangeListener(new RealmChangeListener<RealmResults<SaleModel>>() {
                    @Override
                    public void onChange(RealmResults<SaleModel> notiList) {

                        hideKeyboard();

                        if (notiList.size() > 0) {

                            Intent i = new Intent(getActivity(), FreeActivity.class);
                            i.putExtra("frag", CommonObjects.OpenFragments.saleHistoryDetail);
                            i.putExtra("saleid", notiList.get(0).id);
                            getActivity().startActivity(i);

                        }
                        else {

                            hideKeyboard();
                            Snackbar.make(mView, "No sale found!", Snackbar.LENGTH_SHORT).show();

                        }
                    }
                });

    }
}
