package com.approsity.salesinventory.history.model;


import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.add.models.Product;
import com.approsity.salesinventory.add.models.SubCategory;
import com.approsity.salesinventory.purchase.models.Purchase;
import com.approsity.salesinventory.user.models.Vendor;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class Transactions extends RealmObject{

    public Transactions() {
    }


    public Transactions(int id, RealmList<Category> catList, RealmList<SubCategory> subcatList, RealmList<Product> productList, RealmList<Vendor> vendorList, RealmList<ItemModel> itemList, RealmList<Purchase> purchaseList, Date datetime, String desc, String name) {
        this.id = id;
        this.catList = catList;
        this.subcatList = subcatList;
        this.productList = productList;
        this.vendorList = vendorList;
        this.itemList = itemList;
        this.purchaseList = purchaseList;
        this.datetime = datetime;
        this.desc = desc;
        this.name = name;
    }

    @PrimaryKey
    public int id;

    public RealmList<Category> catList;
    public RealmList<SubCategory> subcatList;
    public RealmList<Product> productList;
    public RealmList<Vendor> vendorList;
    public RealmList<ItemModel> itemList;
    public RealmList<Purchase> purchaseList;
    public Date datetime;
    public String desc;
    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RealmList<Category> getCatList() {
        return catList;
    }

    public void setCatList(RealmList<Category> catList) {
        this.catList = catList;
    }

    public RealmList<SubCategory> getSubcatList() {
        return subcatList;
    }

    public void setSubcatList(RealmList<SubCategory> subcatList) {
        this.subcatList = subcatList;
    }

    public RealmList<Product> getProductList() {
        return productList;
    }

    public void setProductList(RealmList<Product> productList) {
        this.productList = productList;
    }

    public RealmList<Vendor> getVendorList() {
        return vendorList;
    }

    public void setVendorList(RealmList<Vendor> vendorList) {
        this.vendorList = vendorList;
    }

    public RealmList<ItemModel> getItemList() {
        return itemList;
    }

    public void setItemList(RealmList<ItemModel> itemList) {
        this.itemList = itemList;
    }

    public RealmList<Purchase> getPurchaseList() {
        return purchaseList;
    }

    public void setPurchaseList(RealmList<Purchase> purchaseList) {
        this.purchaseList = purchaseList;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
