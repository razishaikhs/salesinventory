package com.approsity.salesinventory.history.model;


import android.support.v4.app.Fragment;

import com.approsity.salesinventory.MenuItemsModel;
import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.add.models.Product;
import com.approsity.salesinventory.add.models.SubCategory;
import com.approsity.salesinventory.purchase.models.Purchase;
import com.approsity.salesinventory.user.models.Vendor;

import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class ExpendableTransactionsModel{

    public ExpendableTransactionsModel() {
    }

    public ExpendableTransactionsModel(String title, List<Transactions> tList) {
        this.title = title;
        this.tList = tList;
    }

    public String title;
    public List<Transactions> tList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Transactions> gettList() {
        return tList;
    }

    public void settList(List<Transactions> tList) {
        this.tList = tList;
    }

}
