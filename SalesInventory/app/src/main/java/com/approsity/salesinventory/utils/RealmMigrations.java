package com.approsity.salesinventory.utils;


import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

/**
 * Created by Raziuddin.Shaikh on 1/19/2018.
 */

public class RealmMigrations implements RealmMigration {

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        final RealmSchema schema = realm.getSchema();

        if (newVersion == 1) {
            final RealmObjectSchema saleSchema = schema.get("SaleModel");
            saleSchema.addField("barcode",String.class);
        }

    }
}
