package com.approsity.salesinventory.user;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.approsity.salesinventory.NavigationActivity;
import com.approsity.salesinventory.R;
import com.approsity.salesinventory.notification.NotificationData;
import com.approsity.salesinventory.user.models.Customer;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;

/**
 * Created by Raziuddin.Shaikh on 11/24/2017.
 */

public class LoginActivity extends AppCompatActivity {

    ScrollView sv;

    Button btnLogin;
    TextView link_to_register,link_to_forgot;

    SharedPreferences app_preferences;
    SharedPreferences.Editor editor;

    EditText et_email,et_password;

    Animation shake;

    public static boolean loginUpdateCheck;

    @Override
    public void onStart() {
        super.onStart();

        if(!app_preferences.getString("email","").equals("")){
            signIn(app_preferences.getString("email",""),app_preferences.getString("pass",""));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        /*Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            Log.e("EXTRA Title -->", bundle.getString("title")+ " ");
            Log.e("EXTRA MSG -->", bundle.getString("message")+ " ");

            if(bundle.getString("title") != null && bundle.getString("message") != null){
                Long tsLong = System.currentTimeMillis()/1000;
                String ts = tsLong.toString();

                Realm db = Realm.getDefaultInstance();
                db.beginTransaction();
                NotificationData noti1 = new NotificationData(Integer.parseInt(ts),bundle.getString("title"),bundle.getString("message"), CommonActions.currentDateTime(), CommonObjects.UNREAD);
                db.insertOrUpdate(noti1);
                db.commitTransaction();
            }
        }*/

        setContentView(R.layout.activity_login);

        CommonObjects.pushNotificationToken = FirebaseInstanceId.getInstance().getToken();

        setupAds();

        shake = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.shake);

        app_preferences = getSharedPreferences("settingFile", Context.MODE_PRIVATE);
        editor = app_preferences.edit();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hideKeyboard();

                if(TextUtils.isEmpty(et_email.getText().toString()))
                {
                    sv.post(new Runnable() {
                        @Override
                        public void run() {
                            sv.scrollTo(0, et_email.getTop());
                        }
                    });
                    et_email.requestFocus();
                    et_email.setError("Please enter the email");
                    et_email.startAnimation(shake);
                }
                else if(!CommonActions.isEmailValid(et_email.getText().toString()))
                {
                    sv.post(new Runnable() {
                        @Override
                        public void run() {
                            sv.scrollTo(0, et_email.getTop());
                        }
                    });
                    et_email.requestFocus();
                    et_email.setError("Please enter correct email");
                    et_email.startAnimation(shake);
                }
                else if(TextUtils.isEmpty(et_password.getText().toString()))
                {
                    sv.post(new Runnable() {
                        @Override
                        public void run() {
                            sv.scrollTo(0, et_password.getTop());
                        }
                    });
                    et_password.requestFocus();
                    et_password.setError("Please enter password");
                    et_password.startAnimation(shake);
                }
                else{
                    signIn(et_email.getText().toString(),et_password.getText().toString());
                }
            }
        });

        link_to_register = (TextView) findViewById(R.id.link_to_register);
        link_to_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               startActivity(new Intent(LoginActivity.this,RegistrationActivity.class));
            }
        });

        link_to_forgot = (TextView) findViewById(R.id.link_to_forgot);
        link_to_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Reset Password");
                builder.setMessage("Please enter email to reset password !");

                final EditText input = new EditText(LoginActivity.this);
                input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                input.setGravity(Gravity.CENTER);
                builder.setView(input);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        hideKeyboard();

                        if(TextUtils.isEmpty(input.getText().toString()))
                        {
                            input.requestFocus();
                            input.setError("Please enter the email");
                            input.startAnimation(shake);
                        }
                        else if(!CommonActions.isEmailValid(input.getText().toString()))
                        {
                            input.requestFocus();
                            input.setError("Please enter correct email");
                            input.startAnimation(shake);
                        }
                        else{

                            dialog.cancel();

                            resetPassword(input.getText().toString());
                        }
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            }
        });

        sv = (ScrollView) findViewById(R.id.sv);

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                test();
            }
        }, 3000);*/

    }

    /*private void test() {

        CommonObjects.uObj = new User();
        CommonObjects.uObj.setEmail("razi@hotmail.com");
        CommonObjects.uObj.setUserName("Razi Shaikh");

        startActivity(new Intent(LoginActivity.this, NavigationActivity.class));
        LoginActivity.this.finish();
    }*/

    public void signIn(final String email, final String pass){

        final ProgressDialog pDialog = CommonActions.createProgressDialog(LoginActivity.this);

        if (CommonActions.isConnected(LoginActivity.this)) {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, pass)
                    .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {

                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                                FirebaseDatabase.getInstance().getReference().child("userdata").child(user.getUid()).child("push").setValue(CommonObjects.pushNotificationToken);

                                if(user.isEmailVerified()) {

                                    FirebaseDatabase.getInstance().getReference().child("userdata").child(user.getUid()).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot snapshot) {

                                            if(!loginUpdateCheck) {
                                                loginUpdateCheck = false;

                                                CommonObjects.uObj = snapshot.getValue(User.class);

                                                Crashlytics.setUserIdentifier(CommonObjects.uObj.userId);
                                                Crashlytics.setUserEmail(CommonObjects.uObj.email);
                                                Crashlytics.setUserName(CommonObjects.uObj.userName);

                                                editor.putString("email", email);
                                                editor.commit();
                                                editor.putString("pass", pass);
                                                editor.commit();

                                                if (pDialog.isShowing())
                                                    pDialog.dismiss();

                                                startActivity(new Intent(LoginActivity.this, NavigationActivity.class));
                                                LoginActivity.this.finish();
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                            if (pDialog.isShowing())
                                                pDialog.dismiss();

                                            Snackbar.make(sv, databaseError.getMessage(), Snackbar.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                else{

                                    if (pDialog.isShowing())
                                        pDialog.dismiss();

                                    CustomSnackbar("Email verification is pending. Please verify email.",user);
                                }

                            } else {

                                if (pDialog.isShowing())
                                    pDialog.dismiss();

                                Snackbar.make(sv, task.getException().getMessage(), Snackbar.LENGTH_SHORT).show();

                                editor.putString("email", "");
                                editor.commit();
                                editor.putString("pass", "");
                                editor.commit();

                            }
                        }
                    });
        }else{

            if (pDialog.isShowing())
                pDialog.dismiss();

            Snackbar.make(sv,"Please connect to the internet!",Snackbar.LENGTH_SHORT).show();
        }
    }

    public void resetPassword(String email){

        final ProgressDialog pDialog = CommonActions.createProgressDialog(LoginActivity.this);

        if (CommonActions.isConnected(LoginActivity.this)) {
            FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if (pDialog.isShowing())
                                pDialog.dismiss();

                            if (task.isSuccessful()) {
                                Snackbar.make(sv,"Reset password email has been sent !",Snackbar.LENGTH_SHORT).show();
                            }
                            else {

                                Snackbar.make(sv, task.getException().getMessage(), Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
        else{

            if (pDialog.isShowing())
                pDialog.dismiss();

            Snackbar.make(sv,"Please connect to the internet!",Snackbar.LENGTH_SHORT).show();
        }
    }

    public void hideKeyboard(){
        try {
            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(sv.getWindowToken(), 0);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setupAds() {

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("ads").child("appid").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                CommonObjects.ADS_APPID = snapshot.getValue().toString();

                MobileAds.initialize(getApplicationContext(), CommonObjects.ADS_APPID);

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        mDatabase.child("ads").child("unitid").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                CommonObjects.ADS_UNITID = snapshot.getValue().toString();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void CustomSnackbar(String s,final FirebaseUser user) {

        LinearLayout.LayoutParams objLayoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final Snackbar snackbar = Snackbar.make(sv, "", 5000);

        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        layout.setPadding(0,0,0,0);

        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);

        LayoutInflater mInflater = (LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE);
        View snackView = getLayoutInflater().inflate(R.layout.snackbar, null);

        TextView tv_text = (TextView) snackView.findViewById(R.id.tv_text);
        tv_text.setText(s);

        TextView tv_resend = (TextView) snackView.findViewById(R.id.tv_resend);

        tv_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                user.sendEmailVerification()
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {

                                    Snackbar.make(sv, "Verification email has sent to " + user.getEmail(), Snackbar.LENGTH_LONG).show();
                                }
                            }
                        });

                snackbar.dismiss();
            }
        });

        TextView tv_dismiss = (TextView) snackView.findViewById(R.id.tv_dismiss);
        tv_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                snackbar.dismiss();
            }
        });

        layout.addView(snackView, objLayoutParams);
        snackbar.show();
    }
}
