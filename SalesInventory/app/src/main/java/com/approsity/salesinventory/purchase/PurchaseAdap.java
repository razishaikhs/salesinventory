package com.approsity.salesinventory.purchase;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.item.ItemDialog;
import com.approsity.salesinventory.add.item.ItemHistoryActivity;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.purchase.models.Purchase;
import com.approsity.salesinventory.user.models.Vendor;
import com.approsity.salesinventory.user.vendor.VendorDetail;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.viethoa.RecyclerViewFastScroller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

public class PurchaseAdap extends RecyclerView.Adapter<PurchaseAdap.ViewHolder> implements RecyclerViewFastScroller.BubbleTextGetter, Filterable {

    private static final String TAG = PurchaseAdap.class.getSimpleName();

    private List<Purchase> mItems,items2;

    Activity act;

    private int position;

    CommonObjects.RecyclerViewClickListener itemListener;

    private CustomFilter mFilter;

    Purchase_Edit_Dialog purchaseEditDialog;

    RecyclerView rc;

    public PurchaseAdap(Activity act, List<Purchase> items, CommonObjects.RecyclerViewClickListener itemListener, RecyclerView rc) {
        mItems = items;
        this.act = act;
        this.rc = rc;
        this.itemListener = itemListener;

        mFilter = new CustomFilter();
        this.items2 = new ArrayList<Purchase>();
        this.items2.addAll(mItems);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int view_type) {

        View v = null;

        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.frag_purchaselist_row, viewGroup, false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int pos) {

        final Purchase aObj = mItems.get(pos);

        String invoice ="<i>Invoice :</i>"+" "+ aObj.invoice;
        String vend ="<i>Vend :</i>"+" "+ aObj.vendorname;
        String item ="<i>Item :</i>"+" "+ aObj.itemname;

        viewHolder.tv_invoice.setText(Html.fromHtml(invoice), TextView.BufferType.SPANNABLE);
        viewHolder.text_vname.setText(Html.fromHtml(vend), TextView.BufferType.SPANNABLE);
        viewHolder.text_iname.setText(Html.fromHtml(item), TextView.BufferType.SPANNABLE);
        viewHolder.text_alias.setText(aObj.alias);
        viewHolder.text_desc.setText(aObj.desc);

        String type ="<i>Type :</i>"+" "+ aObj.itemtype;
        String size ="<i>Size :</i>"+" "+ aObj.itemsize;
        String qty ="<i>Qty :</i>"+" "+ aObj.quantity;
        String cost ="<i>Cost :</i>"+" "+ aObj.cost;
        String tv_payby ="<i>Pay by :</i>"+" "+ aObj.method;
        String tv_datetime ="<i>D :</i>"+" "+ CommonActions.dateToStringFormat(aObj.datetime);

        viewHolder.tv_type.setText(Html.fromHtml(type), TextView.BufferType.SPANNABLE);
        viewHolder.tv_size.setText(Html.fromHtml(size), TextView.BufferType.SPANNABLE);
        viewHolder.tv_qty.setText(Html.fromHtml(qty), TextView.BufferType.SPANNABLE);
        viewHolder.tv_cost.setText(Html.fromHtml(cost), TextView.BufferType.SPANNABLE);
        viewHolder.tv_datetime.setText(Html.fromHtml(tv_datetime), TextView.BufferType.SPANNABLE);
        viewHolder.tv_paymentmethod.setText(Html.fromHtml(tv_payby), TextView.BufferType.SPANNABLE);


        viewHolder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(act, R.style.MyDialogTheme);
                builder1.setMessage("Do you want to delete category ?");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                deleteRecord(aObj.id);
                            }
                        });
                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        viewHolder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getEditDialog(aObj);

            }
        });

        viewHolder.btn_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(act,PurchaseHistoryActivity.class);
                i.putExtra("vid",aObj.id);
                act.startActivity(i);
            }
        });


        if (CommonObjects.VendorPurchaseCheck) {
            viewHolder.btn_delete.setVisibility(View.GONE);
            viewHolder.btn_edit.setVisibility(View.GONE);
            viewHolder.btn_info.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemViewType(int position) {

       return 0;
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String getTextToShowInBubble(int pos) {

        if (pos < 0 || pos >= mItems.size())
            return null;

        String name = mItems.get(pos).alias;
        if (name == null || name.length() < 1)
            return null;

        return mItems.get(pos).alias.substring(0, 1);
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView text_vname,text_alias,text_desc,tv_type,tv_size,tv_qty,tv_cost,tv_datetime;
        TextView tv_invoice,text_iname,tv_paymentmethod;
        TextView btn_delete,btn_edit,btn_info;

        View rootView;

        ViewHolder(View v) {
            super(v);

            text_vname = (TextView) v.findViewById(R.id.text_vname);
            text_alias = (TextView) v.findViewById(R.id.text_alias);
            text_desc = (TextView) v.findViewById(R.id.text_desc);
            tv_type = (TextView) v.findViewById(R.id.tv_type);
            tv_size = (TextView) v.findViewById(R.id.tv_size);
            tv_qty = (TextView) v.findViewById(R.id.tv_qty);
            tv_cost = (TextView) v.findViewById(R.id.tv_cost);
            tv_datetime = (TextView) v.findViewById(R.id.tv_datetime);

            tv_invoice = (TextView) v.findViewById(R.id.tv_invoice);
            text_iname = (TextView) v.findViewById(R.id.text_iname);
            tv_paymentmethod = (TextView) v.findViewById(R.id.tv_paymentmethod);

            btn_delete = (TextView) v.findViewById(R.id.btn_delete);
            btn_edit = (TextView) v.findViewById(R.id.btn_edit);

            btn_info = (TextView) v.findViewById(R.id.btn_info);

            text_alias.setOnClickListener(this);
            text_desc.setOnClickListener(this);


            v.setOnClickListener(this);


            rootView = v;
        }

        @Override
        public void onClick(View v) {

            itemListener.recyclerViewListClicked(v, getLayoutPosition(),act);

           /* selectedPosition=getLayoutPosition();
            notifyDataSetChanged();*/

        }
    }


    public Purchase getItem(int position) {
        return mItems.get(position);
    }

    public void addData(Purchase newModelData, int position) {
        mItems.add(position, newModelData);
        notifyItemInserted(position);
    }

    public void removeData(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    public void clearData() {
        Log.v(TAG, "clearData()");
        int size = this.mItems.size();

        int index = 0;
        if (size > index) {
            for (int i = index; i < size; i++) {
                this.mItems.remove(index);
            }

            this.notifyItemRangeRemoved(index, size - 1);
        }

    }

    public class CustomFilter extends Filter {


        private CustomFilter() {
            super();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            List<Purchase> filteredList = new ArrayList<Purchase>();
            FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                mItems.clear();
                mItems.addAll(items2);
                filteredList.addAll(mItems);
            } else {

                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (Purchase mWords : items2) {
                    if (mWords.invoice.toLowerCase().startsWith(filterPattern)) {
                        filteredList.add(mWords);
                    }
                }

                mItems.clear();
                mItems = filteredList;
            }

            results.values = filteredList;
            results.count = filteredList.size();


            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            notifyDataSetChanged();
        }
    }

    public void deleteRecord(int id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Purchase c = realm.where(Purchase.class).equalTo("id", id).findFirst();

        int i = Integer.parseInt(System.currentTimeMillis()/1000+"");

        RealmList<Purchase> cl = new RealmList<>();
        cl.add(c);

        RealmList<Vendor> vl = new RealmList<>();
        vl.addAll(c.vendorList);

        RealmList<ItemModel> il = new RealmList<>();
        il.addAll(c.itemList);

        Transactions tObj = new Transactions(i,null,null,null,vl,il,cl, CommonActions.currentDateTime(),"Purchases deleted",c.invoice);
        realm.insertOrUpdate(tObj);


        if(c != null)
            c.deleteFromRealm();
        realm.commitTransaction();
        realm.close();
        notifyDataSetChanged();

        if(c != null)
            Snackbar.make(rc,"Record Deleted Successfully!",Snackbar.LENGTH_SHORT).show();
    }

    public void getEditDialog(final Purchase o) {

        purchaseEditDialog = new Purchase_Edit_Dialog().newInstance(act,o);
        FragmentManager manager = ((AppCompatActivity)act).getSupportFragmentManager();
        purchaseEditDialog.show(manager,"");
    }

}