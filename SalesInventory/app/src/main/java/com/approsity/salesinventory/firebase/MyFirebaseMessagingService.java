package com.approsity.salesinventory.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.notification.NotificationData;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import io.realm.Realm;

public class MyFirebaseMessagingService extends FirebaseMessagingService{

    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

         /*
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        og.e(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        Log.e(TAG, "Notification: " + remoteMessage.getNotification().getTitle());
        */

        //FCMCallBack.fcmOnCallback.getNotificationBody(remoteMessage.getNotification().getBody());

        Log.e("DATA -->",remoteMessage.getData()+" ");
        Log.e("TITLE -->",remoteMessage.getData().get("title")+" ");
        Log.e("MSG -->",remoteMessage.getData().get("message")+" ");

        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();

        Realm db = Realm.getDefaultInstance();
        db.beginTransaction();
        NotificationData noti1 = new NotificationData(Integer.parseInt(ts),remoteMessage.getData().get("title"),remoteMessage.getData().get("message"), CommonActions.currentDateTime(), CommonObjects.UNREAD);
        db.insertOrUpdate(noti1);
        db.commitTransaction();


        sendNotification(remoteMessage);

    }

    private void sendNotification(RemoteMessage messageBody) {

        Intent intent = new Intent(this, MyFirebaseMessagingService.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,"0")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(messageBody.getData().get("title"))
                .setContentText(messageBody.getData().get("message"))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setLights(Color.RED, 3000, 3000)
                .setContentIntent(null);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();

        notificationManager.notify(Integer.parseInt(ts), notificationBuilder.build());
    }

}