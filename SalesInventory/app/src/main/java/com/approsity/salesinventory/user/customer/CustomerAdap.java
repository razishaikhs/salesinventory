package com.approsity.salesinventory.user.customer;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.user.models.Customer;
import com.approsity.salesinventory.user.models.Vendor;
import com.approsity.salesinventory.user.vendor.VendorDetailViewPager;
import com.approsity.salesinventory.user.vendor.VendorDialog;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.approsity.salesinventory.utils.FreeActivity;
import com.viethoa.RecyclerViewFastScroller;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

public class CustomerAdap extends RecyclerView.Adapter<CustomerAdap.ViewHolder> implements RecyclerViewFastScroller.BubbleTextGetter, Filterable {

    private static final String TAG = CustomerAdap.class.getSimpleName();

    private List<Customer> mItems,items2;

    Activity act;

    private int position;

    private CustomFilter mFilter;

    CustomerDialog custDialog;

    RecyclerView rc;

    public CustomerAdap(Activity act, List<Customer> items, RecyclerView rc) {
        mItems = items;
        this.act = act;
        this.rc = rc;

        mFilter = new CustomFilter();
        this.items2 = new ArrayList<Customer>();
        this.items2.addAll(mItems);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int view_type) {

        View v = null;

        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.frag_vendorlist_row, viewGroup, false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int pos) {

        final Customer aObj = mItems.get(pos);

        if (pos % 2 == 1) {
            //viewHolder.rl2.setBackgroundColor(context.getResources().getColor(R.color.appointment_green));
        } else {
            //viewHolder.rl2.setBackgroundColor(context.getResources().getColor(R.color.appointment_blue));

            //viewHolder.tv_status.setTextColor(context.getResources().getColor(R.color.gray));
           // viewHolder.tv_status.setText("10AM-11AM");
        }



        viewHolder.text_name.setText(aObj.name);
        viewHolder.text_email.setText(aObj.email);
        viewHolder.text_address.setText(aObj.address);
        viewHolder.text_contactno.setText(aObj.contactno);

        final String email = viewHolder.text_email.getText().toString();

        viewHolder.text_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent mailer = new Intent(Intent.ACTION_SEND);
                mailer.setType("text/plain");
                mailer.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
                mailer.putExtra(Intent.EXTRA_SUBJECT, "Sales Inventory");
                mailer.putExtra(Intent.EXTRA_TEXT, "");
                act.startActivity(Intent.createChooser(mailer, "Send email..."));

            }
        });

        final String phone = viewHolder.text_contactno.getText().toString();
        viewHolder.text_contactno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                act.startActivity(intent);
            }
        });

        final String map = viewHolder.text_address.getText().toString();
        viewHolder.text_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q="+map));
                act.startActivity(intent);
            }
        });

        viewHolder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(act, R.style.MyDialogTheme);
                builder1.setMessage("Do you want to delete category ?");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                deleteRecord(aObj.id);
                            }
                        });
                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        viewHolder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getEditDialog(aObj);

            }
        });


        viewHolder.btn_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(act, FreeActivity.class);
                i.putExtra("frag", CommonObjects.OpenFragments.saleHistory);
                i.putExtra("custid", aObj.id);
                act.startActivity(i);
            }
        });


    }


    @Override
    public int getItemViewType(int position) {

       return 0;
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String getTextToShowInBubble(int pos) {

        if (pos < 0 || pos >= mItems.size())
            return null;

        String name = mItems.get(pos).name;
        if (name == null || name.length() < 1)
            return null;

        return mItems.get(pos).name.substring(0, 1);
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView text_name,text_email,text_address,text_contactno;
        TextView btn_delete,btn_edit,btn_info;

        View rootView;

        ViewHolder(View v) {
            super(v);

            text_name = (TextView) v.findViewById(R.id.text_name);
            text_email = (TextView) v.findViewById(R.id.text_email);
            text_address = (TextView) v.findViewById(R.id.text_address);
            text_contactno = (TextView) v.findViewById(R.id.text_contactno);

            btn_delete = (TextView) v.findViewById(R.id.btn_delete);
            btn_edit = (TextView) v.findViewById(R.id.btn_edit);
            btn_info = (TextView) v.findViewById(R.id.btn_info);

           // v.setOnClickListener(this);

            rootView = v;
        }

    }


    public Customer getItem(int position) {
        return mItems.get(position);
    }

    public void addData(Customer newModelData, int position) {
        mItems.add(position, newModelData);
        notifyItemInserted(position);
    }

    public void removeData(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    public void clearData() {
        Log.v(TAG, "clearData()");
        int size = this.mItems.size();

        int index = 0;
        if (size > index) {
            for (int i = index; i < size; i++) {
                this.mItems.remove(index);
            }

            this.notifyItemRangeRemoved(index, size - 1);
        }

    }

    public class CustomFilter extends Filter {


        private CustomFilter() {
            super();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            List<Customer> filteredList = new ArrayList<Customer>();
            FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                mItems.clear();
                mItems.addAll(items2);
                filteredList.addAll(mItems);
            } else {

                final String filterPattern = constraint.toString();

                for (Customer mWords : items2) {
                    if (mWords.contactno.trim().contains(filterPattern.trim())) {
                        filteredList.add(mWords);
                    }
                }

                mItems.clear();
                mItems = filteredList;
            }

            results.values = filteredList;
            results.count = filteredList.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            notifyDataSetChanged();
        }
    }

    public void deleteRecord(int id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Vendor v = realm.where(Vendor.class).equalTo("id", id).findFirst();

        /*int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
        RealmList<Vendor> vl = new RealmList<>();
        vl.add(v);
        Transactions tObj = new Transactions(i,null,null,null,vl,null,null, CommonActions.currentDateTime(),"Vendor deleted",v.name);
        realm.insertOrUpdate(tObj);*/


        if(v != null)
            v.deleteFromRealm();
        realm.commitTransaction();
        realm.close();
        notifyDataSetChanged();

        if(v != null)
            Snackbar.make(rc,"Record Deleted Successfully!",Snackbar.LENGTH_SHORT).show();

    }

    public void getEditDialog(final Customer o) {

        custDialog = new CustomerDialog().newInstance(act,o);
        custDialog.setButtonClicked(new CustomerDialog.onButtonClicked() {
            @Override
            public void onClicked(final String name, final String contact, final String email, final String address) {
                custDialog.dismiss();

                Realm realm = Realm.getDefaultInstance();
                Customer toEdit = realm.where(Customer.class).equalTo("id", o.id).findFirst();
                realm.beginTransaction();
                if(toEdit != null) {
                    toEdit.name = name;
                    toEdit.contactno = contact;
                    toEdit.address = address;
                    toEdit.email = email;
                }
                realm.commitTransaction();
                realm.close();
                notifyDataSetChanged();

                if(toEdit != null)
                   Snackbar.make(rc,"Record Updated Successfully!",Snackbar.LENGTH_SHORT).show();

                /*RealmList<Customer> vl = new RealmList<>();
                vl.add(toEdit);

                int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                Transactions tObj = new Transactions(i,null,null,null,vl,null,null, CommonActions.currentDateTime(),"Customer edited",toEdit.name);

                realm.beginTransaction();
                realm.insertOrUpdate(tObj);
                realm.commitTransaction();*/

            }
        });

        FragmentManager manager = ((AppCompatActivity)act).getSupportFragmentManager();
        custDialog.show(manager,"edit");
    }

}