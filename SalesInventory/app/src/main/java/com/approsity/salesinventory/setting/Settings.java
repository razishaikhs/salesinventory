package com.approsity.salesinventory.setting;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.SettingInjectorService;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.approsity.salesinventory.NavigationActivity;
import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.inapp.util.IabHelper;
import com.approsity.salesinventory.inapp.util.IabResult;
import com.approsity.salesinventory.inapp.util.Inventory;
import com.approsity.salesinventory.purchase.models.Purchase;
import com.approsity.salesinventory.user.LoginActivity;
import com.approsity.salesinventory.user.RegistrationActivity;
import com.approsity.salesinventory.user.User;
import com.approsity.salesinventory.utils.CSVData;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.approsity.salesinventory.utils.RealmBackupRestore;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by Raziuddin.Shaikh on 12/26/2017.
 */

public class Settings extends Fragment {

    View mView;

    public  static final int RequestPermissionCode  = 1;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    int PERMISSION  = 1;

    private static final String TAG = "BillingService";
    com.approsity.salesinventory.inapp.util.IabHelper mHelper;

    boolean uploadCheck;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_settings, container, false);

        Button btn_restore = (Button) mView.findViewById(R.id.btn_restore);
        Button btn_backup = (Button) mView.findViewById(R.id.btn_backup);
        Button btn_delete = (Button) mView.findViewById(R.id.btn_delete);
        Button btn_deletefile = (Button) mView.findViewById(R.id.btn_deletefile);

        btn_backup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                    requestPermissions(PERMISSIONS, PERMISSION);
                }
                else {
                    CSVData csv = new CSVData(getActivity(),mView);
                    csv.ItemToCSV();


                    int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                    Realm realm = Realm.getDefaultInstance();
                    Transactions tObj = new Transactions(i,null,null,null,null,null,null, CommonActions.currentDateTime(),"Items exported to item.csv file","Export Items");
                    realm.beginTransaction();
                    realm.insertOrUpdate(tObj);
                    realm.commitTransaction();
                    realm.close();
                }
            }
        });

        btn_restore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                    requestPermissions(PERMISSIONS, PERMISSION);
                }
                else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
                    builder1.setMessage("Are you sure you want to Restore Items ?");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();


                                    CSVData csv = new CSVData(getActivity(),mView);
                                    csv.CSVToItem();

                                    int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                                    Realm realm = Realm.getDefaultInstance();
                                    Transactions tObj = new Transactions(i,null,null,null,null,null,null, CommonActions.currentDateTime(),"Items imported from item.csv file","Import Items");
                                    realm.beginTransaction();
                                    realm.insertOrUpdate(tObj);
                                    realm.commitTransaction();
                                    realm.close();

                                }
                            });
                    builder1.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }

            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
                builder1.setMessage("Are you sure you want to Delete All Items ?");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                Realm realm = Realm.getDefaultInstance();
                                realm.beginTransaction();
                                RealmResults<ItemModel> r = realm.where(ItemModel.class).findAll();
                                if(r.size() > 0)
                                    r.deleteAllFromRealm();
                                realm.commitTransaction();
                                realm.close();

                                Snackbar.make(mView,"All items deleted successfully!",Snackbar.LENGTH_SHORT).show();

                                int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                                realm = Realm.getDefaultInstance();
                                Transactions tObj = new Transactions(i,null,null,null,null,null,null, CommonActions.currentDateTime(),"All Items deleted","Items Deleted");
                                realm.beginTransaction();
                                realm.insertOrUpdate(tObj);
                                realm.commitTransaction();
                                realm.close();
                            }
                        });
                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();


            }
        });

        btn_deletefile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
                builder1.setMessage("Are you sure you want to delete backup file ?");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                boolean check=false;

                                File itemCsv = new File(CommonObjects.appPath+File.separator+CSVData.items);
                                if(itemCsv.exists())
                                    check = itemCsv.delete();


                                if(check) {
                                    Snackbar.make(mView, "Backup items file deleted !", Snackbar.LENGTH_SHORT).show();

                                    int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                                    Realm realm = Realm.getDefaultInstance();
                                    Transactions tObj = new Transactions(i,null,null,null,null,null,null, CommonActions.currentDateTime(),"items.csv file deleted","Items File Deleted");
                                    realm.beginTransaction();
                                    realm.insertOrUpdate(tObj);
                                    realm.commitTransaction();
                                    realm.close();
                                }
                                else
                                    Snackbar.make(mView,"No backup file found!",Snackbar.LENGTH_SHORT).show();
                            }
                        });
                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();


            }
        });

        if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
            requestPermissions(PERMISSIONS, PERMISSION);
        }

        Button btn_restore_pur = (Button) mView.findViewById(R.id.btn_restore_pur);
        Button btn_backup_pur = (Button) mView.findViewById(R.id.btn_backup_pur);
        Button btn_delete_pur = (Button) mView.findViewById(R.id.btn_delete_pur);
        Button btn_deletefile_pur = (Button) mView.findViewById(R.id.btn_deletefile_pur);

        btn_backup_pur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                    requestPermissions(PERMISSIONS, PERMISSION);
                }
                else {
                    CSVData csv = new CSVData(getActivity(),mView);
                    csv.PurchaseToCSV();

                    int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                    Realm realm = Realm.getDefaultInstance();
                    Transactions tObj = new Transactions(i,null,null,null,null,null,null, CommonActions.currentDateTime(),"Purchases exported to purchase.csv file","Export Purchases");
                    realm.beginTransaction();
                    realm.insertOrUpdate(tObj);
                    realm.commitTransaction();
                    realm.close();
                }
            }
        });

        btn_restore_pur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                    requestPermissions(PERMISSIONS, PERMISSION);
                }
                else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
                    builder1.setMessage("Are you sure you want to restore Purchases ?");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();


                                    CSVData csv = new CSVData(getActivity(),mView);
                                    csv.CSVToPurchase();

                                    int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                                    Realm realm = Realm.getDefaultInstance();
                                    Transactions tObj = new Transactions(i,null,null,null,null,null,null, CommonActions.currentDateTime(),"Purchases imported from purchases.csv file","Import Purchases");
                                    realm.beginTransaction();
                                    realm.insertOrUpdate(tObj);
                                    realm.commitTransaction();
                                    realm.close();


                                }
                            });
                    builder1.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }

            }
        });

        btn_delete_pur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
                builder1.setMessage("Are you sure you want to DELETE all Purchases ?");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                Realm realm = Realm.getDefaultInstance();
                                realm.beginTransaction();
                                RealmResults<Purchase> p = realm.where(Purchase.class).findAll();
                                if(p.size() > 0)
                                    p.deleteAllFromRealm();
                                realm.commitTransaction();
                                realm.close();

                                int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                                realm = Realm.getDefaultInstance();
                                Transactions tObj = new Transactions(i,null,null,null,null,null,null, CommonActions.currentDateTime(),"All Purchases deleted","Purchases Deleted");
                                realm.beginTransaction();
                                realm.insertOrUpdate(tObj);
                                realm.commitTransaction();
                                realm.close();

                                Snackbar.make(mView,"All purchases deleted successfully!",Snackbar.LENGTH_SHORT).show();
                            }
                        });
                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();


            }
        });

        btn_deletefile_pur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
                builder1.setMessage("Are you sure you want to delete backup file ?");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                boolean check=false;

                                File purCsv = new File(CommonObjects.appPath+File.separator+CSVData.purchase);
                                if(purCsv.exists())
                                    check = purCsv.delete();


                                if(check) {
                                    Snackbar.make(mView, "Backup purchases file deleted !", Snackbar.LENGTH_SHORT).show();

                                    int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                                    Realm realm = Realm.getDefaultInstance();
                                    Transactions tObj = new Transactions(i,null,null,null,null,null,null, CommonActions.currentDateTime(),"purchases.csv file deleted","Purchases File Deleted");
                                    realm.beginTransaction();
                                    realm.insertOrUpdate(tObj);
                                    realm.commitTransaction();
                                    realm.close();
                                }
                                else
                                    Snackbar.make(mView,"No backup file found!",Snackbar.LENGTH_SHORT).show();
                            }
                        });
                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });


        Button btn_backup_db = (Button) mView.findViewById(R.id.btn_backup_db);
        Button btn_restore_db = (Button) mView.findViewById(R.id.btn_restore_db);
        Button btn_upload_db = (Button) mView.findViewById(R.id.btn_upload_db);
        Button btn_download_db = (Button) mView.findViewById(R.id.btn_download_db);

        btn_backup_db.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                    requestPermissions(PERMISSIONS, PERMISSION);
                }else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
                    builder1.setMessage("Are you sure you backup database ?");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    RealmBackupRestore rm = new RealmBackupRestore(getActivity());
                                    rm.backup();

                                }
                            });
                    builder1.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }

            }
        });

        btn_restore_db.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                    requestPermissions(PERMISSIONS, PERMISSION);
                }else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
                    builder1.setMessage("Are you sure you want to restore database ?");
                    builder1.setCancelable(true);
                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                                        requestPermissions(PERMISSIONS, PERMISSION);
                                    }
                                    else {

                                        File f = new File(CommonObjects.appPath+"/"+CommonObjects.REALM_FILE_NAME);
                                        if(f.exists()){

                                            RealmBackupRestore rm = new RealmBackupRestore(getActivity());
                                            rm.restore();

                                            Realm.getDefaultInstance().removeAllChangeListeners();
                                            Realm.getDefaultInstance().close();
                                            Realm.getDefaultInstance().init(getActivity());
                                            Realm.getDefaultInstance().refresh();

                                            Snackbar.make(mView, "POS is shutting down to restore database !", Snackbar.LENGTH_SHORT).show();

                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    getActivity().finish();
                                                    System.exit(0);
                                                }
                                            }, 3000);

                                        }else{
                                            Snackbar.make(mView, "No file found.", Snackbar.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            });
                    builder1.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            }
        });


        btn_upload_db.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                    requestPermissions(PERMISSIONS, PERMISSION);
                }
                else {

                    uploadCheck = true;
                    mHelper.queryInventoryAsync(mGotInventoryListener);
                }
            }
        });

        btn_download_db.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                    requestPermissions(PERMISSIONS, PERMISSION);
                }
                else {

                    uploadCheck = false;
                    mHelper.queryInventoryAsync(mGotInventoryListener);
                }
            }
        });



       setupInapp();

        return mView;
    }

    private void uploadDB() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        builder1.setMessage("Are you sure you want to upload database ?");
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        final ProgressDialog pDialog = CommonActions.createProgressDialog(getActivity());

                        if (CommonActions.isConnected(getActivity())) {

                            RealmBackupRestore rm = new RealmBackupRestore(getActivity());
                            rm.backup();

                            Uri file = Uri.fromFile(new File(CommonObjects.appPath + "/" + CommonObjects.REALM_FILE_NAME));
                            StorageReference riversRef = FirebaseStorage.getInstance().getReference().child(CommonObjects.firebaseStoragePath + CommonObjects.uObj.getEmail() + "/" + CommonObjects.REALM_FILE_NAME);
                            UploadTask uploadTask = riversRef.putFile(file);

                            // Register observers to listen for when the download is done or if it fails
                            uploadTask.addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle unsuccessful uploads

                                    Snackbar.make(mView, exception.getMessage(), Snackbar.LENGTH_SHORT).show();

                                    if (pDialog.isShowing())
                                        pDialog.dismiss();
                                }
                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                                    Uri downloadUrl = taskSnapshot.getDownloadUrl();

                                    Snackbar.make(mView, "Database uploaded successfully!", Snackbar.LENGTH_SHORT).show();

                                    if (pDialog.isShowing())
                                        pDialog.dismiss();
                                }
                            });
                        } else {
                            Snackbar.make(mView, "Please connect to the internet!", Snackbar.LENGTH_SHORT).show();
                        }


                    }
                });
        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void downloadDB(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
        builder1.setMessage("Are you sure you want to download database ?");
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        final ProgressDialog pDialog = CommonActions.createProgressDialog(getActivity());

                        if (CommonActions.isConnected(getActivity())) {

                            StorageReference islandRef = FirebaseStorage.getInstance().getReference().child(CommonObjects.firebaseStoragePath + CommonObjects.uObj.getEmail()+ "/" + CommonObjects.REALM_FILE_NAME);

                            final File localFile = new File(CommonObjects.appPath+"/"+CommonObjects.REALM_FILE_NAME);

                            islandRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                                    Snackbar.make(mView, "Database has downloaded to "+localFile.getPath(), Snackbar.LENGTH_SHORT).show();

                                    if (pDialog.isShowing())
                                        pDialog.dismiss();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {

                                    Snackbar.make(mView, exception.getMessage(), Snackbar.LENGTH_SHORT).show();

                                    if (pDialog.isShowing())
                                        pDialog.dismiss();
                                }
                            });
                        } else {
                            Snackbar.make(mView, "Please connect to the internet!", Snackbar.LENGTH_SHORT).show();
                        }
                    }
                });
        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    //CommonActions.snackMsgs(mView,"Permission Granted");

                } else {

                    CommonActions.snackMsgs(mView,"Permission Canceled");
                }
                break;
        }
    }



    private void setupInapp() {

        mHelper = new IabHelper(getActivity(), CommonObjects.base64EncodedPublicKey);
        mHelper.enableDebugLogging(true);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.e("In-App","Failure");
                    return;
                }else {
                    Log.e("In-App","Successfull");
                }
            }
        });
    }

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {

        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

            Log.e("Query Inventrory",result+"  ");

            if(result.toString().contains("OK"))
            {
                Log.e("Inventory Details",inventory.getPurchase(CommonObjects.SKUID)+" ");
                Log.e("Query Inventrory",inventory.getSkuDetails(CommonObjects.SKUID)+"  ");

                if (inventory.hasPurchase(CommonObjects.SKUID)) {
                    ///Already Purchased

                    if(uploadCheck)
                        uploadDB();
                    else
                        downloadDB();

                    if(CommonObjects.SKUID.equals("android.test.purchased"))
                       mHelper.consumeAsync(inventory.getPurchase(CommonObjects.SKUID), mConsumeFinishedListener);
                }
                else {
                    //Start Purchasing
                    mHelper.launchPurchaseFlow(getActivity(), CommonObjects.SKUID,CommonObjects.RC_REQUEST, mPurchaseFinishedListener);
                }
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.e("INNNN","INNNNNNNNNNNNNNNNNNNNNN");

        Log.e("TAG", "onActivityResult(" + requestCode + "," + resultCode + "," + data);

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.e(TAG, "onActivityResult handled by IABUtil.");
        }
    }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        @Override
        public void onIabPurchaseFinished(IabResult result, com.approsity.salesinventory.inapp.util.Purchase purchase) {

            Log.e(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            if (result.isFailure()) {
                Log.e("Purchase","Your Purchase Fail");
                return;
            }
            Log.e("Purchase successful.","Yes");

            if(uploadCheck)
                uploadDB();
            else
                downloadDB();

            if(CommonObjects.SKUID.equals("android.test.purchased"))
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(com.approsity.salesinventory.inapp.util.Purchase purchase, IabResult result) {

            if (result.isSuccess()) {

                Log.e("SUCCESS","CONSUME");
            }
            else {
                Log.e("Error while consuming: " , result+" ");
            }

        }
    };

    @Override
    public void onDestroy() {
        if (mHelper != null) mHelper.dispose();
        mHelper = null;

        super.onDestroy();
    }

}
