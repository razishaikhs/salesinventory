package com.approsity.salesinventory.add.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class SubCategory extends RealmObject {

    public SubCategory() {
    }

    public SubCategory(int id, String name, String alias, String desc) {
        this.id = id;
        this.name = name;
        this.alias = alias;
        this.desc = desc;
    }

    @PrimaryKey
    public int id;
    public String name;
    public String alias;
    public String desc;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
