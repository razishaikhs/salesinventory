package com.approsity.salesinventory.add.product;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.category.CategoryAdap;
import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.add.models.Product;
import com.approsity.salesinventory.add.models.SubCategory;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.viethoa.RecyclerViewFastScroller;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

public class ProductAdap extends RecyclerView.Adapter<ProductAdap.ViewHolder> implements RecyclerViewFastScroller.BubbleTextGetter, Filterable {

    private static final String TAG = CategoryAdap.class.getSimpleName();

    private List<Product> mItems,items2;

    Activity act;

    private int position;

    CommonObjects.RecyclerViewClickListener itemListener;

    private CustomFilter mFilter;

    ProductDialog addDialog;

    RecyclerView rc;

    public ProductAdap(Activity act, List<Product> items, CommonObjects.RecyclerViewClickListener itemListener, RecyclerView rc) {
        mItems = items;
        this.act = act;
        this.rc = rc;
        this.itemListener = itemListener;

        mFilter = new CustomFilter();
        this.items2 = new ArrayList<Product>();
        this.items2.addAll(mItems);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int view_type) {

        View v = null;

        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.frag_categorylist_row, viewGroup, false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int pos) {

        final Product aObj = mItems.get(pos);

        if (pos % 2 == 1) {
            //viewHolder.rl2.setBackgroundColor(context.getResources().getColor(R.color.appointment_green));
        } else {
            //viewHolder.rl2.setBackgroundColor(context.getResources().getColor(R.color.appointment_blue));

            //viewHolder.tv_status.setTextColor(context.getResources().getColor(R.color.gray));
           // viewHolder.tv_status.setText("10AM-11AM");
        }

        viewHolder.text_name.setText(aObj.name);
        viewHolder.text_alias.setText(aObj.alias);
        viewHolder.text_desc.setText(aObj.desc);

        viewHolder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(act, R.style.MyDialogTheme);
                builder1.setMessage("Do you want to delete product ?");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                deleteRecord(aObj.id);
                            }
                        });
                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        viewHolder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getEditDialog(aObj);

            }
        });




    }


    @Override
    public int getItemViewType(int position) {

       return 0;
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String getTextToShowInBubble(int pos) {

        if (pos < 0 || pos >= mItems.size())
            return null;

        String name = mItems.get(pos).name;
        if (name == null || name.length() < 1)
            return null;

        return mItems.get(pos).name.substring(0, 1);
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView text_name,text_alias,text_desc;
        TextView btn_delete,btn_edit;

        View rootView;

        ViewHolder(View v) {
            super(v);

            text_name = (TextView) v.findViewById(R.id.text_name);
            text_alias = (TextView) v.findViewById(R.id.text_alias);
            text_desc = (TextView) v.findViewById(R.id.text_desc);

            btn_delete = (TextView) v.findViewById(R.id.btn_delete);
            btn_edit = (TextView) v.findViewById(R.id.btn_edit);

            text_name.setOnClickListener(this);
            text_alias.setOnClickListener(this);
            text_desc.setOnClickListener(this);

            v.setOnClickListener(this);


            rootView = v;
        }

        @Override
        public void onClick(View v) {

            itemListener.recyclerViewListClicked(v, getLayoutPosition(),act);

           /* selectedPosition=getLayoutPosition();
            notifyDataSetChanged();*/

        }
    }


    public Product getItem(int position) {
        return mItems.get(position);
    }

    public void addData(Product newModelData, int position) {
        mItems.add(position, newModelData);
        notifyItemInserted(position);
    }

    public void removeData(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    public void clearData() {
        Log.v(TAG, "clearData()");
        int size = this.mItems.size();

        int index = 0;
        if (size > index) {
            for (int i = index; i < size; i++) {
                this.mItems.remove(index);
            }

            this.notifyItemRangeRemoved(index, size - 1);
        }

    }

    public class CustomFilter extends Filter {


        private CustomFilter() {
            super();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            List<Product> filteredList = new ArrayList<Product>();
            FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                mItems.clear();
                mItems.addAll(items2);
                filteredList.addAll(mItems);
            } else {

                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (Product mWords : items2) {
                    if (mWords.alias.toLowerCase().startsWith(filterPattern)) {
                        filteredList.add(mWords);
                    }
                }

                mItems.clear();
                mItems = filteredList;
            }

            results.values = filteredList;
            results.count = filteredList.size();


            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            notifyDataSetChanged();
        }
    }

    public void deleteRecord(int id) {

        int i = Integer.parseInt(System.currentTimeMillis()/1000+"");

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Product p = realm.where(Product.class).equalTo("id", id).findFirst();

        RealmList<Product> pl = new RealmList<>();
        pl.add(p);
        Transactions tObj = new Transactions(i,null,null,pl,null,null,null, CommonActions.currentDateTime(),"Product deleted",p.name);
        realm.insertOrUpdate(tObj);

        if(p != null)
            p.deleteFromRealm();
        realm.commitTransaction();
        realm.close();
        notifyDataSetChanged();
       /* NotificationData rows= Realm.getDefaultInstance().where(NotificationData.class).equalTo("user_id", id).findFirst();
        rows.deleteFromRealm();*/

        Snackbar.make(rc,"Record Deleted Successfully!",Snackbar.LENGTH_SHORT).show();
    }

    public void getEditDialog(final Product o) {

        addDialog = new ProductDialog().newInstance(act,o);
        addDialog.setButtonClicked(new ProductDialog.onButtonClicked() {
            @Override
            public void onClicked(final String name, final String alias, final String desc) {
                addDialog.dismiss();

                Realm realm = Realm.getDefaultInstance();
                Product toEdit = realm.where(Product.class).equalTo("id", o.id).findFirst();
                realm.beginTransaction();
                toEdit.name = name;
                toEdit.alias = alias;
                toEdit.desc = desc;
                realm.commitTransaction();
                realm.close();
                notifyDataSetChanged();

                Snackbar.make(rc,"Record Updated Successfully!",Snackbar.LENGTH_SHORT).show();

                RealmList<Product> pl = new RealmList<>();
                pl.add(toEdit);
                int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                Transactions tObj = new Transactions(i,null,null,pl,null,null,null, CommonActions.currentDateTime(),"Product edited",toEdit.name);
                realm.beginTransaction();
                realm.insertOrUpdate(tObj);
                realm.commitTransaction();
            }
        });

        FragmentManager manager = ((AppCompatActivity)act).getSupportFragmentManager();
        addDialog.show(manager,"edit");
    }

}