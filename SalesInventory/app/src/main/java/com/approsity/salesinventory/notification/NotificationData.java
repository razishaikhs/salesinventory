package com.approsity.salesinventory.notification;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class NotificationData extends RealmObject {

    public NotificationData() {
    }

    public NotificationData(int id, String title, String message, Date datetime, int status) {
        this.id = id;
        this.title = title;
        this.message = message;
        this.datetime = datetime;
        this.status = status;
    }

    @PrimaryKey
    public int id;
    public String title;
    public String message;
    public Date datetime;
    public int status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
