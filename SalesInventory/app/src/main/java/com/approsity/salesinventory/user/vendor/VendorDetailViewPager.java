package com.approsity.salesinventory.user.vendor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.item.ItemAdd;
import com.approsity.salesinventory.add.item.ItemList;
import com.approsity.salesinventory.utils.CommonObjects;

import java.util.ArrayList;
import java.util.List;


public class VendorDetailViewPager extends AppCompatActivity {

    SectionsPagerAdapter mSectionsPagerAdapter;
    TabLayout mSlidingTabLayout;
    ViewPager mViewPager;
    List<String> rList;

    int id;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager_with_toolbar);

        rList = new ArrayList<String>();
        rList.add("Purchases");
        rList.add("History");

        mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(0);
        mViewPager.setOffscreenPageLimit(rList.size() + 1);
        mSlidingTabLayout.setupWithViewPager(mViewPager);

        Bundle extras = getIntent().getExtras();
        id = extras.getInt("vid");

        ImageView backBtn = (ImageView) findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            /*FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.container,new Parents_Routes()).commit();
            Bundle args = new Bundle();
            args.putInt("pos", position);
            fragment.setArguments(args);*/

            Fragment fragment = null;
            Bundle args;

            switch(position) {
                case 0:
                    fragment = new VendorDetail();
                    args = new Bundle();
                    args.putInt("vid", id);
                    fragment.setArguments(args);
                    break;
                case 1:
                    fragment = new VendorHistory();
                    args = new Bundle();
                    args.putInt("vid", id);
                    fragment.setArguments(args);
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return rList.size();
        }

        @Override
        public int getItemPosition(Object object) {
            mSectionsPagerAdapter.notifyDataSetChanged();
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return " "+rList.get(position);
        }
    }

}