package com.approsity.salesinventory.firebase;

/**
 * Created by Raziuddin.Shaikh on 11/30/2017.
 */

public class FCMCallBack {

    public interface FCMInterface{
        void getNotificationBody(String body);
    }

    public static FCMInterface fcmOnCallback;
}
