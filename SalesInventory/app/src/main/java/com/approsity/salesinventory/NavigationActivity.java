package com.approsity.salesinventory;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.approsity.salesinventory.home.Home;
import com.approsity.salesinventory.setting.Settings;
import com.approsity.salesinventory.user.LoginActivity;
import com.approsity.salesinventory.utils.CommonObjects;
import com.approsity.salesinventory.utils.FreeActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.List;

public class NavigationActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<MenuGroupModel> groupList;

    View main_view;

    TextView name,email;

    LinearLayout ll_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_navigation);

        setupInAppSku();
        //SampleData.sampleRecords();

        main_view = (View) findViewById(R.id.main_view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);

        groupList = new ArrayList<>();
        groupList = MenuItems.getData();

        expandableListAdapter = new MenuAdapter(this, groupList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
              //  Toast.makeText(getApplicationContext(), groupList.get(groupPosition).title + " List Expanded.", Toast.LENGTH_SHORT).show();

                if(groupList.get(groupPosition).iList.size() == 0) {

                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);

                    goToFragment(groupList.get(groupPosition).frag, false);
                    setTitle(groupList.get(groupPosition).title);
                }
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
               // Toast.makeText(getApplicationContext(), groupList.get(groupPosition).title + " List Collapsed.", Toast.LENGTH_SHORT).show();

                if(groupList.get(groupPosition).iList.size() == 0) {

                    goToFragment(groupList.get(groupPosition).frag, false);
                    setTitle(groupList.get(groupPosition).title);

                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);
                }
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                //Toast.makeText(getApplicationContext(), groupList.get(groupPosition).title + " -> " + groupList.get(groupPosition).iList.get(childPosition).title, Toast.LENGTH_SHORT).show();

                goToFragment(groupList.get(groupPosition).iList.get(childPosition).frag, false);
                setTitle(groupList.get(groupPosition).iList.get(childPosition).title);

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                return false;
            }
        });


        goToFragment(new Home(), false);
        setTitle(groupList.get(0).title);

        name = (TextView) findViewById(R.id.name);
        email = (TextView) findViewById(R.id.email);

        name.setText(CommonObjects.uObj.userName);
        email.setText(CommonObjects.uObj.email);

        ImageView btn_logout = (ImageView) findViewById(R.id.btn_logout);
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(NavigationActivity.this, R.style.MyDialogTheme);
                builder1.setMessage("Are you sure you logout ?");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                FirebaseAuth.getInstance().signOut();

                                SharedPreferences app_preferences = getSharedPreferences("settingFile", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = app_preferences.edit();
                                editor.putString("email", "");
                                editor.commit();
                                editor.putString("pass", "");
                                editor.commit();

                                startActivity(new Intent(NavigationActivity.this, LoginActivity.class));
                                NavigationActivity.this.finish();

                            }
                        });
                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        ll_email = (LinearLayout) findViewById(R.id.ll_email);
        ll_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(NavigationActivity.this, FreeActivity.class);
                i.putExtra("frag", CommonObjects.OpenFragments.profile);
                startActivity(i);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        name.setText(CommonObjects.uObj.userName);
        email.setText(CommonObjects.uObj.email);


    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Snackbar.make(main_view, "Please click back again to close", Snackbar.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);

        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void goToFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.replace(R.id.container, fragment).commit();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(getSupportFragmentManager().findFragmentById(R.id.container) instanceof Settings) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        else
            super.onActivityResult(requestCode, resultCode, data);
    }

    private void setupInAppSku() {
        FirebaseDatabase.getInstance().getReference().child("inapp").child("skuid").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                CommonObjects.SKUID = snapshot.getValue().toString();
                Log.e("SKUID",CommonObjects.SKUID+" ");
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

}
