package com.approsity.salesinventory.sale;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.approsity.salesinventory.R;
import java.util.ArrayList;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;


/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class SaleHistory extends Fragment{

    View mView;

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;

    List<ExpendableSalesModel> parentList;
    List<SaleModel> tList,tempList;

    RelativeLayout rl_actions;
    TextView tv_norecord;

    String dateFormat="";
    String title;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_transactionhistory, container, false);

        tv_norecord = (TextView) mView.findViewById(R.id.tv_norecord);
        tv_norecord.setVisibility(View.VISIBLE);

        rl_actions = (RelativeLayout) mView.findViewById(R.id.rl_actions);
        rl_actions.setVisibility(View.GONE);

        expandableListView = (ExpandableListView) mView.findViewById(R.id.expandableListView);

        getData();

        return mView;
    }

    public void getData() {

        if(getArguments() == null) {
            Realm.getDefaultInstance().where(SaleModel.class)
                    .sort("dateTime", Sort.DESCENDING)
                    .findAllAsync()
                    .addChangeListener(new RealmChangeListener<RealmResults<SaleModel>>() {
                        @Override
                        public void onChange(RealmResults<SaleModel> notiList) {

                            if (notiList.size() > 0) {

                                setRc(notiList);

                                rl_actions.setVisibility(View.VISIBLE);
                                tv_norecord.setVisibility(View.GONE);
                            } else {
                                rl_actions.setVisibility(View.GONE);
                                tv_norecord.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        }
        else{
            Realm.getDefaultInstance().where(SaleModel.class)
                    .equalTo("custList.id",getArguments().getInt("custid"))
                    .sort("dateTime", Sort.DESCENDING)
                    .findAllAsync()
                    .addChangeListener(new RealmChangeListener<RealmResults<SaleModel>>() {
                        @Override
                        public void onChange(RealmResults<SaleModel> notiList) {

                            if (notiList.size() > 0) {

                                setRc(notiList);

                                rl_actions.setVisibility(View.VISIBLE);
                                tv_norecord.setVisibility(View.GONE);
                            } else {
                                rl_actions.setVisibility(View.GONE);
                                tv_norecord.setVisibility(View.VISIBLE);
                            }
                        }
                    });

        }
    }

    public void setRc(List<SaleModel> t){

        expandableListAdapter = new SaleHistoryAdapter(getActivity(), applyDateFilter(t));
        expandableListView.setAdapter(expandableListAdapter);
    }

    public List<ExpendableSalesModel> applyDateFilter(List<SaleModel> t){

        tList = new ArrayList<>();
        tList.addAll(t);

        parentList = new ArrayList<>();
        tempList = new ArrayList<>();

        int c=0;

        for(int i=0;i<tList.size();i++){

            String d = tList.get(i).dateTime+"";

            boolean hCheck;

            if(!dateFormat.equals(d.split(" ")[0]+" "+ d.split(" ")[1]+" "+d.split(" ")[2]+" "+d.split(" ")[5])){

                hCheck = true;

                if(c>0){
                    ExpendableSalesModel pObj = new ExpendableSalesModel(title,tempList);
                    parentList.add(pObj);
                }
                c++;

                title = d.split(" ")[0]+" "+ d.split(" ")[1]+" "+d.split(" ")[2]+" "+d.split(" ")[5];

            }else{
                hCheck = false;
            }

            if(hCheck){
                tempList = new ArrayList<>();
                tempList.add(tList.get(i));

                if(i+1 == tList.size()){
                    ExpendableSalesModel pObj = new ExpendableSalesModel(title,tempList);
                    parentList.add(pObj);
                }
            }
            else {
                tempList.add(tList.get(i));

                if(i+1 == tList.size()){
                    ExpendableSalesModel pObj = new ExpendableSalesModel(title,tempList);
                    parentList.add(pObj);
                }
            }

            dateFormat = d.split(" ")[0]+" "+ d.split(" ")[1]+" "+d.split(" ")[2]+" "+d.split(" ")[5];
        }

        return parentList;
    }

}
