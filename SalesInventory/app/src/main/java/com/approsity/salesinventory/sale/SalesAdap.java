package com.approsity.salesinventory.sale;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.category.CategoryDialog;
import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.viethoa.RecyclerViewFastScroller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

public class SalesAdap extends RecyclerView.Adapter<SalesAdap.ViewHolder> {

    private static final String TAG = SalesAdap.class.getSimpleName();

    private List<SaleItemModel> mItems;

    Activity act;

    private int position;

    boolean editCheck;


    public SalesAdap(Activity act, List<SaleItemModel> items, boolean c) {
        mItems = items;
        this.act = act;

        this.editCheck = c;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int view_type) {

        View v = null;

        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.frag_sales_row, viewGroup, false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int pos) {

        final SaleItemModel aObj = mItems.get(pos);

        if (pos % 2 == 1) {
            //viewHolder.rl2.setBackgroundColor(context.getResources().getColor(R.color.appointment_green));
        } else {
            //viewHolder.rl2.setBackgroundColor(context.getResources().getColor(R.color.appointment_blue));

            //viewHolder.tv_status.setTextColor(context.getResources().getColor(R.color.gray));
           // viewHolder.tv_status.setText("10AM-11AM");
        }

        viewHolder.tv_item.setText(aObj.itemname);
        viewHolder.tv_qty.setText(aObj.qty);
        viewHolder.tv_price.setText(aObj.price);

        if(!this.editCheck)
            viewHolder.btn_del.setVisibility(View.GONE);


        viewHolder.btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(act, R.style.MyDialogTheme);
                builder1.setMessage("Do you want to remove ?");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                removeData(pos);
                            }
                        });
                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });


        /*viewHolder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(act, R.style.MyDialogTheme);
                builder1.setMessage("Do you want to delete category ?");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                deleteRecord(aObj.id);
                            }
                        });
                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        viewHolder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               // getEditDialog(aObj);

            }
        });*/




    }


    @Override
    public int getItemViewType(int position) {

       return 0;
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView tv_item,tv_qty,tv_price;

        ImageView btn_del;

        View rootView;

        ViewHolder(View v) {
            super(v);

            tv_item = (TextView) v.findViewById(R.id.tv_item);
            tv_qty = (TextView) v.findViewById(R.id.tv_qty);
            tv_price = (TextView) v.findViewById(R.id.tv_price);

            btn_del = (ImageView) v.findViewById(R.id.btn_del);


            rootView = v;
        }
    }


    public SaleItemModel getItem(int position) {
        return mItems.get(position);
    }

    public void addData(SaleItemModel newModelData, int position) {
        mItems.add(position, newModelData);
        notifyItemInserted(position);
    }

    public void removeData(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    public void clearData() {
        Log.v(TAG, "clearData()");
        int size = this.mItems.size();

        int index = 0;
        if (size > index) {
            for (int i = index; i < size; i++) {
                this.mItems.remove(index);
            }

            this.notifyItemRangeRemoved(index, size - 1);
        }

    }

}