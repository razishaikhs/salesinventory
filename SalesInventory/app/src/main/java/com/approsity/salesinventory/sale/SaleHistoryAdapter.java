package com.approsity.salesinventory.sale;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.approsity.salesinventory.utils.FreeActivity;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Raziuddin.Shaikh on 12/15/2017.
 */

public class SaleHistoryAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<ExpendableSalesModel> groupList;


    public SaleHistoryAdapter(Context context, List<ExpendableSalesModel> groupList) {
        this.context = context;
        this.groupList = groupList;
    }

    @Override
    public SaleModel getChild(int listPosition, int expandedListPosition) {

        return this.groupList.get(listPosition).tList.get(expandedListPosition);
        //return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(final int listPosition, final int expandedListPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.frag_salehistory_child_row, null);
        }

        TextView text_time = (TextView) convertView.findViewById(R.id.text_time);
        TextView text_amount = (TextView) convertView.findViewById(R.id.text_amount);
        TextView btn_info = (TextView) convertView.findViewById(R.id.btn_info);
        TextView text_method = (TextView) convertView.findViewById(R.id.text_method);
        TextView text_customer = (TextView) convertView.findViewById(R.id.text_customer);
        TextView text_items = (TextView) convertView.findViewById(R.id.text_items);
        TextView text_qty = (TextView) convertView.findViewById(R.id.text_qty);
        TextView text_profit = (TextView) convertView.findViewById(R.id.text_profit);

        text_time.setText(CommonActions.dateToStringFormatWithSeconds(getChild(listPosition, expandedListPosition).dateTime).split(" ",2)[1]);

        String amount ="<i>Total Amount :</i>"+" "+ getChild(listPosition, expandedListPosition).total;
        String method ="<i>Pay by       :</i>"+" "+ getChild(listPosition, expandedListPosition).paymentmethod;
        String items  ="<i>Items        :</i>"+" "+ getChild(listPosition, expandedListPosition).saleItemsList.size();
        String profit  ="<i>Total Profit        :</i>"+" "+ getChild(listPosition, expandedListPosition).totalprofit;

        text_amount.setText(Html.fromHtml(amount), TextView.BufferType.SPANNABLE);
        text_method.setText(Html.fromHtml(method), TextView.BufferType.SPANNABLE);
        text_items.setText(Html.fromHtml(items), TextView.BufferType.SPANNABLE);
        text_profit.setText(Html.fromHtml(profit), TextView.BufferType.SPANNABLE);

        String q = "0";
        for(int i=0;i<getChild(listPosition, expandedListPosition).saleItemsList.size();i++){

            BigDecimal bd = new BigDecimal(getChild(listPosition, expandedListPosition).saleItemsList.get(i).qty);
            q = new BigDecimal(q).add(bd)+"";
        }

        String qty    ="<i>Qty          :</i>"+" "+ q;
        text_qty.setText(Html.fromHtml(qty), TextView.BufferType.SPANNABLE);

        try {
            text_customer.setVisibility(View.VISIBLE);
            String cust   ="<i>Customer     :</i>"+" "+ getChild(listPosition, expandedListPosition).custList.get(0).name;
            text_customer.setText(Html.fromHtml(cust), TextView.BufferType.SPANNABLE);
        }catch(Exception e){
            e.printStackTrace();
            text_customer.setVisibility(View.GONE);
        }

        btn_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(context, FreeActivity.class);
                i.putExtra("frag", CommonObjects.OpenFragments.saleHistoryDetail);
                i.putExtra("saleid", getChild(listPosition, expandedListPosition).id);
                context.startActivity(i);

            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.groupList.get(listPosition).tList.size();
       // return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
    }

    @Override
    public ExpendableSalesModel getGroup(int listPosition) {
        return this.groupList.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.groupList.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String listTitle = (String) getGroup(listPosition).title;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.frag_transhistory_group_row, null);
        }

        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.text_date);
        listTitleTextView.setText(listTitle);


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

}