package com.approsity.salesinventory.user.vendor;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.purchase.PurchaseAdap;
import com.approsity.salesinventory.purchase.PurchaseFilterDialog;
import com.approsity.salesinventory.purchase.models.Purchase;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.viethoa.RecyclerViewFastScroller;
import com.viethoa.models.AlphabetItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;



public class VendorDetail extends Fragment implements CommonObjects.RecyclerViewClickListener{

    View mView;

    RecyclerView rc;
    PurchaseAdap adap;
    LinearLayoutManager mLayoutManager;

    List<Purchase> cList;
    List<Purchase> filterList;

    RecyclerViewFastScroller fast_scroller;

    EditText et_search;

    RelativeLayout rl_actions;
    TextView tv_norecord;

    PurchaseFilterDialog filterDialog;

    int id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_categorylist, container, false);

        Bundle extras = getActivity().getIntent().getExtras();
        id = extras.getInt("vid");

        rc = (RecyclerView) mView.findViewById(R.id.rc);
        fast_scroller = (RecyclerViewFastScroller) mView.findViewById(R.id.fast_scroller);

        tv_norecord = (TextView) mView.findViewById(R.id.tv_norecord);
        tv_norecord.setVisibility(View.VISIBLE);

        rl_actions = (RelativeLayout) mView.findViewById(R.id.rl_actions);
        rl_actions.setVisibility(View.GONE);

        filterDialog = PurchaseFilterDialog.newInstance(getActivity());

        FloatingActionButton fab = (FloatingActionButton) mView.findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                filerDialog();
            }
        });

        et_search = (EditText) mView.findViewById(R.id.et_search);
        et_search.setHint("Search by invoice number");
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    adap.getFilter().filter(s.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        getData();


        return mView;
    }

    public void getData() {
        cList = new ArrayList<>();

        RealmResults l;
        l = Realm.getDefaultInstance().where(Purchase.class).equalTo("vendorList.id",id).findAllSortedAsync("datetime", Sort.DESCENDING);
        l.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<Purchase>>() {
            @Override
            public void onChange(RealmResults<Purchase> itemModels, OrderedCollectionChangeSet changeSet) {

                cList = itemModels;

                if(itemModels.size() > 0) {

                    setRc(cList);

                    rl_actions.setVisibility(View.VISIBLE);
                    tv_norecord.setVisibility(View.GONE);
                }
                else
                {
                    rl_actions.setVisibility(View.GONE);
                    tv_norecord.setVisibility(View.VISIBLE);
                }
            }
        });
        l.load();
    }

    public void setRc(List<Purchase> list){
        CommonObjects.VendorPurchaseCheck = true;

        adap = new PurchaseAdap(getActivity(), AddAlphabeticalFilter(Realm.getDefaultInstance().copyFromRealm(list)), this,rc);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rc.setLayoutManager(mLayoutManager);
        rc.setAdapter(adap);

        fast_scroller.setRecyclerView(rc);
    }

    public List<Purchase> AddAlphabeticalFilter(List<Purchase> catList) {

        ArrayList<AlphabetItem> mAlphabetItems = new ArrayList<>();
        List<String> strAlphabets = new ArrayList<>();
        for (int i = 0; i < catList.size(); i++) {
            String name = catList.get(i).alias.toUpperCase();
            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1);
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }

        fast_scroller.setUpAlphabet(mAlphabetItems);

        return catList;
    }

    @Override
    public void recyclerViewListClicked(View v, int position, Context con) {

    }

    public void filerDialog() {

        filterDialog.setButtonClicked(new PurchaseFilterDialog.onButtonClicked() {
            @Override
            public void onClicked(final int vendId, final int itemId, final String method, final String startDate, final String endDate) {
                filterDialog.dismiss();

                Realm db = Realm.getDefaultInstance();
                db.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        filterList = new ArrayList<>();

                        RealmResults<Purchase> items = realm.where(Purchase.class).findAll();

                        RealmQuery c = items.where().beginGroup();

                        if(vendId != -1) {
                            c = c.equalTo("vendorList.id",vendId);
                        }


                        if(itemId != -1) {
                            c = c.equalTo("itemList.id",itemId);
                        }

                        if(!TextUtils.isEmpty(method.trim().toString())) {

                            c = c.equalTo("method",method);
                        }

                        if(!TextUtils.isEmpty(startDate.trim().toString()) && !TextUtils.isEmpty(endDate.trim().toString())) {

                            try {
                                Date d1 = CommonActions.stringToDateFormat_onlyDate(startDate.trim().toString());
                                Date d2 = CommonActions.stringToDateFormat_onlyDate(endDate.trim().toString());

                                c = c.between("datetime", d1, d2);

                            } catch (Exception e) {
                                e.printStackTrace();
                                Snackbar.make(rl_actions,"ERROR",Snackbar.LENGTH_SHORT).show();
                            }

                        }
                        else if(!TextUtils.isEmpty(startDate.trim().toString())) {

                            try
                            {
                                Date d = CommonActions.stringToDateFormat(startDate.trim().toString()+ " 12:00:00 AM");
                                Date d2 = CommonActions.stringToDateFormat(startDate.trim().toString()+" 11:59:59 PM");

                                c = c.between("datetime", d, d2);

                            } catch (Exception e) {
                                e.printStackTrace();
                                Snackbar.make(rl_actions,"ERROR",Snackbar.LENGTH_SHORT).show();
                            }
                        }
                        else if(!TextUtils.isEmpty(endDate.trim().toString())) {

                            try {
                                Date d = CommonActions.stringToDateFormat(endDate.trim().toString()+ " 12:00:00 AM");
                                Date d2 = CommonActions.stringToDateFormat(endDate.trim().toString()+" 11:59:59 PM");
                                c = c.between("datetime", d, d2);
                            } catch (Exception e) {
                                e.printStackTrace();
                                Snackbar.make(rl_actions,"ERROR",Snackbar.LENGTH_SHORT).show();
                            }
                        }

                        if(vendId == -1 && itemId == -1 && TextUtils.isEmpty(method.trim().toString()) && TextUtils.isEmpty(startDate.trim().toString()) && TextUtils.isEmpty(endDate.trim().toString())) {
                            setRc(cList);
                        }
                        else {
                            items = c.endGroup().findAll();
                            filterList = realm.copyToRealmOrUpdate(items);

                            if(items.size() > 0){
                                setRc(filterList);
                            }
                            else
                                Snackbar.make(rl_actions,"No Record Found!",Snackbar.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        });

        filterDialog.show(getFragmentManager(),"");
    }

}

