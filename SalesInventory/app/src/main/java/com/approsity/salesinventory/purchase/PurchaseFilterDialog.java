package com.approsity.salesinventory.purchase;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.user.models.Vendor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;


public class PurchaseFilterDialog extends DialogFragment implements View.OnClickListener{

    View v;

    List<Vendor> vList;
    List<ItemModel> iList;

    Spinner spinner_vend,spinner_item;
    EditText et_payby,et_sdate,et_edate;

    Button btn_add;

    Activity act;

    ImageView iv_edate,iv_sdate;

    int year, month, day;

    private onButtonClicked buttonClicked;

    public static PurchaseFilterDialog newInstance(Activity activity) {

        PurchaseFilterDialog frag = new PurchaseFilterDialog();
        frag.act = activity;
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setStyle(STYLE_NO_FRAME, R.style.DialogTheme);
        setCancelable(true);

    }


    public View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.dialog_purchase_filter, container, false);

        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        btn_add = (Button) v.findViewById(R.id.btn_add);
        btn_add.setOnClickListener(this);
        btn_add.setText("Filter");

        et_payby = (EditText) v.findViewById(R.id.et_payby);
        et_sdate = (EditText) v.findViewById(R.id.et_sdate);
        et_edate = (EditText) v.findViewById(R.id.et_edate);

        spinner_vend = (Spinner) v.findViewById(R.id.spinner_vend);
        spinner_item = (Spinner) v.findViewById(R.id.spinner_item);

        iv_edate = (ImageView) v.findViewById(R.id.iv_edate);
        iv_sdate = (ImageView) v.findViewById(R.id.iv_sdate);

        setSpinnerData();

        setOperations();

        return v;
    }

    private void setOperations() {

        iv_sdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                et_sdate.setText("");
            }
        });

        iv_edate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                et_edate.setText("");
            }
        });

        et_sdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate(et_sdate);
            }
        });

        et_edate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate(et_edate);
            }
        });
    }

    public void setSpinnerData() {
        getRealmVendor();
        getRealmItem();
    }

    public void getRealmVendor() {

        vList = new ArrayList<>();
        RealmResults<Vendor> c = Realm.getDefaultInstance().where(Vendor.class).findAllSortedAsync("name",  Sort.ASCENDING);
        vList = Realm.getDefaultInstance().copyFromRealm(c);

        List<String> vlist = new ArrayList<>();
        vlist.add("No Selection");
        for(int i=0;i<vList.size();i++) {
            vlist.add(vList.get(i).name);
        }

        ArrayAdapter<String> spinner_vAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,vlist);
        spinner_vend.setAdapter(spinner_vAdap);

    }

    public void getRealmItem() {

        iList = new ArrayList<>();
        RealmResults<ItemModel> sc =  Realm.getDefaultInstance().where(ItemModel.class).findAllSortedAsync("name",  Sort.ASCENDING);
        iList =  Realm.getDefaultInstance().copyFromRealm(sc);

        List<String> ilist = new ArrayList<>();
        ilist.add("No Selection");
        for(int i=0;i<iList.size();i++) {
            ilist.add(iList.get(i).name);
        }

        ArrayAdapter<String> spinner_iAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,ilist);
        spinner_item.setAdapter(spinner_iAdap);
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()) {
            case R.id.btn_add:
                FilterButton();
                break;
            default:
        }
    }

    public void FilterButton() {

        int v,i;

        if(spinner_vend.getSelectedItemPosition() == 0)
            v = -1;
        else
            v = vList.get(spinner_vend.getSelectedItemPosition() - 1).getId();

        if(spinner_item.getSelectedItemPosition() == 0)
            i = -1;
        else
            i = iList.get(spinner_item.getSelectedItemPosition() - 1).getId();


        buttonClicked.onClicked(v,i,et_payby.getText().toString(),et_sdate.getText().toString(),et_edate.getText().toString());
    }

    public void setButtonClicked(onButtonClicked buttonClicked) {
        this.buttonClicked = buttonClicked;
    }


    public interface onButtonClicked {
        public void onClicked(int vendId, int itemId, String method, String startDate, String EndDate);
    }

    public void setDate(final EditText et) {
        //showDate(year, month+1, day);
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
                        // TODO Auto-generated method stub
                        // arg1 = year
                        // arg2 = month
                        // arg3 = day
                        int month = arg2+1;
                        StringBuilder sb = new StringBuilder().append(arg3).append("/").append(month).append("/").append(arg1);
                        et.setText(sb.toString());

                    }
                }, year, month, day);

        datePicker.show();
    }

}

