package com.approsity.salesinventory;

import com.approsity.salesinventory.add.category.CategoryList;
import com.approsity.salesinventory.add.item.ItemViewPager;
import com.approsity.salesinventory.add.product.ProductList;
import com.approsity.salesinventory.add.subcategory.SubCategoryList;
import com.approsity.salesinventory.history.TransactionsHistory;
import com.approsity.salesinventory.home.Home;
import com.approsity.salesinventory.notification.NotificationFrag;
import com.approsity.salesinventory.purchase.PurchaseViewPager;
import com.approsity.salesinventory.sale.SaleHistory;
import com.approsity.salesinventory.sale.SaleHistoryViewPager;
import com.approsity.salesinventory.sale.SalesList;
import com.approsity.salesinventory.setting.Settings;
import com.approsity.salesinventory.user.customer.CustomerList;
import com.approsity.salesinventory.user.vendor.VendorList;

import java.util.ArrayList;

import java.util.List;

/**
 * Created by Raziuddin.Shaikh on 12/15/2017.
 */

public class MenuItems {


    public static List<MenuGroupModel> getData() {

        List<MenuGroupModel> gList = new ArrayList<>();

        List<MenuItemsModel> iList = new ArrayList<>();

        iList = new ArrayList<>();
        MenuGroupModel groupObj = new MenuGroupModel("HOME", R.drawable.ic_home,iList,new Home());
        gList.add(groupObj);

        iList = new ArrayList<>();
        groupObj = new MenuGroupModel("SALES", R.drawable.ic_sale,iList,new SalesList());
        gList.add(groupObj);

        iList = new ArrayList<>();
        groupObj = new MenuGroupModel("SALES HISTORY", R.drawable.ic_salehistory,iList,new SaleHistoryViewPager());
        gList.add(groupObj);

        iList = new ArrayList<>();
        iList.add(new MenuItemsModel("Categories", 0, new CategoryList()));
        iList.add(new MenuItemsModel("Sub Categories", 0, new SubCategoryList()));
        iList.add(new MenuItemsModel("Products", 0, new ProductList()));
        groupObj = new MenuGroupModel("INVENTORY",R.drawable.ic_inventory,iList, null);
        gList.add(groupObj);

        iList = new ArrayList<>();
        groupObj = new MenuGroupModel("ITEMS", R.drawable.ic_items,iList,new ItemViewPager());
        gList.add(groupObj);

        iList = new ArrayList<>();
        groupObj = new MenuGroupModel("PURCHASES",R.drawable.ic_purchase,iList, new PurchaseViewPager());
        gList.add(groupObj);

        iList = new ArrayList<>();
        groupObj = new MenuGroupModel("VENDOR",R.drawable.ic_vendor,iList, new VendorList());
        gList.add(groupObj);

        iList = new ArrayList<>();
        groupObj = new MenuGroupModel("CUSTOMER",R.drawable.ic_users,iList, new CustomerList());
        gList.add(groupObj);

        iList = new ArrayList<>();
        groupObj = new MenuGroupModel("HISTORY",R.drawable.ic_history,iList, new TransactionsHistory());
        gList.add(groupObj);

        iList = new ArrayList<>();
        groupObj = new MenuGroupModel("NOTIFICATIONS",R.drawable.ic_noti,iList, new NotificationFrag());
        gList.add(groupObj);

        iList = new ArrayList<>();
        groupObj = new MenuGroupModel("SETTINGS",R.drawable.ic_settings,iList, new Settings());
        gList.add(groupObj);

        return gList;
    }
}
