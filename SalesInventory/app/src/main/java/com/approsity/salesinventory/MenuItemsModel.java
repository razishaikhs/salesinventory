package com.approsity.salesinventory;

import android.support.v4.app.Fragment;

/**
 * Created by Raziuddin.Shaikh on 12/15/2017.
 */

public class MenuItemsModel {

    public String title;
    public int drawable;
    public Fragment frag;

    public MenuItemsModel(String title, int drawable, Fragment frag) {
        this.title = title;
        this.drawable = drawable;
        this.frag = frag;
    }

    @Override
    public String toString() {
        return "MenuItemsModel{" +
                "title='" + title + '\'' +
                ", drawable=" + drawable +
                ", frag=" + frag +
                '}';
    }
}
