package com.approsity.salesinventory.add.item;

import android.content.ClipData;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.category.CategoryDialog;
import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.add.models.Product;
import com.approsity.salesinventory.add.models.SubCategory;
import com.approsity.salesinventory.add.product.ProductDialog;
import com.approsity.salesinventory.add.subcategory.SubCategoryDialog;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.utils.CommonActions;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Umair on 17/12/2017.
 */

public class ItemAdd extends Fragment implements View.OnClickListener{

    View mView;

    List<Category> cList;
    List<SubCategory> scList;
    List<Product> pList;

    Spinner spinner_cat,spinner_subcat,spinner_product;
    ImageView btn_add_cat,btn_add_subcat,btn_add_product;

    EditText et_name,et_alias,et_desc,et_type,et_size,et_quantity,et_costperunit,et_totalprice,et_saleprice,et_barcode;

    Button btn_add;

    ImageView iv_barcode;

    LinearLayout ll_spinner_cat,ll_spinner_subcat, ll_spinner_product;

    ScrollView sv;

    Animation shake;

    String bCode_cat="0000", bCode_subcat="0000", bCode_product="0000", bCode_item="0000";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.dialog_item_add, container, false);

        shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);

        spinner_cat = (Spinner) mView.findViewById(R.id.spinner_cat);
        spinner_subcat = (Spinner) mView.findViewById(R.id.spinner_subcat);
        spinner_product = (Spinner) mView.findViewById(R.id.spinner_product);

        btn_add_cat = (ImageView) mView.findViewById(R.id.btn_add_cat);
        btn_add_cat.setOnClickListener(this);
        btn_add_subcat = (ImageView) mView.findViewById(R.id.btn_add_subcat);
        btn_add_subcat.setOnClickListener(this);
        btn_add_product = (ImageView) mView.findViewById(R.id.btn_add_product);
        btn_add_product.setOnClickListener(this);

        et_name = (EditText) mView.findViewById(R.id.et_name);
        et_alias = (EditText) mView.findViewById(R.id.et_alias);
        et_desc = (EditText) mView.findViewById(R.id.et_desc);
        et_type = (EditText) mView.findViewById(R.id.et_type);
        et_size = (EditText) mView.findViewById(R.id.et_size);
        et_quantity = (EditText) mView.findViewById(R.id.et_quantity);
        et_costperunit = (EditText) mView.findViewById(R.id.et_costperunit);
        et_totalprice = (EditText) mView.findViewById(R.id.et_totalprice);
        et_saleprice = (EditText) mView.findViewById(R.id.et_saleprice);
        et_barcode = (EditText) mView.findViewById(R.id.et_barcode);

        btn_add = (Button) mView.findViewById(R.id.btn_add);
        btn_add.setOnClickListener(this);

        iv_barcode = (ImageView) mView.findViewById(R.id.iv_barcode);

        ll_spinner_cat = mView.findViewById(R.id.ll_spinner_cat);
        ll_spinner_subcat = mView.findViewById(R.id.ll_spinner_subcat);
        ll_spinner_product = mView.findViewById(R.id.ll_spinner_product);

        sv = (ScrollView) mView.findViewById(R.id.sv);

        getRealmData();

        setOperations();

        return mView;
    }

    public void getRealmData() {

        getRealmCategory();
        getRealmSubCategory();
        getRealmProduct();
        getRealmItemId();
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()) {
            case R.id.btn_add_cat:
                catDialog();
                break;
            case R.id.btn_add_subcat:
                subcatDialog();
                break;
            case R.id.btn_add_product:
                productDialog();
                break;
            case R.id.btn_add:
                AddButton();
                break;
            default:
        }
    }

    public void catDialog() {

        final CategoryDialog categoryDialog = CategoryDialog.newInstance(getActivity(), null);
        categoryDialog.setButtonClicked(new CategoryDialog.onButtonClicked() {
            @Override
            public void onClicked(final String name, final String alias, final String desc) {
                categoryDialog.dismiss();

                Realm db = Realm.getDefaultInstance();
                db.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        Number currentIdNum = realm.where(Category.class).max("id");
                        int nextId = 0;
                        if(currentIdNum == null) {
                            nextId = 1;
                        } else {
                            nextId = currentIdNum.intValue() + 1;
                        }

                        Category fvData = new Category(nextId, name, alias ,desc);
                        realm.insert(fvData);

                        Snackbar.make(mView,"Category Added Successfully!",Snackbar.LENGTH_SHORT).show();

                        RealmList<Category> cl = new RealmList<>();
                        cl.add(fvData);
                        int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                        Transactions tObj = new Transactions(i,cl,null,null,null,null,null, CommonActions.currentDateTime(),"Category created",fvData.name);
                        realm.insertOrUpdate(tObj);

                        getRealmCategory();

                    }
                });
            }
        });

        categoryDialog.show(getFragmentManager(),"add");
    }

    public void subcatDialog() {

        final SubCategoryDialog addDialog = SubCategoryDialog.newInstance(getActivity(), null);
        addDialog.setButtonClicked(new SubCategoryDialog.onButtonClicked() {
            @Override
            public void onClicked(final String name, final String alias, final String desc) {
                addDialog.dismiss();

                Realm db = Realm.getDefaultInstance();
                db.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        Number currentIdNum = realm.where(SubCategory.class).max("id");
                        int nextId = 0;
                        if(currentIdNum == null) {
                            nextId = 1;
                        } else {
                            nextId = currentIdNum.intValue() + 1;
                        }

                        SubCategory fvData = new SubCategory(nextId, name, alias ,desc);
                        realm.insert(fvData);

                        Snackbar.make(mView,"SubCategory Added Successfully!",Snackbar.LENGTH_SHORT).show();

                        RealmList<SubCategory> cl = new RealmList<>();
                        cl.add(fvData);
                        int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                        Transactions tObj = new Transactions(i,null,cl,null,null,null,null, CommonActions.currentDateTime(),"Sub category created",fvData.name);
                        realm.insertOrUpdate(tObj);

                        getRealmSubCategory();
                    }
                });
            }
        });

        addDialog.show(getFragmentManager(),"add");
    }

    public void productDialog() {

        final ProductDialog addDialog = ProductDialog.newInstance(getActivity(), null);
        addDialog.setButtonClicked(new ProductDialog.onButtonClicked() {
            @Override
            public void onClicked(final String name, final String alias, final String desc) {
                addDialog.dismiss();

                Realm db = Realm.getDefaultInstance();
                db.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        Number currentIdNum = realm.where(Product.class).max("id");
                        int nextId = 0;
                        if(currentIdNum == null) {
                            nextId = 1;
                        } else {
                            nextId = currentIdNum.intValue() + 1;
                        }

                        Product fvData = new Product(nextId, name, alias ,desc);
                        realm.insert(fvData);

                        Snackbar.make(mView,"Product Added Successfully!",Snackbar.LENGTH_SHORT).show();

                        RealmList<Product> cl = new RealmList<>();
                        cl.add(fvData);
                        int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                        Transactions tObj = new Transactions(i,null,null,cl,null,null,null, CommonActions.currentDateTime(),"Product created",fvData.name);
                        realm.insertOrUpdate(tObj);

                        getRealmProduct();
                    }
                });
            }
        });

        addDialog.show(getFragmentManager(),"add");
    }

    public void getRealmCategory() {
        cList = new ArrayList<>();
        RealmResults<Category> c = Realm.getDefaultInstance().where(Category.class).sort("name",Sort.ASCENDING).findAllAsync();
        cList = Realm.getDefaultInstance().copyFromRealm(c);

        List<String> slist = new ArrayList<>();
        for(Category cat : cList) {
            slist.add(cat.name);
        }
        ArrayAdapter<String> spinner_catAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,slist);
        spinner_cat.setAdapter(spinner_catAdap);

        spinner_cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                bCode_cat = String.format("%04d", cList.get(position).id);
                barCodeGenerator();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        if(cList.size() > 0) {
            bCode_cat = String.format("%04d", cList.get(0).id);
            barCodeGenerator();
        }
    }

    public void getRealmSubCategory() {
        scList = new ArrayList<>();
        RealmResults<SubCategory> sc =  Realm.getDefaultInstance().where(SubCategory.class).sort("name",  Sort.ASCENDING).findAllAsync();
        scList =  Realm.getDefaultInstance().copyFromRealm(sc);

        List<String> sclist = new ArrayList<>();
        for(SubCategory scat : scList) {
            sclist.add(scat.name);
        }
        ArrayAdapter<String> spinner_scatAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,sclist);
        spinner_subcat.setAdapter(spinner_scatAdap);

        spinner_subcat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                bCode_subcat = String.format("%04d", scList.get(position).id);
                barCodeGenerator();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        if(scList.size() > 0) {
            bCode_subcat = String.format("%04d", scList.get(0).id);
            barCodeGenerator();
        }
    }

    public void getRealmProduct() {

        pList = new ArrayList<>();
        RealmResults<Product> pObj = Realm.getDefaultInstance().where(Product.class).sort("name",  Sort.ASCENDING).findAllAsync();
        pList = Realm.getDefaultInstance().copyFromRealm(pObj);

        List<String> plist = new ArrayList<>();
        for(Product p : pList) {
            plist.add(p.name);
        }
        ArrayAdapter<String> spinner_productAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,plist);
        spinner_product.setAdapter(spinner_productAdap);

        spinner_product.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                bCode_product = String.format("%04d", pList.get(position).id);
                barCodeGenerator();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        if(pList.size() > 0) {
            bCode_product = String.format("%04d", pList.get(0).id);
            barCodeGenerator();
        }
    }

    public void getRealmItemId() {

        Number currentIdNum = Realm.getDefaultInstance().where(ItemModel.class).max("id");

        int nextId = 0;
        if(currentIdNum == null) {
            nextId = 1;
        } else {
            nextId = currentIdNum.intValue() + 1;
        }

        bCode_item = String.format("%04d", nextId);
        barCodeGenerator();
    }

    public void setOperations() {

       // et_quantity.setText("1.0");
        et_quantity.setText("0.0");
        et_costperunit.setText("0.0");
        et_saleprice.setText("0.0");

        et_totalprice.setText(calculateBigDecimal(et_quantity.getText().toString(),et_costperunit.getText().toString())+"");

        et_quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(!TextUtils.isEmpty(et_quantity.getText().toString()) && !TextUtils.isEmpty(et_costperunit.getText().toString())) {
                    et_totalprice.setText(calculateBigDecimal(et_quantity.getText().toString(),et_costperunit.getText().toString())+"");
                }
            }
        });

        et_quantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b) {
                    /*if(TextUtils.isEmpty(et_quantity.getText().toString())){
                        et_quantity.setText("1.0");
                    }

                    BigDecimal bd = new BigDecimal(et_quantity.getText().toString());

                    if(bd.doubleValue() <= 0){
                        et_quantity.setText("1.0");
                    }*/

                    if(TextUtils.isEmpty(et_quantity.getText().toString())){
                        et_quantity.setText("0.0");
                    }

                    BigDecimal bd = new BigDecimal(et_quantity.getText().toString());

                    if(bd.doubleValue() <= 0){
                        et_quantity.setText("0.0");
                    }
                }
            }
        });

        et_costperunit.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(!TextUtils.isEmpty(et_quantity.getText().toString()) && !TextUtils.isEmpty(et_costperunit.getText().toString())) {
                    BigDecimal b1 = new BigDecimal(et_quantity.getText().toString());
                    BigDecimal b2 = new BigDecimal(et_costperunit.getText().toString());
                    BigDecimal b = b1.multiply(b2);
                    et_totalprice.setText(b+"");
                }
            }
        });

        et_costperunit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b) {

                    if(TextUtils.isEmpty(et_costperunit.getText().toString())){
                        et_costperunit.setText("0.0");
                    }

                    BigDecimal bd = new BigDecimal(et_costperunit.getText().toString());

                    if(bd.doubleValue() <= 0){
                        et_costperunit.setText("0.0");
                    }
                }
            }
        });

        et_saleprice.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b) {

                    if(TextUtils.isEmpty(et_saleprice.getText().toString())){
                        et_saleprice.setText("0.0");
                    }

                    BigDecimal bd = new BigDecimal(et_saleprice.getText().toString());

                    if(bd.doubleValue() <= 0){
                        et_saleprice.setText("0.0");
                    }
                }
            }
        });
    }

    public BigDecimal calculateBigDecimal(String s1, String s2) {
        BigDecimal b1 = new BigDecimal(s1);
        BigDecimal b2 = new BigDecimal(s2);
        BigDecimal b = b1.multiply(b2);

        return b;
    }

    public void barCodeGenerator() {

        et_barcode.setText(bCode_cat+bCode_subcat +bCode_product+bCode_item);

        String text= et_barcode.getText().toString();

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.CODE_128,600,150);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            iv_barcode.setImageBitmap(bitmap);

//            bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE,100,100);
//            barcodeEncoder = new BarcodeEncoder();
//            bitmap = barcodeEncoder.createBitmap(bitMatrix);
//            BitmapDrawable drawableEnd = new BitmapDrawable(getResources(), bitmap);
//            et_barcode.setCompoundDrawablesWithIntrinsicBounds(null,null, drawableEnd,null);

        } catch (WriterException e) {
            e.printStackTrace();
        }

    }

    public void AddButton() {

        if(cList.size() == 0)
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, ll_spinner_cat.getTop());
                }
            });

            Snackbar.make(mView,"Please add Category",Snackbar.LENGTH_SHORT).show();
            ll_spinner_cat.startAnimation(shake);
        }
        else if(scList.size() == 0)
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, ll_spinner_subcat.getTop());
                }
            });
            Snackbar.make(mView,"Please add Sub Category",Snackbar.LENGTH_SHORT).show();
            ll_spinner_subcat.startAnimation(shake);
        }
        else if(pList.size() == 0)
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, ll_spinner_product.getTop());
                }
            });
            Snackbar.make(mView,"Please add Product",Snackbar.LENGTH_SHORT).show();
            ll_spinner_product.startAnimation(shake);
        }
        else if(TextUtils.isEmpty(et_name.getText().toString()))
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, et_name.getTop());
                }
            });
            et_name.requestFocus();
            et_name.setError("Please add the name");
            et_name.startAnimation(shake);
        }
        /*else if(TextUtils.isEmpty(et_barcode.getText().toString()))
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, et_barcode.getTop());
                }
            });
            et_barcode.requestFocus();
            et_barcode.setError("Please add the barcode");
            et_barcode.startAnimation(shake);
        }
        else if(checkBarcode(et_barcode.getText().toString()))
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, et_barcode.getTop());
                }
            });
            et_barcode.requestFocus();
            et_barcode.setError("Already taken");
            et_barcode.startAnimation(shake);
        }*/
        else
        {
            Realm db = Realm.getDefaultInstance();
            db.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    Category cObj = cList.get(spinner_cat.getSelectedItemPosition());
                    SubCategory scObj = scList.get(spinner_subcat.getSelectedItemPosition());
                    Product pObj = pList.get(spinner_product.getSelectedItemPosition());

                    String name = et_name.getText().toString().toUpperCase();
                    String alis = et_alias.getText().toString().toUpperCase();
                    String desc = et_desc.getText().toString();
                    String type = et_type.getText().toString();
                    String size = et_size.getText().toString();

                    String quantity = et_quantity.getText().toString();
                    String cpu = et_costperunit.getText().toString();
                    String tp = et_totalprice.getText().toString();
                    String sp = et_saleprice.getText().toString();
                    String barCode = et_barcode.getText().toString();

                    RealmList<Category> c = new RealmList<>();
                    c.add(cObj);

                    RealmList<SubCategory> sc = new RealmList<>();
                    sc.add(scObj);

                    RealmList<Product> p = new RealmList<>();
                    p.add(pObj);

                    ItemModel i = new ItemModel(Integer.parseInt(bCode_item),c,sc,p,name,alis,desc,type,size,quantity,cpu,tp,sp,barCode, CommonActions.currentDateTime());
                    realm.insertOrUpdate(i);

                    Snackbar.make(mView,name + " item record added successfully!",Snackbar.LENGTH_SHORT).show();

                    et_name.setText("");
                    et_alias.setText("");
                    et_desc.setText("");
                    et_type.setText("");
                    et_size.setText("");

                    et_quantity.setText("0");
                    et_costperunit.setText("0.0");
                    et_totalprice.setText("0.0");
                    et_saleprice.setText("0.0");

                    bCode_item = String.format("%04d", Integer.parseInt(bCode_item) + 1);
                    barCodeGenerator();

                    RealmList<ItemModel> il = new RealmList<>();
                    il.add(i);
                    int id = Integer.parseInt(System.currentTimeMillis()/1000+"");
                    Transactions tObj = new Transactions(id,c,sc,p,null,il,null, CommonActions.currentDateTime(),"Item created",i.name);
                    realm.insertOrUpdate(tObj);

                }
            });
        }

    }

    /*public boolean checkBarcode(String s){

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        ItemModel i = realm.where(ItemModel.class).equalTo("barcode", s).findFirst();
        realm.commitTransaction();

        if(i != null){
            return true;
        }

        return false;

    }*/


}
