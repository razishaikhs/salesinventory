package com.approsity.salesinventory.add.models;

import com.opencsv.bean.CsvBindByName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.RealmObject;

import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Raziuddin.Shaikh on 12/14/2017.
 */

public class ItemModel extends RealmObject {

    public ItemModel() {
    }

    public ItemModel(int id, RealmList<Category> catList, RealmList<SubCategory> subcatList, RealmList<Product> pList, String name, String alias, String desc, String type, String size, String quantity, String costperunit, String totalprice, String saleprice, String barcode, Date datetime) {
        this.id = id;
        this.catList = catList;
        this.subcatList = subcatList;
        this.pList = pList;
        this.name = name;
        this.alias = alias;
        this.desc = desc;
        this.type = type;
        this.size = size;
        this.quantity = quantity;
        this.costperunit = costperunit;
        this.totalprice = totalprice;
        this.saleprice = saleprice;
        this.barcode = barcode;
        this.datetime = datetime;
    }

    @PrimaryKey
    public int id;

    public RealmList<Category> catList;
    public RealmList<SubCategory> subcatList;
    public RealmList<Product> pList;

    public String name;
    public String alias;
    public String desc;
    public String type;
    public String size;
    public String quantity;
    public String costperunit;
    public String totalprice;
    public String saleprice;
    public String barcode;
    public Date datetime;

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RealmList<Category> getCatList() {
        return catList;
    }

    public void setCatList(RealmList<Category> catList) {
        this.catList = catList;
    }

    public RealmList<SubCategory> getSubcatList() {
        return subcatList;
    }

    public void setSubcatList(RealmList<SubCategory> subcatList) {
        this.subcatList = subcatList;
    }

    public RealmList<Product> getpList() {
        return pList;
    }

    public void setpList(RealmList<Product> pList) {
        this.pList = pList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCostperunit() {
        return costperunit;
    }

    public void setCostperunit(String costperunit) {
        this.costperunit = costperunit;
    }

    public String getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(String totalprice) {
        this.totalprice = totalprice;
    }

    public String getSaleprice() {
        return saleprice;
    }

    public void setSaleprice(String saleprice) {
        this.saleprice = saleprice;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

}
