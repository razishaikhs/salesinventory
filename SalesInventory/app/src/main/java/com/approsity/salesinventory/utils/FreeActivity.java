package com.approsity.salesinventory.utils;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.category.CategoryList;
import com.approsity.salesinventory.add.item.PrintBarCode;
import com.approsity.salesinventory.sale.ChargeSale;
import com.approsity.salesinventory.sale.SaleHistory;
import com.approsity.salesinventory.sale.SaleHistoryDetail;
import com.approsity.salesinventory.user.ProfileFrag;


public class FreeActivity extends AppCompatActivity {

    CommonObjects.OpenFragments which;
    Fragment frag;
    Bundle extras;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_free);

        frag = null;

        Intent intent = getIntent();
        extras = intent.getExtras();
        /*if (extras != null) {
            Log.e("EXTRAS", extras.toString());
            Log.e("Extras", extras.getString("isedit") + " ");
            Log.e("Extras", extras.get("isedit") + " ");
            Log.e("Extras", extras.get("userid") + " ");
        }*/

        ImageView backBtn = (ImageView) findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });



        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, getFrag()).commit();


    }

    public Fragment getFrag() {

        which = (CommonObjects.OpenFragments) getIntent().getSerializableExtra("frag");

        if (which == CommonObjects.OpenFragments.chargesale) {
            frag = new ChargeSale();

            /*if (extras != null) {
                frag.setArguments(extras);
            }*/
        }
        else if (which == CommonObjects.OpenFragments.saleHistoryDetail) {
            frag = new SaleHistoryDetail();

            if (extras != null) {
                frag.setArguments(extras);
            }
        }
        else if (which == CommonObjects.OpenFragments.saleHistory) {
            frag = new SaleHistory();

            if (extras != null) {
                frag.setArguments(extras);
            }
        }
        else if (which == CommonObjects.OpenFragments.barcode) {
            frag = new PrintBarCode();

            if (extras != null) {
                frag.setArguments(extras);
            }
        }
        else if (which == CommonObjects.OpenFragments.profile) {
            frag = new ProfileFrag();

            if (extras != null) {
                frag.setArguments(extras);
            }
        }



        return frag;
    }
}
