package com.approsity.salesinventory.user.vendor;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.user.models.Vendor;
import com.approsity.salesinventory.utils.CommonActions;


public class VendorDialog extends DialogFragment {

    EditText et_name, et_contactno, et_email, et_address;
    Button btn_add;
    private onButtonClicked buttonClicked;
    Activity act;

    Animation shake;

    Vendor Obj;

    public static VendorDialog newInstance(Activity activity, Vendor Obj) {

        VendorDialog frag = new VendorDialog();
        frag.act = activity;
        frag.Obj = Obj;
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // setStyle(STYLE_NO_FRAME, R.style.DialogTheme);
        setCancelable(true);

    }



    public View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.dialog_vendor_add, container, false);

        shake = AnimationUtils.loadAnimation(act, R.anim.shake);

        et_name = (EditText) v.findViewById(R.id.et_name);
        et_contactno = (EditText) v.findViewById(R.id.et_contactno);
        et_email = (EditText) v.findViewById(R.id.et_email);
        et_address = (EditText) v.findViewById(R.id.et_address);

        btn_add = (Button) v.findViewById(R.id.btn_add);

        if(getTag().equalsIgnoreCase("add")){
            et_name.setText("");
            et_contactno.setText("");
            et_email.setText("");
            et_address.setText("");

            btn_add.setText("Add");
        }
        else {

            et_name.setText(Obj.name);
            et_contactno.setText(Obj.contactno);
            et_email.setText(Obj.email);
            et_address.setText(Obj.address);

            btn_add.setText("Update");
        }

        setListener();

        return v;
    }

    private void setListener() {

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(TextUtils.isEmpty(et_name.getText().toString())) {
                    et_name.requestFocus();
                    et_name.setError("Please fill the name");

                    et_name.startAnimation(shake);
                }
                else if(!TextUtils.isEmpty(et_email.getText().toString())) {

                    if(!CommonActions.isEmailValid(et_email.getText().toString()))
                    {
                        et_email.requestFocus();
                        et_email.setError("Please enter correct email address");

                        et_email.startAnimation(shake);
                    }
                    else
                       buttonClicked.onClicked(et_name.getText().toString().toUpperCase(), et_contactno.getText().toString(),et_email.getText().toString(),et_address.getText().toString());
                }
                else
                    buttonClicked.onClicked(et_name.getText().toString().toUpperCase(), et_contactno.getText().toString(),et_email.getText().toString(),et_address.getText().toString());

            }
        });
    }

    public void setButtonClicked(onButtonClicked buttonClicked) {
        this.buttonClicked = buttonClicked;
    }



    public interface onButtonClicked {
        public void onClicked(String name, String contact, String email, String city);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (getActivity() != null)
            getActivity().setTheme(R.style.AppTheme);
    }

}

