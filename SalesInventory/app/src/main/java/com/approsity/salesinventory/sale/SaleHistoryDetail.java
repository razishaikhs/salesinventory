package com.approsity.salesinventory.sale;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.utils.CSVData;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;


public class SaleHistoryDetail extends Fragment {

    View mView;

    RecyclerView rc;
    SalesAdap adap;
    LinearLayoutManager mLayoutManager;

    TextView title,shopname,address,date,customer,telephone,fax,email;
    TextView total,discount;

    Button btn_share;

    int saleId=0;

    List<SaleModel> saleList;

    ScrollView receipt_ll;

    public  static final int RequestPermissionCode  = 1;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    int PERMISSION  = 1;

    RelativeLayout rl_toolbar;

    TextView tv_barcode;
    ImageView iv_barcode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_salehistory_detail, container, false);

        saleId = getArguments().getInt("saleid");

        rl_toolbar = (RelativeLayout) getActivity().findViewById(R.id.rl_toolbar);
        rl_toolbar.setVisibility(View.GONE);

        rc = (RecyclerView) mView.findViewById(R.id.rc);

       // title = (TextView) mView.findViewById(R.id.title);
        shopname = (TextView) mView.findViewById(R.id.shopname);
        address = (TextView) mView.findViewById(R.id.address);
        date = (TextView) mView.findViewById(R.id.date);
        customer = (TextView) mView.findViewById(R.id.customer);
        telephone = (TextView) mView.findViewById(R.id.telephone);
        fax = (TextView) mView.findViewById(R.id.fax);
        email = (TextView) mView.findViewById(R.id.email);

        total= (TextView) mView.findViewById(R.id.total);
        discount = (TextView) mView.findViewById(R.id.discount);

        tv_barcode = (TextView) mView.findViewById(R.id.tv_barcode);
        iv_barcode = (ImageView) mView.findViewById(R.id.iv_barcode);

        receipt_ll = (ScrollView) mView.findViewById(R.id.receipt_ll);

        //title.setText("Sale Receipt");
        shopname.setText(CommonObjects.uObj.shopName);
        address.setText(CommonObjects.uObj.address);

        String telno ="<i>Tel # </i>"+" "+ CommonObjects.uObj.telephone;
        String faxno ="<i>Fax #</i>"+" "+ CommonObjects.uObj.fax;
        String emailno  ="<i>Email </i>"+" "+ CommonObjects.uObj.email;

        telephone.setText(Html.fromHtml(telno), TextView.BufferType.SPANNABLE);
        fax.setText(Html.fromHtml(faxno), TextView.BufferType.SPANNABLE);
        email.setText(Html.fromHtml(emailno), TextView.BufferType.SPANNABLE);

        getData();

        btn_share = (Button) mView.findViewById(R.id.btn_share);
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                    requestPermissions(PERMISSIONS, PERMISSION);
                }
                else {

                    View view = receipt_ll;
                    Bitmap returnedBitmap = Bitmap.createBitmap(receipt_ll.getChildAt(0).getWidth(), receipt_ll.getChildAt(0).getHeight(),Bitmap.Config.ARGB_8888);
                    //Bind a canvas to it
                    Canvas canvas = new Canvas(returnedBitmap);
                    //Get the view's background
                    Drawable bgDrawable = view.getBackground();
                    if (bgDrawable!=null) {
                        //has background drawable, then draw it on the canvas
                        bgDrawable.draw(canvas);
                    }   else{
                        //does not have background drawable, then draw white background on the canvas
                        canvas.drawColor(Color.WHITE);
                    }
                    // draw the view on the canvas
                    view.draw(canvas);

                    share(saveBitmap(returnedBitmap));
                }
            }
        });

        if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
            requestPermissions(PERMISSIONS, PERMISSION);
        }

        return mView;
    }

    public void getData() {

        Realm.getDefaultInstance().where(SaleModel.class)
                .equalTo("id",saleId)
                .findAllAsync()
                .addChangeListener(new RealmChangeListener<RealmResults<SaleModel>>() {
                    @Override
                    public void onChange(RealmResults<SaleModel> notiList) {

                        saleList = notiList;

                        if(notiList.size() > 0) {

                            setRc(notiList);
                        }
                    }});

    }

    public void setRc(List<SaleModel> list){

       // List<SaleItemModel> sl = new ArrayList<>();
        adap = new SalesAdap(getActivity(), list.get(0).getSaleItemsList(), false);
       // adap = new SalesAdap(getActivity(), sl, false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rc.setLayoutManager(mLayoutManager);
        rc.setAdapter(adap);

        /*adap.addData(new SaleItemModel(),0);
        adap.addData(new SaleItemModel(),1);
        adap.addData(new SaleItemModel(),2);
        adap.addData(new SaleItemModel(),3);
        adap.addData(new SaleItemModel(),4);
        adap.addData(new SaleItemModel(),5);
        adap.addData(new SaleItemModel(),6);
        adap.addData(new SaleItemModel(),7);
        adap.addData(new SaleItemModel(),8);
        adap.addData(new SaleItemModel(),9);
        adap.addData(new SaleItemModel(),10);
        adap.addData(new SaleItemModel(),11);
        adap.addData(new SaleItemModel(),12);
        adap.addData(new SaleItemModel(),13);
        adap.addData(new SaleItemModel(),14);
        adap.addData(new SaleItemModel(),15);*/


        String dateno ="<i>Date :</i>"+" "+ CommonActions.dateToStringFormatWithSeconds(list.get(0).dateTime);
        String totalno ="<i>Total :</i>"+" "+ list.get(0).total +" RS";

        String discountno;
        if(!TextUtils.isEmpty(list.get(0).discount.toString()))
                discountno ="<i>Discount :</i>"+" "+ list.get(0).discount + " %";
        else
            discountno ="<i>Discount :</i> 0.0 %";

        String custno="";
        try {
            custno = "<i>Customer :</i>" + " " + list.get(0).custList.get(0).name;
        }catch (Exception e){
            e.printStackTrace();
            custno = "<i>Customer :</i>" + " ";
        }

        date.setText(Html.fromHtml(dateno), TextView.BufferType.SPANNABLE);
        customer.setText(Html.fromHtml(custno), TextView.BufferType.SPANNABLE);
        total.setText(Html.fromHtml(totalno), TextView.BufferType.SPANNABLE);
        discount.setText(Html.fromHtml(discountno), TextView.BufferType.SPANNABLE);

        try {
            tv_barcode.setText(list.get(0).barcode);

            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            try {
                BitMatrix bitMatrix = multiFormatWriter.encode(list.get(0).barcode, BarcodeFormat.CODE_128, 300, 75);
                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                iv_barcode.setImageBitmap(bitmap);

//            bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE,100,100);
//            barcodeEncoder = new BarcodeEncoder();
//            bitmap = barcodeEncoder.createBitmap(bitMatrix);
//            BitmapDrawable drawableEnd = new BitmapDrawable(getResources(), bitmap);
//            et_barcode.setCompoundDrawablesWithIntrinsicBounds(null,null, drawableEnd,null);

            } catch (WriterException e) {
                e.printStackTrace();
            }
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public File saveBitmap(Bitmap BITMAP){

        if (BITMAP != null) {
            try {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                BITMAP.compress(Bitmap.CompressFormat.PNG, 40, bytes);

                File createFolder = new File(CommonObjects.appPath+"/receipts");
                if(!createFolder.exists())
                    createFolder.mkdir();

                File newFile = new File(CommonObjects.appPath+"/receipts/"+saleId+".png");

                if(newFile.exists())
                    newFile.delete();

                newFile.createNewFile();
                // write the bytes in file
                FileOutputStream fo = new FileOutputStream(newFile);
                fo.write(bytes.toByteArray());
                // remember close de FileOutput
                fo.close();

                new SingleMediaScanner(getActivity(), CommonObjects.appPath);

                return newFile;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public void share(File f){

        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        // set the type
        shareIntent.setType("image/png");
        // add a subject
        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Email with attachment");
        // build the body of the message to be shared
        String shareMessage = "\n";
        // add the message
        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareMessage);
        shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.fromFile(f));
        // start the chooser for sharing
        startActivity(Intent.createChooser(shareIntent, "Please select sharing medium"));
    }


    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    //CommonActions.snackMsgs(mView,"Permission Granted");

                } else {

                    CommonActions.snackMsgs(mView,"Permission Canceled");
                }
                break;
        }
    }

    private class SingleMediaScanner implements MediaScannerConnection.MediaScannerConnectionClient {
        private MediaScannerConnection mMs;
        private String path;

        SingleMediaScanner(Context context, String f) {
            path = f;
            mMs = new MediaScannerConnection(context, this);
            mMs.connect();
        }
        @Override
        public void onMediaScannerConnected() {
            mMs.scanFile(path, null);
        }
        @Override
        public void onScanCompleted(String path, Uri uri) {
            mMs.disconnect();
        }
    }

}
