package com.approsity.salesinventory.history;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.approsity.salesinventory.MenuAdapter;
import com.approsity.salesinventory.MenuItems;
import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.category.CategoryAdap;
import com.approsity.salesinventory.add.category.CategoryDialog;
import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.history.model.ExpendableTransactionsModel;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.viethoa.RecyclerViewFastScroller;
import com.viethoa.models.AlphabetItem;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;


/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class TransactionsHistory extends Fragment{

    View mView;

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;

    List<ExpendableTransactionsModel> parentList;
    List<Transactions> tList,tempList;

    RelativeLayout rl_actions;
    TextView tv_norecord;

    String dateFormat="";
    String title;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_transactionhistory, container, false);

        tv_norecord = (TextView) mView.findViewById(R.id.tv_norecord);
        tv_norecord.setVisibility(View.VISIBLE);

        rl_actions = (RelativeLayout) mView.findViewById(R.id.rl_actions);
        rl_actions.setVisibility(View.GONE);

        expandableListView = (ExpandableListView) mView.findViewById(R.id.expandableListView);

        getData();

        return mView;
    }

    public void getData() {

        Realm.getDefaultInstance().where(Transactions.class)
                .findAllSortedAsync("datetime",  Sort.DESCENDING)
                .addChangeListener(new RealmChangeListener<RealmResults<Transactions>>() {
                    @Override
                    public void onChange(RealmResults<Transactions> notiList) {

                        if(notiList.size() > 0) {

                            setRc(notiList);

                            rl_actions.setVisibility(View.VISIBLE);
                            tv_norecord.setVisibility(View.GONE);
                        }
                        else
                        {
                            rl_actions.setVisibility(View.GONE);
                            tv_norecord.setVisibility(View.VISIBLE);
                        }
                    }});

    }

    public void setRc(List<Transactions> t){

        expandableListAdapter = new HistoryAdapter(getActivity(), applyDateFilter(t));
        expandableListView.setAdapter(expandableListAdapter);
    }

    public List<ExpendableTransactionsModel> applyDateFilter(List<Transactions> t){

        tList = new ArrayList<>();
        tList.addAll(t);

        parentList = new ArrayList<>();
        tempList = new ArrayList<>();

        int c=0;

        for(int i=0;i<tList.size();i++){

            String d = tList.get(i).datetime+"";
            boolean hCheck;

            if(!dateFormat.equals(d.split(" ")[0]+" "+ d.split(" ")[1]+" "+d.split(" ")[2]+" "+d.split(" ")[5])){

                hCheck = true;

                if(c>0){
                    ExpendableTransactionsModel pObj = new ExpendableTransactionsModel(title,tempList);
                    parentList.add(pObj);
                }
                c++;

                title = d.split(" ")[0]+" "+ d.split(" ")[1]+" "+d.split(" ")[2]+" "+d.split(" ")[5];

            }else{
                hCheck = false;
            }

            if(hCheck){
                tempList = new ArrayList<>();
                tempList.add(tList.get(i));

                if(i+1 == tList.size()){
                    ExpendableTransactionsModel pObj = new ExpendableTransactionsModel(title,tempList);
                    parentList.add(pObj);
                }
            }
            else {
                tempList.add(tList.get(i));

                if(i+1 == tList.size()){
                    ExpendableTransactionsModel pObj = new ExpendableTransactionsModel(title,tempList);
                    parentList.add(pObj);
                }
            }

            dateFormat = d.split(" ")[0]+" "+ d.split(" ")[1]+" "+d.split(" ")[2]+" "+d.split(" ")[5];
        }

        return parentList;
    }

}
