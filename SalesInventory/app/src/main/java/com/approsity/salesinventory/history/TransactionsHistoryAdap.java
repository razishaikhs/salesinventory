package com.approsity.salesinventory.history;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.category.CategoryDialog;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.utils.CommonActions;
import com.viethoa.RecyclerViewFastScroller;

import java.util.ArrayList;
import java.util.List;

public class TransactionsHistoryAdap extends RecyclerView.Adapter<TransactionsHistoryAdap.ViewHolder> implements RecyclerViewFastScroller.BubbleTextGetter, Filterable {

    private static final String TAG = TransactionsHistoryAdap.class.getSimpleName();

    private List<Transactions> mItems,items2;

    Activity act;

    private int position;

    private CustomFilter mFilter;

    CategoryDialog categoryDialog;

    RecyclerView rc;

    String dateFormat="";

    public TransactionsHistoryAdap(Activity act, List<Transactions> items, RecyclerView rc) {
        mItems = items;
        this.act = act;
        this.rc = rc;

        mFilter = new CustomFilter();
        this.items2 = new ArrayList<Transactions>();
        this.items2.addAll(mItems);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int view_type) {

        View v = null;

        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.frag_transhistory_child_row, viewGroup, false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int pos) {

        final Transactions aObj = mItems.get(pos);

        if (pos % 2 == 1) {
            // viewHolder.ll.setBackgroundColor(act.getResources().getColor(R.color.white));
        } else {
             //viewHolder.ll.setBackgroundColor(act.getResources().getColor(android.R.color.darker_gray));
        }

        viewHolder.text_name.setText(aObj.name);
        viewHolder.text_time.setText(CommonActions.dateToStringFormatWithSeconds(aObj.datetime).split(" ",2)[1]);
        viewHolder.text_desc.setText(aObj.desc);

        String d = aObj.datetime+"";

        if(!dateFormat.equals(d.split(" ")[0]+" "+ d.split(" ")[1]+" "+d.split(" ")[2]+" "+d.split(" ")[5])){

        }
        else
            viewHolder.ll_date.setVisibility(View.GONE);


        dateFormat = d.split(" ")[0]+" "+ d.split(" ")[1]+" "+d.split(" ")[2]+" "+d.split(" ")[5];
        viewHolder.text_date.setText(dateFormat);

       // viewHolder.ll_date.setVisibility(View.GONE);

    }


    @Override
    public int getItemViewType(int position) {

       return 0;
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String getTextToShowInBubble(int pos) {

        if (pos < 0 || pos >= mItems.size())
            return null;

        String name = mItems.get(pos).desc;
        if (name == null || name.length() < 1)
            return null;

        return mItems.get(pos).desc.substring(0, 1);
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView text_time,text_desc,text_name;
        LinearLayout ll;
        LinearLayout ll_date;
        TextView text_date;

        View rootView;

        ViewHolder(View v) {
            super(v);

            text_time = (TextView) v.findViewById(R.id.text_time);
            text_desc = (TextView) v.findViewById(R.id.text_desc);
            text_name = (TextView) v.findViewById(R.id.text_name);

            ll = (LinearLayout) v.findViewById(R.id.ll);

            text_date = (TextView) v.findViewById(R.id.text_date);
            ll_date = (LinearLayout) v.findViewById(R.id.ll_date);

            v.setOnClickListener(this);

            rootView = v;
        }

        @Override
        public void onClick(View v) {

           /* selectedPosition=getLayoutPosition();
            notifyDataSetChanged();*/

        }
    }


    public Transactions getItem(int position) {
        return mItems.get(position);
    }

    public void addData(Transactions newModelData, int position) {
        mItems.add(position, newModelData);
        notifyItemInserted(position);
    }

    public void removeData(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    public void clearData() {
        Log.v(TAG, "clearData()");
        int size = this.mItems.size();

        int index = 0;
        if (size > index) {
            for (int i = index; i < size; i++) {
                this.mItems.remove(index);
            }

            this.notifyItemRangeRemoved(index, size - 1);
        }

    }

    public class CustomFilter extends Filter {


        private CustomFilter() {
            super();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            List<Transactions> filteredList = new ArrayList<Transactions>();
            FilterResults results = new FilterResults();

            if (constraint.length() == 0) {
                mItems.clear();
                mItems.addAll(items2);
                filteredList.addAll(mItems);
            } else {

                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (Transactions mWords : items2) {
                    if (mWords.name.toLowerCase().startsWith(filterPattern)) {
                        filteredList.add(mWords);
                    }
                }

                mItems.clear();
                mItems = filteredList;
            }

            results.values = filteredList;
            results.count = filteredList.size();


            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            notifyDataSetChanged();
        }
    }


}