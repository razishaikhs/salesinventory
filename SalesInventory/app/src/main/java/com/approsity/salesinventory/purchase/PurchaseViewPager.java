package com.approsity.salesinventory.purchase;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.purchase.models.Purchase;

import java.util.ArrayList;
import java.util.List;



public class PurchaseViewPager extends Fragment {

    SectionsPagerAdapter mSectionsPagerAdapter;
    TabLayout mSlidingTabLayout;
    ViewPager mViewPager;
    List<String> rList;
    View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_product_viewpager, container, false);

        rList = new ArrayList<String>();
        rList.add("Purchases");
        rList.add("Add");

        mSlidingTabLayout = (TabLayout) mView.findViewById(R.id.sliding_tabs);
        mViewPager = (ViewPager) mView.findViewById(R.id.viewpager);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(0);
        mViewPager.setOffscreenPageLimit(rList.size() + 1);
        mSlidingTabLayout.setupWithViewPager(mViewPager);

        return mView;
    }
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {

            /*FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.container,new Parents_Routes()).commit();
            Bundle args = new Bundle();
            args.putInt("pos", position);
            fragment.setArguments(args);*/

            Fragment fragment = null;
            switch(position) {
                case 0:
                    fragment = new PurchaseList();
                    break;
                case 1:
                    fragment = new PurchasesAdd();
                    break;
            }
            return fragment;
        }
        @Override
        public int getCount() {
            return rList.size();
        }
        @Override
        public int getItemPosition(Object object) {
            mSectionsPagerAdapter.notifyDataSetChanged();
            return POSITION_NONE;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return " "+rList.get(position);
        }
    }

}