package com.approsity.salesinventory.utils;

import android.support.design.widget.Snackbar;

import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.add.models.Product;
import com.approsity.salesinventory.add.models.SubCategory;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.user.models.Vendor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by Umair on 29/12/2017.
 */

public class SampleData {

    public static void sampleRecords() {

        Realm db = Realm.getDefaultInstance();
        db.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                List<Category> cList = new ArrayList<>();
                List<SubCategory> scList = new ArrayList<>();
                List<Product> pList = new ArrayList<>();
                List<Vendor> vList = new ArrayList<>();

                String arr[] = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};

                Random r = new Random();

                String desc = "Lorem Ispum dolor sit amet Lorem Ispum dolor sit amet Lorem Ispum dolor sit amet Lorem Ispum dolor sit amet Lorem Ispum dolor sit amet";

                for(int i=0;i<100000;i++) {
                    cList.add(new Category(i, arr[r.nextInt(26)]+" stuff",arr[r.nextInt(26)]+"H",desc));
                }

                for(int i=0;i<100000;i++) {
                    scList.add(new SubCategory(i, arr[r.nextInt(26)]+" stuff",arr[r.nextInt(26)]+"H",desc));
                }

                for(int i=0;i<100000;i++) {
                    pList.add(new Product(i, arr[r.nextInt(26)]+" stuff",arr[r.nextInt(26)]+"H",desc));
                }

                for(int i=0;i<100000;i++) {
                    vList.add(new Vendor(i, arr[r.nextInt(26)]+" Vendor","Block D, North Nazimabad","03452432698","xaharx@gmail.com"));
                }

                Collections.sort(cList, new Comparator<Category>() {
                    @Override
                    public int compare(Category o1, Category o2) {
                        return o1.name.compareTo(o2.name);
                    }
                });

                Collections.sort(scList, new Comparator<SubCategory>() {
                    @Override
                    public int compare(SubCategory o1, SubCategory o2) {
                        return o1.name.compareTo(o2.name);
                    }
                });

                Collections.sort(pList, new Comparator<Product>() {
                    @Override
                    public int compare(Product o1, Product o2) {
                        return o1.name.compareTo(o2.name);
                    }
                });

                Collections.sort(vList, new Comparator<Vendor>() {
                    @Override
                    public int compare(Vendor o1, Vendor o2) {
                        return o1.name.compareTo(o2.name);
                    }
                });

                realm.insertOrUpdate(cList);
                realm.insertOrUpdate(scList);
                realm.insertOrUpdate(pList);
                realm.insertOrUpdate(vList);

            }
        });

    }
}
