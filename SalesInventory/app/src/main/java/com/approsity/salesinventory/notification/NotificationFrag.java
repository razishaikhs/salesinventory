package com.approsity.salesinventory.notification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.approsity.salesinventory.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;


public class NotificationFrag extends Fragment {

    View mView;

    RecyclerView rc;
    NotificationListAdapter adap;
    LinearLayoutManager mLayoutManager;

    List<NotificationData> nList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_notification, container, false);

        rc = (RecyclerView) mView.findViewById(R.id.rc);

        nList = new ArrayList<NotificationData>();
        setData();

        return mView;
    }


    public void setData() {

        Realm.getDefaultInstance().where(NotificationData.class)
                .findAllAsync()
                .addChangeListener(new RealmChangeListener<RealmResults<NotificationData>>() {
                    @Override
                    public void onChange(RealmResults<NotificationData> notiList) {

                        if(notiList.size() > 0) {
                            nList = notiList;
                            setRc();
                        }

                    }});
    }

    public void setRc(){

        adap = new NotificationListAdapter(getActivity(), nList);

        mLayoutManager = new LinearLayoutManager(getActivity());
        rc.setLayoutManager(mLayoutManager);
        rc.setAdapter(adap);
    }


}
