package com.approsity.salesinventory.purchase;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.approsity.salesinventory.NavigationActivity;
import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.category.CategoryDialog;
import com.approsity.salesinventory.add.item.ItemViewPager;
import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.add.models.Product;
import com.approsity.salesinventory.add.models.SubCategory;
import com.approsity.salesinventory.add.product.ProductDialog;
import com.approsity.salesinventory.add.subcategory.SubCategoryDialog;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.purchase.models.Purchase;
import com.approsity.salesinventory.user.models.Vendor;
import com.approsity.salesinventory.user.vendor.VendorDialog;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;


public class Purchase_Edit_Dialog extends DialogFragment implements View.OnClickListener{

    View v;

    List<Vendor> vList;
    List<ItemModel> iList;

    Spinner spinner_ven,spinner_item;

    EditText et_alias, et_payby, et_invoice,et_quantity,et_cost,et_date,et_desc;
    ImageView btn_add_ven,btn_add_item;

    Button btn_add;

    LinearLayout ll_spinner_ven,ll_spinner_item;

    ScrollView sv;

    Activity act;

    Animation shake;

    Purchase Obj;

    int year, month, day;
    int mHour,mMinute,mSeconds;

    String date,time;

    public static Purchase_Edit_Dialog newInstance(Activity activity, Purchase Obj) {

        Purchase_Edit_Dialog frag = new Purchase_Edit_Dialog();
        frag.act = activity;
        frag.Obj = Obj;
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setStyle(STYLE_NO_FRAME, R.style.DialogTheme);
        setCancelable(true);

    }



    public View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.dialog_purchases_add, container, false);

        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        mHour = calendar.get(Calendar.HOUR_OF_DAY);
        mMinute = calendar.get(Calendar.MINUTE);
        mSeconds = calendar.get(Calendar.SECOND);

        int m = month + 1;
        date = day+"/"+m+"/"+year;
        time = CommonActions.getCurrentTimeAmPm();

        shake = AnimationUtils.loadAnimation(act, R.anim.shake);

        btn_add = (Button) v.findViewById(R.id.btn_add);
        btn_add.setOnClickListener(this);
        btn_add.setText("Update");
        btn_add_ven = (ImageView) v.findViewById(R.id.btn_add_ven);
        btn_add_ven.setOnClickListener(this);
        btn_add_item = (ImageView) v.findViewById(R.id.btn_add_item);
        btn_add_item.setOnClickListener(this);

        et_alias = (EditText) v.findViewById(R.id.et_alias);
        et_desc = (EditText) v.findViewById(R.id.et_desc);
        et_payby  = (EditText) v.findViewById(R.id.et_payby);
        et_invoice = (EditText) v.findViewById(R.id.et_invoice);
        et_quantity = (EditText) v.findViewById(R.id.et_quantity);
        et_cost = (EditText) v.findViewById(R.id.et_cost);
        et_date = (EditText) v.findViewById(R.id.et_date);

        spinner_ven = (Spinner) v.findViewById(R.id.spinner_ven);
        spinner_item = (Spinner) v.findViewById(R.id.spinner_item);

        ll_spinner_ven = v.findViewById(R.id.ll_spinner_ven);
        ll_spinner_item = v.findViewById(R.id.ll_spinner_item);

        sv = (ScrollView) v.findViewById(R.id.sv);

        et_alias.setText(Obj.alias);
        et_desc.setText(Obj.desc);
        et_payby.setText(Obj.method);
        et_invoice.setText(Obj.invoice);
        et_quantity.setText(Obj.quantity);
        et_cost.setText(Obj.cost);
        et_date.setText(CommonActions.dateToStringFormat(Obj.datetime));

        setSpinnerData();

        setOperations();

        return v;
    }

    public void setSpinnerData() {
        getRealmVen();
        getRealmItem();
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()) {
            case R.id.btn_add_ven:
                vendorDialog();
                break;
            case R.id.btn_add_item:

                CommonObjects.itemPos = 1;

                ((NavigationActivity)this.getActivity()).setTitle("ITEMS");
                ((NavigationActivity)this.getActivity()).goToFragment(new ItemViewPager(),false);

                dismiss();
                break;
            case R.id.btn_add:
                AddButton();
                break;
            default:
        }
    }

    public void vendorDialog() {

        final VendorDialog vendorDialog = VendorDialog.newInstance(getActivity(), null);
        vendorDialog.setButtonClicked(new VendorDialog.onButtonClicked() {
            @Override
            public void onClicked(final String name, final String contact, final String email, final String address) {
                vendorDialog.dismiss();

                Realm db = Realm.getDefaultInstance();
                db.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        Number currentIdNum = realm.where(Vendor.class).max("id");
                        int nextId = 0;
                        if(currentIdNum == null) {
                            nextId = 1;
                        } else {
                            nextId = currentIdNum.intValue() + 1;
                        }

                        Vendor fvData = new Vendor(nextId, name, address ,contact,email);
                        realm.insert(fvData);

                        Snackbar.make(v,"Vendor Added Successfully!",Snackbar.LENGTH_SHORT).show();

                        RealmList<Vendor> cl = new RealmList<>();
                        cl.add(fvData);
                        int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                        Transactions tObj = new Transactions(i,null,null,null,cl,null,null, CommonActions.currentDateTime(),"Vendor created",fvData.name);
                        realm.insertOrUpdate(tObj);

                        getRealmVen();

                    }
                });
            }
        });

        vendorDialog.show(getFragmentManager(),"add");
    }

    public void getRealmVen() {
        vList = new ArrayList<>();
        RealmResults<Vendor> v = Realm.getDefaultInstance().where(Vendor.class).findAllSortedAsync("name",  Sort.ASCENDING);
        vList = Realm.getDefaultInstance().copyFromRealm(v);

        List<String> vlist = new ArrayList<>();
        for(Vendor cat : vList) {
            vlist.add(cat.name);
        }
        ArrayAdapter<String> spinner_venAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,vlist);
        spinner_ven.setAdapter(spinner_venAdap);
    }

    public void getRealmItem() {
        iList = new ArrayList<>();
        RealmResults<ItemModel> i =  Realm.getDefaultInstance().where(ItemModel.class).findAllSortedAsync("name",  Sort.ASCENDING);
        iList =  Realm.getDefaultInstance().copyFromRealm(i);

        List<String> ilist = new ArrayList<>();
        for(ItemModel item : iList) {

            if(!item.alias.equals("")) {
                String spinneralias = item.name + " - "  + item.alias;
                ilist.add(spinneralias);
            }else
                ilist.add(item.name);
        }
        ArrayAdapter<String> spinner_scatAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,ilist);
        spinner_item.setAdapter(spinner_scatAdap);
    }

    public void setOperations() {

        et_quantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b) {
                    if(TextUtils.isEmpty(et_quantity.getText().toString())){
                        et_quantity.setText(Obj.quantity);
                    }

                    BigDecimal bd = new BigDecimal(et_quantity.getText().toString());

                    if(bd.doubleValue() <= 0){
                        et_quantity.setText(Obj.quantity);
                    }
                }
            }
        });

        et_cost.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b) {
                    if(TextUtils.isEmpty(et_cost.getText().toString())){
                        et_cost.setText(Obj.cost);
                    }

                    BigDecimal bd = new BigDecimal(et_cost.getText().toString());

                    if(bd.doubleValue() <= 0){
                        et_cost.setText(Obj.cost);
                    }
                }
            }
        });

        et_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate();
            }
        });
    }

    public void AddButton() {

        if(vList.size() == 0)
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, ll_spinner_ven.getTop());
                }
            });

            Snackbar.make(v,"Please add Vendor",Snackbar.LENGTH_SHORT).show();
            ll_spinner_ven.startAnimation(shake);
        }
        else if(iList.size() == 0)
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, ll_spinner_item.getTop());
                }
            });
            Snackbar.make(v,"Please add ItemModel",Snackbar.LENGTH_SHORT).show();
            ll_spinner_item.startAnimation(shake);
        }
        else if(TextUtils.isEmpty(et_invoice.getText().toString()))
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, et_invoice.getTop());
                }
            });
            et_invoice.requestFocus();
            et_invoice.setError("Please add the invoice number");
            et_invoice.startAnimation(shake);
        }
        else
        {
            Realm db = Realm.getDefaultInstance();
            db.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    Vendor vObj = vList.get(spinner_ven.getSelectedItemPosition());
                    ItemModel iObj = iList.get(spinner_item.getSelectedItemPosition());

                    String alias = et_alias.getText().toString().toUpperCase();
                    String invoice = et_invoice.getText().toString();
                    String quantity = et_quantity.getText().toString();
                    String cost = et_cost.getText().toString();
                    String desc = et_desc.getText().toString();
                    String payby = et_payby.getText().toString();
                    String date = et_date.getText().toString();

                    RealmList<Vendor> vl = new RealmList<>();
                    vl.add(vObj);

                    RealmList<ItemModel> il = new RealmList<>();

                    ItemModel item = realm.where(ItemModel.class).equalTo("id",iObj.id).findFirst();
                    item.quantity = (addBigDecimal(et_quantity.getText().toString(),item.quantity).subtract(new BigDecimal(Obj.quantity))).abs()+"";
                    item.totalprice = (addBigDecimal(et_cost.getText().toString(),item.totalprice).subtract(new BigDecimal(Obj.cost))).abs()+"";
                    item.costperunit = divideBigDecimal(item.totalprice,item.quantity)+"";

                    il.add(item);

                    Purchase toEdit = realm.where(Purchase.class).equalTo("id", Obj.id).findFirst();
                    if(toEdit != null) {
                        toEdit.vendorList.addAll(vl);
                        toEdit.itemList.addAll(il);
                        toEdit.invoice = invoice;
                        toEdit.quantity = quantity;
                        toEdit.cost = cost;
                        toEdit.desc = desc;
                        toEdit.datetime = CommonActions.stringToDateFormat(date);
                        toEdit.alias = alias;
                        toEdit.method = payby;
                        toEdit.vendorname = vObj.name;
                        toEdit.itemname = iObj.name;
                        toEdit.itemsize = iObj.size;
                        toEdit.itemtype = iObj.type;
                    }

                    realm.insertOrUpdate(toEdit);

                    Snackbar.make(v,"Purchases record updated successfully!",Snackbar.LENGTH_SHORT).show();

                    RealmList<Purchase> pl = new RealmList<>();
                    pl.add(toEdit);
                    int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                    Transactions tObj = new Transactions(i,null,null,null,toEdit.vendorList,toEdit.itemList,pl, CommonActions.currentDateTime(),"Purchases updated by editing "+et_quantity.getText().toString()+" qty @"+et_cost.getText().toString() +" rs",toEdit.invoice);
                    realm.insertOrUpdate(tObj);

                    et_invoice.setText("");
                    et_alias.setText("");
                    et_desc.setText("");

                    et_quantity.setText("1");
                    et_cost.setText("1.0");

                    dismiss();

                }
            });
        }
    }

    public void setDate() {
        //showDate(year, month+1, day);
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
                        // TODO Auto-generated method stub
                        // arg1 = year
                        // arg2 = month
                        // arg3 = day
                        showDate(arg1, arg2+1, arg3);
                    }
                }, year, month, day);

        datePicker.show();
    }

    private void showDate(int year, int month, int day) {
        StringBuilder sb = new StringBuilder().append(day).append("/").append(month).append("/").append(year);
        date = sb.toString();

        et_date.setText(date+" "+time);

        getTime();
    }

    public void getTime() {
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        mHour = hourOfDay;
                        mMinute = minute;

                        Calendar calendar = Calendar.getInstance();
                        mSeconds = calendar.get(Calendar.SECOND);

                        time = CommonActions.timeDialogToAMPM(mHour,minute);

                        et_date.setText(date+" "+time + "");

                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public BigDecimal addBigDecimal(String s1,String s2) {
        BigDecimal bd1 = new BigDecimal(s1);
        BigDecimal bd2 = new BigDecimal(s2);

        return bd1.add(bd2);
    }

    public BigDecimal divideBigDecimal(String cost,String quantity) {

        try {
            BigDecimal cbd = new BigDecimal(cost);
            BigDecimal qbd = new BigDecimal(quantity);

            return cbd.divide(qbd, 2, RoundingMode.HALF_UP);
        }catch ( Exception e){
            e.printStackTrace();
            return BigDecimal.ZERO;
        }
    }

}

