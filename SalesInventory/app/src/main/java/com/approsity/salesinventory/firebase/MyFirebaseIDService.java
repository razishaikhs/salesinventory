package com.approsity.salesinventory.firebase;

import android.util.Log;

import com.approsity.salesinventory.utils.CommonObjects;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class MyFirebaseIDService extends FirebaseInstanceIdService {

   // protected BasePreferenceHelper prefHelper = new BasePreferenceHelper(this);

    private String token;

    @Override
    public void onTokenRefresh() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        Log.e("FCM Refreshed token: ", refreshedToken +" ");
        sendRegistrationToServer(refreshedToken);

    }

    public void sendRegistrationToServer(String token) {

      //  prefHelper.putFCMID(token);
       // String fCM_ = prefHelper.getFCMID();

        CommonObjects.pushNotificationToken = token;
    }

}
