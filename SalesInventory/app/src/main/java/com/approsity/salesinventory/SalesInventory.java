package com.approsity.salesinventory;

import android.content.Context;
import com.approsity.salesinventory.user.User;
import com.approsity.salesinventory.utils.CommonObjects;
import com.approsity.salesinventory.utils.RealmMigrations;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;


public class SalesInventory extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
       // StrictMode.setVmPolicy(builder.build());

        Realm.init(this);

        RealmConfiguration configuration = new RealmConfiguration.Builder().schemaVersion(1).migration(new RealmMigrations()).build();
        Realm.setDefaultConfiguration(configuration);
        Realm.getInstance(configuration);

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onTerminate() {

        Realm.getDefaultInstance().close();

        super.onTerminate();
    }
}

