package com.approsity.salesinventory.sale;

import com.approsity.salesinventory.user.models.Customer;

import java.io.Serializable;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Raziuddin.Shaikh on 1/1/2018.
 */

public class SaleModel extends RealmObject implements Serializable {

    public SaleModel() {
    }

    public SaleModel(int id, RealmList<SaleItemModel> saleItemsList, RealmList<Customer> custList,Date dateTime, String discount, String total, String paymentmethod, String paybackamount, String receivedamount, String totalprofit,String barcode) {
        this.id = id;
        this.saleItemsList = saleItemsList;
        this.custList = custList;
        this.dateTime = dateTime;
        this.discount = discount;
        this.total = total;
        this.paymentmethod = paymentmethod;
        this.paybackamount = paybackamount;
        this.receivedamount = receivedamount;
        this.totalprofit = totalprofit;
        this.barcode = barcode;
    }

    @PrimaryKey
    public int id;

    public RealmList<SaleItemModel> saleItemsList;
    public RealmList<Customer> custList;
    public Date dateTime;
    public String discount;
    public String total;
    public String paymentmethod;
    public String paybackamount;
    public String receivedamount;
    public String totalprofit;
    public String barcode;

    public RealmList<Customer> getCustList() {
        return custList;
    }

    public void setCustList(RealmList<Customer> custList) {
        this.custList = custList;
    }

    public String getPaymentmethod() {
        return paymentmethod;
    }

    public void setPaymentmethod(String paymentmethod) {
        this.paymentmethod = paymentmethod;
    }

    public String getPaybackamount() {
        return paybackamount;
    }

    public void setPaybackamount(String paybackamount) {
        this.paybackamount = paybackamount;
    }

    public String getReceivedamount() {
        return receivedamount;
    }

    public void setReceivedamount(String receivedamount) {
        this.receivedamount = receivedamount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RealmList<SaleItemModel> getSaleItemsList() {
        return saleItemsList;
    }

    public void setSaleItemsList(RealmList<SaleItemModel> saleItemsList) {
        this.saleItemsList = saleItemsList;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotalprofit() {
        return totalprofit;
    }

    public void setTotalprofit(String totalprofit) {
        this.totalprofit = totalprofit;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
