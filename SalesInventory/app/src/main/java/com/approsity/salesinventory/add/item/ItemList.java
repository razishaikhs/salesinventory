package com.approsity.salesinventory.add.item;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.utils.CommonObjects;
import com.google.gson.Gson;
import com.viethoa.RecyclerViewFastScroller;
import com.viethoa.models.AlphabetItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;


/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class ItemList extends Fragment implements CommonObjects.RecyclerViewClickListener{

    View mView;

    RecyclerView rc;
    ItemAdap adap;
    LinearLayoutManager mLayoutManager;

    List<ItemModel> cList;
    List<ItemModel> filterList;

    RecyclerViewFastScroller fast_scroller;

    EditText et_search;

    RelativeLayout rl_actions;
    TextView tv_norecord;

    ItemFilterDialog filterDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_categorylist, container, false);

        rc = (RecyclerView) mView.findViewById(R.id.rc);
        fast_scroller = (RecyclerViewFastScroller) mView.findViewById(R.id.fast_scroller);

        tv_norecord = (TextView) mView.findViewById(R.id.tv_norecord);
        tv_norecord.setVisibility(View.VISIBLE);

        rl_actions = (RelativeLayout) mView.findViewById(R.id.rl_actions);
        rl_actions.setVisibility(View.GONE);

        filterDialog = ItemFilterDialog.newInstance(getActivity());

        FloatingActionButton fab = (FloatingActionButton) mView.findViewById(R.id.fab);
        fab.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_filter));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                filerDialog();
            }
        });

        et_search = (EditText) mView.findViewById(R.id.et_search);
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    adap.getFilter().filter(s.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        getData();

        return mView;
    }


    public void getData() {

        cList = new ArrayList<>();

        RealmResults l;
        l = Realm.getDefaultInstance().where(ItemModel.class).findAllSortedAsync("name",Sort.ASCENDING);
        l.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<ItemModel>>() {
            @Override
            public void onChange(RealmResults<ItemModel> itemModels, OrderedCollectionChangeSet changeSet) {
                cList = itemModels;

                if(itemModels.size() > 0) {

                    setRc(cList);

                    rl_actions.setVisibility(View.VISIBLE);
                    tv_norecord.setVisibility(View.GONE);
                }
                else
                {
                    rl_actions.setVisibility(View.GONE);
                    tv_norecord.setVisibility(View.VISIBLE);
                }
            }
        });
        l.load();
    }

    public void setRc(List<ItemModel> list){
        adap = new ItemAdap(getActivity(), AddAlphabeticalFilter(Realm.getDefaultInstance().copyFromRealm(list)), this,rc);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rc.setLayoutManager(mLayoutManager);
        rc.setAdapter(adap);

        fast_scroller.setRecyclerView(rc);
    }

    public List<ItemModel> AddAlphabeticalFilter(List<ItemModel> catList) {

        ArrayList<AlphabetItem> mAlphabetItems = new ArrayList<>();
        List<String> strAlphabets = new ArrayList<>();
        for (int i = 0; i < catList.size(); i++) {
            String name = catList.get(i).name.toUpperCase();
            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1);
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }

        fast_scroller.setUpAlphabet(mAlphabetItems);

        return catList;
    }

    @Override
    public void recyclerViewListClicked(View v, int position, Context con) {

    }

    public void filerDialog() {

        filterDialog.setButtonClicked(new ItemFilterDialog.onButtonClicked() {
            @Override
            public void onClicked(final int catId, final int scatId, final int proId, final String type, final String size) {
                filterDialog.dismiss();

                Realm db = Realm.getDefaultInstance();
                db.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        filterList = new ArrayList<>();

                        RealmResults<ItemModel> items = realm.where(ItemModel.class).findAll();

                        RealmQuery c = items.where().beginGroup();

                        if(catId != -1) {
                            c = c.equalTo("catList.id",catId);
                        }

                        if(scatId != -1) {
                            c = c.equalTo("subcatList.id",scatId);
                        }

                        if(proId != -1) {
                            c = c.equalTo("pList.id",proId);
                        }

                        if(!TextUtils.isEmpty(type.trim().toString())) {
                            c = c.equalTo("type",type);
                        }

                        if(!TextUtils.isEmpty(size.trim().toString())) {
                            c = c.equalTo("size",size);
                        }


                        if(catId == -1 && scatId == -1 && proId == -1 && TextUtils.isEmpty(type.trim().toString()) && TextUtils.isEmpty(size.trim().toString())) {
                            setRc(cList);
                        }
                        else {
                            items = c.endGroup().findAll();
                            filterList = realm.copyToRealmOrUpdate(items);

                            if(items.size() > 0){
                                setRc(filterList);
                            }
                            else
                                Snackbar.make(mView,"No Record Found!",Snackbar.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        });

        filterDialog.show(getFragmentManager(),"");
    }

}
