package com.approsity.salesinventory.user;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.sale.SalesList;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by Raziuddin.Shaikh on 11/24/2017.
 */

public class RegistrationActivity extends AppCompatActivity {

    ScrollView sv;

    EditText et_name,et_email,et_shop,et_address,et_phone,et_fax,et_cpass,et_pass;

    Button btn_signup;

    Animation shake;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        sv = (ScrollView) findViewById(R.id.sv);

        shake = AnimationUtils.loadAnimation(RegistrationActivity.this, R.anim.shake);

        et_name = (EditText) findViewById(R.id.et_name);
        et_email = (EditText) findViewById(R.id.et_email);
        et_shop = (EditText) findViewById(R.id.et_shop);
        et_address = (EditText) findViewById(R.id.et_address);
        et_phone = (EditText) findViewById(R.id.et_phone);
        et_fax = (EditText) findViewById(R.id.et_fax);
        et_pass = (EditText) findViewById(R.id.et_pass);
        et_cpass = (EditText) findViewById(R.id.et_cpass);

        btn_signup = (Button) findViewById(R.id.btn_signup);
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hideKeyboard();

                if(TextUtils.isEmpty(et_name.getText().toString()))
                {
                    sv.post(new Runnable() {
                        @Override
                        public void run() {
                            sv.scrollTo(0, et_name.getTop());
                        }
                    });
                    et_name.requestFocus();
                    et_name.setError("Please add the name");
                    et_name.startAnimation(shake);
                }
                else if(TextUtils.isEmpty(et_email.getText().toString()))
                {
                    sv.post(new Runnable() {
                        @Override
                        public void run() {
                            sv.scrollTo(0, et_email.getTop());
                        }
                    });
                    et_email.requestFocus();
                    et_email.setError("Please add email");
                    et_email.startAnimation(shake);
                }
                else if(!CommonActions.isEmailValid(et_email.getText().toString()))
                {
                    sv.post(new Runnable() {
                        @Override
                        public void run() {
                            sv.scrollTo(0, et_email.getTop());
                        }
                    });
                    et_email.requestFocus();
                    et_email.setError("Please enter correct email");
                    et_email.startAnimation(shake);
                }
                else if(TextUtils.isEmpty(et_pass.getText().toString()))
                {
                    sv.post(new Runnable() {
                        @Override
                        public void run() {
                            sv.scrollTo(0, et_pass.getTop());
                        }
                    });
                    et_pass.requestFocus();
                    et_pass.setError("Please add password");
                    et_pass.startAnimation(shake);
                }
                else if(TextUtils.isEmpty(et_cpass.getText().toString()))
                {
                    sv.post(new Runnable() {
                        @Override
                        public void run() {
                            sv.scrollTo(0, et_cpass.getTop());
                        }
                    });
                    et_cpass.requestFocus();
                    et_cpass.setError("Please add password");
                    et_cpass.startAnimation(shake);
                }
                else if(!et_pass.getText().toString().equals(et_cpass.getText().toString()))
                {
                    sv.post(new Runnable() {
                        @Override
                        public void run() {
                            sv.scrollTo(0, et_cpass.getTop());
                        }
                    });
                    et_cpass.requestFocus();
                    et_cpass.setError("Password not match");
                    et_cpass.startAnimation(shake);
                }
                else{

                    final ProgressDialog pDialog = CommonActions.createProgressDialog(RegistrationActivity.this);

                    if (CommonActions.isConnected(RegistrationActivity.this)){

                        FirebaseAuth.getInstance().createUserWithEmailAndPassword(et_email.getText().toString(), et_pass.getText().toString())
                                .addOnCompleteListener(RegistrationActivity.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {

                                        if (pDialog.isShowing())
                                            pDialog.dismiss();

                                        if (task.isSuccessful()) {

                                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                                            User uObj = new User(user.getUid(), et_name.getText().toString(), et_shop.getText().toString(), et_address.getText().toString(), et_phone.getText().toString(), et_fax.getText().toString(), et_email.getText().toString(),CommonObjects.pushNotificationToken);

                                            FirebaseDatabase.getInstance().getReference().child("userdata").child(user.getUid()).setValue(uObj);

                                            sendEmail(user);

                                            Snackbar.make(sv,"Thanks for registration "+et_name.getText().toString()+". Verification email has sent to "+et_email.getText().toString(),Snackbar.LENGTH_SHORT).show();

                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {

                                                    RegistrationActivity.this.finish();
                                                }
                                            }, 3000);

                                        } else {
                                            Snackbar.make(sv,task.getException().getMessage(),Snackbar.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                    }
                    else{
                        if (pDialog.isShowing())
                            pDialog.dismiss();

                        Snackbar.make(sv,"Please connect to the internet!",Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

               hideKeyboard();
            }
        }, 200);
    }

    private void sendEmail(FirebaseUser user) {
        user.sendEmailVerification()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {

                        }
                    }
                });
    }

    public void hideKeyboard(){
        try {
            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(sv.getWindowToken(), 0);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
