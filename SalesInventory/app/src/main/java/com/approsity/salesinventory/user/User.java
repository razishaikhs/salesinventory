package com.approsity.salesinventory.user;

/**
 * Created by Raziuddin.Shaikh on 1/3/2018.
 */

public class User {

    public User() {
    }

    public User(String userId, String userName, String shopName, String address, String telephone, String fax, String email,String push) {
        this.userId = userId;
        this.userName = userName;
        this.shopName = shopName;
        this.address = address;
        this.telephone = telephone;
        this.fax = fax;
        this.email = email;
        this.push = push;
    }

    public String userId;
    public String userName;
    public String shopName;
    public String address;
    public String telephone;
    public String fax;
    public String email;
    public String push;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPush() {
        return push;
    }

    public void setPush(String push) {
        this.push = push;
    }
}
