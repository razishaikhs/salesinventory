package com.approsity.salesinventory.sale;

import com.approsity.salesinventory.add.models.ItemModel;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;


/**
 * Created by Raziuddin.Shaikh on 1/1/2018.
 */

public class SaleItemModel extends RealmObject{


    public SaleItemModel() {
    }

    public SaleItemModel(int id, RealmList<ItemModel> itemList, String itemname, String qty, String price, String itemprofit) {
        this.id = id;
        this.itemList = itemList;
        this.itemname = itemname;
        this.qty = qty;
        this.price = price;
        this.itemprofit = itemprofit;
    }

    public int id;
    public RealmList<ItemModel> itemList;
    public String itemname;
    public String qty;
    public String price;
    public String itemprofit;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RealmList<ItemModel> getItemList() {
        return itemList;
    }

    public void setItemList(RealmList<ItemModel> itemList) {
        this.itemList = itemList;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getItemprofit() {
        return itemprofit;
    }

    public void setItemprofit(String itemprofit) {
        this.itemprofit = itemprofit;
    }
}

