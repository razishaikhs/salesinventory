package com.approsity.salesinventory.home;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.sale.SaleItemModel;
import com.approsity.salesinventory.sale.SaleModel;
import com.approsity.salesinventory.sale.SalesAdap;
import com.approsity.salesinventory.user.models.Customer;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import az.plainpie.PieView;
import az.plainpie.animation.PieAngleAnimation;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;


/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class Home extends Fragment{

    View mView;

    int year, month, day;

    EditText et_sdate;
    ImageView iv_sdate;

    TextView tv_sale,tv_profit;

    PieView animatedPie;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_home, container, false);

        showAds();

        animatedPie = (PieView) mView.findViewById(R.id.pieView);

        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        et_sdate = (EditText) mView.findViewById(R.id.et_sdate);
        iv_sdate = (ImageView) mView.findViewById(R.id.iv_sdate);

        tv_sale = (TextView) mView.findViewById(R.id.tv_sale);
        tv_profit = (TextView) mView.findViewById(R.id.tv_profit);

        String sale ="<i>Total Sale : </i>"+" "+ "0.0 " + "<i>rs</i>";
        String profit ="<i>Total Profit : </i>"+" "+ "0.0 " + "<i>rs</i>";

        tv_sale.setText(Html.fromHtml(sale), TextView.BufferType.SPANNABLE);
        tv_profit.setText(Html.fromHtml(profit), TextView.BufferType.SPANNABLE);

        setOperations();

        et_sdate.setText(day+"/"+month+1+"/"+year);

        Date d1 = CommonActions.stringToDateFormat( day+"/"+month+1+"/"+year+ " 12:00:00 AM");
        Date d2 = CommonActions.stringToDateFormat(day+"/"+month+1+"/"+year+" 11:59:59 PM");
        dayChart(d1,d2);

        return mView;
    }

    private void setOperations() {

        iv_sdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                et_sdate.setText("");
            }
        });


        et_sdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDate(et_sdate);
            }
        });

        et_sdate.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Date d1 = CommonActions.stringToDateFormat(s.toString().trim().toString()+ " 12:00:00 AM");
                Date d2 = CommonActions.stringToDateFormat(s.toString().trim().toString()+" 11:59:59 PM");

                dayChart(d1,d2);
            }
        });

    }

    public void dayChart(final Date d1,final Date d2){

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                List<SaleModel> sList = realm.where(SaleModel.class).between("dateTime",d1,d2).findAll();

                BigDecimal profit = new BigDecimal("0.0"),total = new BigDecimal("0.0");

                if(sList.size() > 0) {
                    for (int i = 0; i < sList.size(); i++) {

                        profit = profit.add(new BigDecimal(sList.get(i).totalprofit));
                        total = total.add(new BigDecimal(sList.get(i).total));
                    }

                    String sale ="<i>Total Sale : </i>"+" "+ total+" "+ "<i>rs</i>";
                    String profits ="<i>Total Profit : </i>"+" "+ profit+" " + "<i>rs</i>";

                    tv_sale.setText(Html.fromHtml(sale), TextView.BufferType.SPANNABLE);
                    tv_profit.setText(Html.fromHtml(profits), TextView.BufferType.SPANNABLE);

                    animatedPie.setPercentage(percentage(profit,total).floatValue());
                    PieAngleAnimation animation = new PieAngleAnimation(animatedPie);
                    animation.setDuration(1000);
                    animatedPie.startAnimation(animation);
                }
                else{

                    try {
                        Snackbar.make(mView, "No data found!", Snackbar.LENGTH_SHORT).show();
                    }catch(Exception e){
                        e.printStackTrace();
                    }

                    animatedPie.setPercentage(0f);
                    PieAngleAnimation animation = new PieAngleAnimation(animatedPie);
                    animation.setDuration(1000);
                    animatedPie.startAnimation(animation);

                    String sale ="<i>Total Sale : </i>"+" "+ "0.0 " + "<i>rs</i>";
                    String profits ="<i>Total Profit : </i>"+" "+ "0.0 " + "<i>rs</i>";

                    tv_sale.setText(Html.fromHtml(sale), TextView.BufferType.SPANNABLE);
                    tv_profit.setText(Html.fromHtml(profits), TextView.BufferType.SPANNABLE);
                }
            }
        });
    }


    public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    public static BigDecimal percentage(BigDecimal base, BigDecimal pct){

        if (base.compareTo(BigDecimal.ZERO) == 0)
            return new BigDecimal("0");

        if (pct.compareTo(BigDecimal.ZERO) == 0)
            return new BigDecimal("0");

        return (base.divide(pct,2, RoundingMode.HALF_UP)).multiply(ONE_HUNDRED);
    }

    public void setDate(final EditText et) {
        //showDate(year, month+1, day);
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
                        // TODO Auto-generated method stub
                        // arg1 = year
                        // arg2 = month
                        // arg3 = day
                        int month = arg2+1;
                        StringBuilder sb = new StringBuilder().append(arg3).append("/").append(month).append("/").append(arg1);
                        et.setText(sb.toString());

                    }
                }, year, month, day);

        datePicker.show();
    }

    View adContainer;
    private void showAds() {

        adContainer = mView.findViewById(R.id.adMobView);
        adContainer.setVisibility(View.GONE);

        AdView mAdView = new AdView(getActivity());
        mAdView.setAdSize(AdSize.BANNER);
        mAdView.setAdUnitId(CommonObjects.ADS_UNITID);
        ((RelativeLayout)adContainer).addView(mAdView);
        AdRequest adRequest = new AdRequest.Builder().build();

        if(mAdView.getAdUnitId() != null)
            mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                adContainer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
            }

            @Override
            public void onAdOpened() {
            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdLeftApplication() {
            }
        });
    }

}
