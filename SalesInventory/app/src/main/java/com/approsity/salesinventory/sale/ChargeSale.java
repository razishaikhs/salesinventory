package com.approsity.salesinventory.sale;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.category.CategoryDialog;
import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.add.models.Product;
import com.approsity.salesinventory.add.models.SubCategory;
import com.approsity.salesinventory.add.product.ProductDialog;
import com.approsity.salesinventory.add.subcategory.SubCategoryDialog;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.purchase.models.Purchase;
import com.approsity.salesinventory.user.customer.CustomerDialog;
import com.approsity.salesinventory.user.models.Customer;
import com.approsity.salesinventory.user.models.Vendor;
import com.approsity.salesinventory.user.vendor.VendorDialog;
import com.approsity.salesinventory.utils.CommonActions;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;


/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class ChargeSale extends Fragment implements View.OnClickListener{

    View mView;

    List<Customer> cList;
    List<String> payBuyList;

    Spinner spinner_cust,spinner_method;
    ImageView btn_add_cust;

    EditText et_total,et_recamount,et_return;

    Button btn_charge;

    LinearLayout ll_spinner_cust,ll_spinner_method;

    ScrollView sv;

    Animation shake;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_sale_charge, container, false);

        shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);

        spinner_cust = (Spinner) mView.findViewById(R.id.spinner_cust);
        spinner_method = (Spinner) mView.findViewById(R.id.spinner_method);

        btn_add_cust = (ImageView) mView.findViewById(R.id.btn_add_cust);
        btn_add_cust.setOnClickListener(this);

        et_total = (EditText) mView.findViewById(R.id.et_total);
        et_recamount = (EditText) mView.findViewById(R.id.et_recamount);
        et_return = (EditText) mView.findViewById(R.id.et_return);

        btn_charge = (Button) mView.findViewById(R.id.btn_charge);
        btn_charge.setOnClickListener(this);

        ll_spinner_cust = mView.findViewById(R.id.ll_spinner_cust);
        ll_spinner_method = mView.findViewById(R.id.ll_spinner_method);

        sv = (ScrollView) mView.findViewById(R.id.sv);

        getRealmData();

        setOperations();

        return mView;
    }

    public void getRealmData() {

        getRealmCustomer();
        getRealmMethod();
        getRealmSalesId();
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()) {
            case R.id.btn_add_cust:
                custDialog();
                break;
            case R.id.btn_charge:
                chargeButton();
                break;
            default:
        }
    }

    public void custDialog() {
        final CustomerDialog custDialog = CustomerDialog.newInstance(getActivity(), null);
        custDialog.setButtonClicked(new CustomerDialog.onButtonClicked() {
            @Override
            public void onClicked(final String name, final String contact, final String email, final String address) {
                custDialog.dismiss();

                Realm db = Realm.getDefaultInstance();
                db.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        Number currentIdNum = realm.where(Customer.class).max("id");
                        int nextId = 0;
                        if(currentIdNum == null) {
                            nextId = 1;
                        } else {
                            nextId = currentIdNum.intValue() + 1;
                        }

                        Customer fvData = new Customer(nextId, name, address ,contact,email);
                        realm.insert(fvData);

                        Snackbar.make(mView,"Customer Added Successfully!",Snackbar.LENGTH_SHORT).show();

                       /* RealmList<Customer> cl = new RealmList<>();
                        cl.add(fvData);
                        int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                        Transactions tObj = new Transactions(i,null,null,null,cl,null,null, CommonActions.currentDateTime(),"Vendor created",fvData.name);
                        realm.insertOrUpdate(tObj);*/

                        getRealmCustomer();

                    }
                });
            }
        });

        custDialog.show(getFragmentManager(),"add");
    }

    public void getRealmCustomer() {
        cList = new ArrayList<>();
        RealmResults<Customer> c = Realm.getDefaultInstance().where(Customer.class).sort("name",  Sort.ASCENDING).findAllAsync();
        cList = Realm.getDefaultInstance().copyFromRealm(c);

        List<String> slist = new ArrayList<>();
        for(Customer cat : cList) {
            slist.add(cat.name);
        }
        ArrayAdapter<String> spinner_catAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,slist);
        spinner_cust.setAdapter(spinner_catAdap);

    }

    public void getRealmMethod() {

        payBuyList = new ArrayList<>();

        payBuyList.add("CASH");
        payBuyList.add("CREDIT CARD");

        ArrayAdapter<String> spinner_scatAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,payBuyList);
        spinner_method.setAdapter(spinner_scatAdap);
    }

    public int getRealmSalesId() {

        Number currentIdNum = Realm.getDefaultInstance().where(SaleModel.class).max("id");

        int nextId = 0;
        if(currentIdNum == null) {
            nextId = 1;
        } else {
            nextId = currentIdNum.intValue() + 1;
        }

        return nextId;
    }

    public void setOperations() {

        et_total.setText(SalesList.saleObj.total);
        et_recamount.setHint("0.0");
        et_return.setText("0.0");

        et_recamount.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    BigDecimal bd = new BigDecimal(et_recamount.getText().toString());

                    if (bd.doubleValue() > 0) {

                        BigDecimal bd1 = new BigDecimal(et_total.getText().toString());
                        et_return.setText(bd.subtract(bd1)+ "");
                    }
                }catch(Exception e){
                    e.printStackTrace();
                    et_return.setText("0.0");
                }
            }
        });

        et_recamount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b) {
                    if(TextUtils.isEmpty(et_recamount.getText().toString())){
                        et_return.setText("0.0");
                    }

                    try {
                        BigDecimal bd = new BigDecimal(et_recamount.getText().toString());

                        if(bd.doubleValue() <= 0){
                            et_return.setText("0.0");
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                        et_return.setText("0.0");
                    }
                }
            }
        });

    }

    public void chargeButton() {

        if(cList.size() == 0)
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, ll_spinner_cust.getTop());
                }
            });

            Snackbar.make(mView,"Please add customer",Snackbar.LENGTH_SHORT).show();
            ll_spinner_cust.startAnimation(shake);
        }
        else if(TextUtils.isEmpty(et_recamount.getText().toString()))
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, et_recamount.getTop());
                }
            });
            et_recamount.requestFocus();
            et_recamount.setError("Please enter the amount");
            et_recamount.startAnimation(shake);
        }
        else
        {
            Realm db = Realm.getDefaultInstance();
            db.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    Customer cObj = cList.get(spinner_cust.getSelectedItemPosition());
                    String payBy = payBuyList.get(spinner_method.getSelectedItemPosition());
                    String recamount = et_recamount.getText().toString().toUpperCase();
                    String returnamount = et_return.getText().toString();

                    RealmList<Customer> clist = new RealmList<>();
                    clist.add(cObj);

                    for(int i=0;i<SalesList.saleObj.saleItemsList.size();i++){
                        ItemModel item = realm.where(ItemModel.class).equalTo("id",SalesList.saleObj.saleItemsList.get(i).itemList.get(0).id).findFirstAsync();
                        item.quantity = subtractBigDecimal(item.quantity, SalesList.saleObj.saleItemsList.get(i).qty.toString());
                        item.totalprice = multiplyBigDecimal(item.quantity,item.costperunit);
                        realm.insertOrUpdate(item);

                        RealmList<ItemModel> cl = new RealmList<>();
                        cl.add(item);
                        int id = Integer.parseInt(System.currentTimeMillis()/1000+"");
                        Transactions tObj = new Transactions(id,item.catList,item.subcatList,item.pList,null,cl,null, CommonActions.currentDateTime(),"Item sell "+SalesList.saleObj.saleItemsList.get(i).qty.toString()+" qty @"+SalesList.saleObj.saleItemsList.get(i).price.toString() +" rs",item.name);
                        realm.insertOrUpdate(tObj);
                    }

                    Log.e("Total Profit",SalesList.saleObj.totalprofit+" ");

                    Long tsLong = System.currentTimeMillis()/1000;
                    String ts = tsLong.toString();

                    SaleModel i = new SaleModel(getRealmSalesId(), SalesList.saleObj.saleItemsList, clist ,CommonActions.currentDateTime(), SalesList.saleObj.discount, SalesList.saleObj.total, payBy, returnamount, recamount,SalesList.saleObj.totalprofit,ts);
                    realm.insertOrUpdate(i);

                    Snackbar.make(mView,"Sale recorded successfully!",Snackbar.LENGTH_SHORT).show();

                    et_recamount.setText("");
                    et_total.setText("0.0");
                    et_recamount.setHint("0.0");
                    et_return.setText("0.0");

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            SalesList.saleChargedSuccessfully = true;
                            getActivity().finish();
                        }
                    }, 1000);
                }
            });
        }

    }

    public String subtractBigDecimal(String s, String s1){

        BigDecimal bd = new BigDecimal(s).subtract(new BigDecimal(s1));

        return bd+"";
    }

    public String multiplyBigDecimal(String s, String s1){

        BigDecimal bd = new BigDecimal(s).multiply(new BigDecimal(s1));

        return bd+"";
    }

}
