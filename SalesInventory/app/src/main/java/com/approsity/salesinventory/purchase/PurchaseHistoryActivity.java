package com.approsity.salesinventory.purchase;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.item.ItemHistory;


public class PurchaseHistoryActivity extends AppCompatActivity {
    
    int id;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_toolbar);

        Bundle extras = getIntent().getExtras();
        id = extras.getInt("vid");

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = new PurchaseHistory();
        Bundle args = new Bundle();
        args.putInt("vid", id);
        fragment.setArguments(args);
        transaction.replace(R.id.container,fragment).commit();

        ImageView backBtn = (ImageView) findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
    }

}