package com.approsity.salesinventory.utils;

import android.app.Activity;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.add.models.Product;
import com.approsity.salesinventory.add.models.SubCategory;
import com.approsity.salesinventory.purchase.models.Purchase;
import com.approsity.salesinventory.user.models.Vendor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmModel;

/**
 * Created by Raziuddin.Shaikh on 12/26/2017.
 */

public class CSVData {

    Activity act;
    View mView;

    String newColoumn = ",";
    String newLine = "\n";

    public static String items = "item.csv";
    public static String purchase = "purchase.csv";

    public CSVData(Activity act, View mView) {

        this.act = act;
        this.mView = mView;

        File createFolder = new File(CommonObjects.appPath);
        if(!createFolder.exists())
            createFolder.mkdir();

        new SingleMediaScanner(act, CommonObjects.appPath);
    }

    public void ItemToCSV() {

        List<ItemModel> iList = Realm.getDefaultInstance().where(ItemModel.class).findAll();

        if (iList.size() > 0) {

            String commaSeparatedValues = "";

            for(int i = 0; i<iList.size();i++) {

                if(i == 0) {
                    commaSeparatedValues += "Category Name" + newColoumn;
                    commaSeparatedValues += "Category Alias" + newColoumn;
                    commaSeparatedValues += "Category Desc" + newColoumn;
                    commaSeparatedValues += "Sub Category Name" + newColoumn;
                    commaSeparatedValues += "Sub Category Alias" + newColoumn;
                    commaSeparatedValues += "Sub Category Desc" + newColoumn;
                    commaSeparatedValues += "Product Name" + newColoumn;
                    commaSeparatedValues += "Product Alias" + newColoumn;
                    commaSeparatedValues += "Product Desc" + newColoumn;
                    commaSeparatedValues += "Item Name" + newColoumn;
                    commaSeparatedValues += "Item Alias" + newColoumn;
                    commaSeparatedValues += "Item Desc" + newColoumn;
                    commaSeparatedValues += "Item Type" + newColoumn;
                    commaSeparatedValues += "Item Size" + newColoumn;
                    commaSeparatedValues += "Item Qty" + newColoumn;
                    commaSeparatedValues += "Item Cost Per Unit" + newColoumn;
                    commaSeparatedValues += "Item Sale Price" + newLine;

                }

                for(Category ca : iList.get(i).catList) {
                    commaSeparatedValues += ca.name + newColoumn;
                    commaSeparatedValues += ca.alias + newColoumn;
                    commaSeparatedValues += ca.desc + newColoumn;
                }

                for(SubCategory sca : iList.get(i).subcatList) {
                    commaSeparatedValues += sca.name + newColoumn;
                    commaSeparatedValues += sca.alias + newColoumn;
                    commaSeparatedValues += sca.desc + newColoumn;
                }

                for(Product p : iList.get(i).pList) {
                    commaSeparatedValues += p.name + newColoumn;
                    commaSeparatedValues += p.alias + newColoumn;
                    commaSeparatedValues += p.desc + newColoumn;
                }

                commaSeparatedValues += iList.get(i).name + newColoumn;
                commaSeparatedValues += iList.get(i).alias + newColoumn;
                commaSeparatedValues += iList.get(i).desc + newColoumn;
                commaSeparatedValues += iList.get(i).type + newColoumn;
                commaSeparatedValues += iList.get(i).size + newColoumn;
                commaSeparatedValues += iList.get(i).quantity + newColoumn;
                commaSeparatedValues += iList.get(i).costperunit + newColoumn;
                commaSeparatedValues += iList.get(i).saleprice + newLine;

                writeToCSV(CommonObjects.appPath+File.separator+items, commaSeparatedValues);
            }

           /* if (commaSeparatedValues.endsWith(",")) {
                commaSeparatedValues = commaSeparatedValues.substring(0, commaSeparatedValues.lastIndexOf(","));
            }*/
        }
        else
            Snackbar.make(mView,"No items found",Snackbar.LENGTH_LONG).show();

    }

    private void writeToCSV(final String path, String value) {
        try {
            File f = new File(path);
            FileWriter fstream = new FileWriter(f, false);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(value);
            out.close();

            Snackbar.make(mView,"Backup taken successfully!",Snackbar.LENGTH_SHORT).show();

            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {

                    Snackbar.make(mView,"File saved to "+path,Snackbar.LENGTH_LONG).show();
                }
            }, 1000);

            new SingleMediaScanner(act, path);

        } catch (Exception e) {
            e.printStackTrace();
            Snackbar.make(mView,"Error while backing up!",Snackbar.LENGTH_SHORT).show();
        }
    }

    public void CSVToItem() {

        int catId = getMaxId(new Category());
        int scatId = getMaxId(new SubCategory());
        int pId = getMaxId(new Product());
        int itemId = getMaxId(new ItemModel());

        String bCode_cat="0000", bCode_subcat="0000", bCode_product="0000", bCode_item="0000";

        int total_items=0;

        File file = new File(CommonObjects.appPath+File.separator+items);
        if(file.exists()){
            try {

                BufferedReader fileReader = new BufferedReader(new FileReader(file));
                fileReader.readLine();

                String line = "";

                while ((line = fileReader.readLine()) != null) {

                    total_items++;

                    String[] row = line.split(newLine);

                    if (row.length > 0) {

                        ItemModel iData = new ItemModel();
                        itemId++;

                        String[] col = line.split(newColoumn);

                        Category checkCat = Realm.getDefaultInstance().where(Category.class).equalTo("name",col[0]).findFirst();
                        Category cat = new Category();
                        if(checkCat != null) {
                            cat = checkCat;
                        }
                        else {
                            cat.id = catId;
                            cat.name = col[0];
                            cat.alias = col[1];
                            cat.desc = col[2];

                            catId++;
                        }

                        SubCategory checkSubCat = Realm.getDefaultInstance().where(SubCategory.class).equalTo("name",col[3]).findFirst();
                        SubCategory scat = new SubCategory();
                        if(checkCat != null) {
                            scat = checkSubCat;
                        }
                        else {
                            scat.id = scatId;
                            scat.name = col[3];
                            scat.alias = col[4];
                            scat.desc = col[5];

                            scatId++;
                        }

                        Product checkProduct = Realm.getDefaultInstance().where(Product.class).equalTo("name",col[6]).findFirst();
                        Product pObj = new Product();
                        if(checkCat != null) {
                            pObj = checkProduct;
                        }
                        else {
                            pObj.id = pId;
                            pObj.name = col[6];
                            pObj.alias = col[7];
                            pObj.desc = col[8];

                            pId++;
                        }


                        RealmList<Category> c = new RealmList<>();
                        c.add(cat);

                        RealmList<SubCategory> sc = new RealmList<>();
                        sc.add(scat);

                        RealmList<Product> p = new RealmList<>();
                        p.add(pObj);

                        ItemModel checkItem = Realm.getDefaultInstance().where(ItemModel.class).equalTo("name",col[9]).findFirst();
                        if(checkItem != null) {
                            itemId = checkItem.id;
                        }
                        else {
                            iData.id = itemId;
                        }

                        iData.catList = c;
                        iData.subcatList = sc;
                        iData.pList = p;
                        iData.name = col[9];
                        iData.alias = col[10];
                        iData.desc = col[11];
                        iData.type = col[12];
                        iData.size = col[13];
                        iData.quantity = col[14];
                        iData.costperunit = col[15];
                        iData.totalprice = multiplyBigDecimal(col[14],col[15])+"";
                        iData.saleprice = col[16];
                        iData.datetime = CommonActions.currentDateTime();

                        bCode_cat = String.format("%04d", c.get(0).id);
                        bCode_subcat = String.format("%04d", sc.get(0).id);
                        bCode_product = String.format("%04d", p.get(0).id);
                        bCode_item = String.format("%04d", itemId);

                        iData.barcode = bCode_cat +"  "+bCode_subcat +"  "+ bCode_product+"  "+ bCode_item;

                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        realm.insertOrUpdate(iData);
                        realm.commitTransaction();
                    }

                }

                Snackbar.make(mView,"Restore "+total_items+" items successfully!",Snackbar.LENGTH_SHORT).show();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Snackbar.make(mView,"File name should be "+items,Snackbar.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
                Snackbar.make(mView,"File format Error",Snackbar.LENGTH_SHORT).show();

            }

        }else{
            Snackbar.make(mView,"Please place the file to "+CommonObjects.appPath,Snackbar.LENGTH_SHORT).show();
        }
    }

    public void PurchaseToCSV() {

        List<Purchase> pList = Realm.getDefaultInstance().where(Purchase.class).findAll();

        if (pList.size() > 0) {

            String commaSeparatedValues = "";

            for(int i = 0; i<pList.size();i++) {

                if(i == 0) {
                    commaSeparatedValues += "Vendor Name" + newColoumn;
                    commaSeparatedValues += "Vendor Contact No" + newColoumn;
                    commaSeparatedValues += "Vendor Email" + newColoumn;
                    commaSeparatedValues += "Vendor Address" + newColoumn;
                    commaSeparatedValues += "Invoice" + newColoumn;
                    commaSeparatedValues += "Item Name" + newColoumn;
                    commaSeparatedValues += "Purchase Alias" + newColoumn;
                    commaSeparatedValues += "Pay by" + newColoumn;
                    commaSeparatedValues += "Purchase Desc" + newColoumn;
                    commaSeparatedValues += "Purchase Type" + newColoumn;
                    commaSeparatedValues += "Purchase Size" + newColoumn;
                    commaSeparatedValues += "Purchase Qty" + newColoumn;
                    commaSeparatedValues += "Purchase Total Price" + newColoumn;
                    commaSeparatedValues += "Purchase Sale Price" + newLine;
                }

                try {
                    Vendor vObj = pList.get(i).vendorList.get(0);
                    commaSeparatedValues += vObj.name + newColoumn;
                    commaSeparatedValues += vObj.contactno + newColoumn;
                    commaSeparatedValues += vObj.email + newColoumn;
                    commaSeparatedValues += vObj.address + newColoumn;
                }catch(Exception e){
                    e.printStackTrace();
                    commaSeparatedValues += pList.get(i).vendorname + newColoumn;
                    commaSeparatedValues += "" + newColoumn;
                    commaSeparatedValues += "" + newColoumn;
                    commaSeparatedValues += "" + newColoumn;
                }

                commaSeparatedValues += pList.get(i).invoice + newColoumn;
                commaSeparatedValues += pList.get(i).itemname+ newColoumn;
                commaSeparatedValues += pList.get(i).alias + newColoumn;
                commaSeparatedValues += pList.get(i).method + newColoumn;
                commaSeparatedValues += pList.get(i).desc + newColoumn;
                commaSeparatedValues += pList.get(i).itemtype + newColoumn;
                commaSeparatedValues += pList.get(i).itemsize + newColoumn;
                commaSeparatedValues += pList.get(i).quantity + newColoumn;
                commaSeparatedValues += pList.get(i).cost + newColoumn;

                try {
                    commaSeparatedValues += pList.get(i).itemList.get(0).saleprice + newLine;
                }catch (Exception e){
                    e.printStackTrace();
                    commaSeparatedValues += "0.0" + newLine;
                }
               // commaSeparatedValues += CommonActions.dateToStringFormatWithSeconds(pList.get(i).datetime) + newLine;

                writeToCSV(CommonObjects.appPath+File.separator+purchase, commaSeparatedValues);
            }

           /* if (commaSeparatedValues.endsWith(",")) {
                commaSeparatedValues = commaSeparatedValues.substring(0, commaSeparatedValues.lastIndexOf(","));
            }*/
        }
        else
            Snackbar.make(mView,"No purchases found",Snackbar.LENGTH_LONG).show();

    }

    public void CSVToPurchase() {

        int venId = getMaxId(new Vendor());
        int itemId = getMaxId(new ItemModel());
        int purId = getMaxId(new Purchase());

        int total_items = 0;

        File file = new File(CommonObjects.appPath+File.separator+purchase);
        if(file.exists()){
            try {

                BufferedReader fileReader = new BufferedReader(new FileReader(file));
                fileReader.readLine();

                String line = "";

                while ((line = fileReader.readLine()) != null) {

                    total_items++;

                    String[] row = line.split(newLine);

                    if (row.length > 0) {

                        Purchase purData = new Purchase();
                        purId++;

                        String[] col = line.split(newColoumn);

                        Vendor checkVen = Realm.getDefaultInstance().where(Vendor.class).equalTo("name",col[0]).findFirst();
                        Vendor ven = new Vendor();
                        if(checkVen != null) {
                            ven = checkVen;
                        }
                        else {
                            ven.id = venId;
                            ven.name = col[0];
                            ven.contactno = col[1];
                            ven.email = col[2];
                            ven.address = col[3];
                            venId++;
                        }

                        Realm realm = Realm.getDefaultInstance();
                        ItemModel checkItem = realm.where(ItemModel.class).equalTo("name",col[5]).findFirst();
                        ItemModel item = new ItemModel();
                        if(checkItem != null) {

                            realm.beginTransaction();
                            checkItem.quantity = addBigDecimal(col[11],checkItem.quantity)+"";
                            checkItem.totalprice = addBigDecimal(col[12],checkItem.totalprice)+"";
                            checkItem.costperunit = divideBigDecimal(checkItem.totalprice,checkItem.quantity)+"";
                            realm.commitTransaction();

                            item = checkItem;
                        }
                        else {
                            item.id = itemId;
                            item.catList = null;
                            item.subcatList = null;
                            item.pList = null;
                            item.name = col[5];
                            item.alias = "";
                            item.desc = "";
                            item.type = "";
                            item.size = "";
                            item.quantity = col[11];
                            item.totalprice = col[12];
                            item.costperunit = divideBigDecimal(col[11],col[12])+"";
                            item.saleprice = col[13];
                            item.barcode = "";
                            item.datetime = CommonActions.currentDateTime();

                            itemId++;
                        }

                        RealmList<Vendor> v = new RealmList<>();
                        v.add(ven);

                        RealmList<ItemModel> it = new RealmList<>();
                        it.add(item);

                        purData.id = purId;
                        purData.vendorList = v;
                        purData.itemList = it;
                        purData.invoice = col[4];
                        purData.quantity = col[11];
                        purData.cost = col[12];
                        purData.desc = col[11];
                        purData.datetime = CommonActions.currentDateTime();
                        purData.alias = col[6];
                        purData.method = col[7];
                        purData.vendorname = v.get(0).name;
                        purData.itemname = it.get(0).name;
                        purData.itemtype = it.get(0).type;
                        purData.itemsize = it.get(0).size;

                        realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        realm.insert(purData);
                        realm.commitTransaction();
                    }

                }

                Snackbar.make(mView,"Restore "+total_items+" items successfully!",Snackbar.LENGTH_SHORT).show();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Snackbar.make(mView,"File name should be "+items,Snackbar.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
                Snackbar.make(mView,"File format Error",Snackbar.LENGTH_SHORT).show();

            }

        }else{
            Snackbar.make(mView,"Please place the file to "+CommonObjects.appPath,Snackbar.LENGTH_SHORT).show();
        }
    }

    public int getMaxId(RealmModel m) {
        Number currentIdNum = Realm.getDefaultInstance().where(m.getClass()).max("id");
        int nextId = 0;
        if(currentIdNum == null) {
            nextId = 0;
        } else {
            nextId = currentIdNum.intValue();
        }

        return nextId;
    }

    private class SingleMediaScanner implements MediaScannerConnection.MediaScannerConnectionClient {
        private MediaScannerConnection mMs;
        private String path;

        SingleMediaScanner(Context context, String f) {
            path = f;
            mMs = new MediaScannerConnection(context, this);
            mMs.connect();
        }
        @Override
        public void onMediaScannerConnected() {
            mMs.scanFile(path, null);
        }
        @Override
        public void onScanCompleted(String path, Uri uri) {
            mMs.disconnect();
        }
    }

    public BigDecimal addBigDecimal(String s1,String s2) {
        BigDecimal bd1 = new BigDecimal(s1);
        BigDecimal bd2 = new BigDecimal(s2);

        return bd1.add(bd2);
    }

    public BigDecimal divideBigDecimal(String cost,String quantity) {

        try {
            BigDecimal cbd = new BigDecimal(cost);
            BigDecimal qbd = new BigDecimal(quantity);

            return cbd.divide(qbd, 2, RoundingMode.HALF_UP);
        }catch ( Exception e){
            e.printStackTrace();
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal multiplyBigDecimal(String s1, String s2) {
        try {
            BigDecimal b1 = new BigDecimal(s1);
            BigDecimal b2 = new BigDecimal(s2);
            BigDecimal b = b1.multiply(b2);

            return b;
        }catch(Exception e){
            e.printStackTrace();
            return BigDecimal.ZERO;
        }
    }
}
