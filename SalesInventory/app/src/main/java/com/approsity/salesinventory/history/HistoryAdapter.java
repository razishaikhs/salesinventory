package com.approsity.salesinventory.history;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import com.approsity.salesinventory.R;
import com.approsity.salesinventory.history.model.ExpendableTransactionsModel;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.utils.CommonActions;

import java.util.List;

/**
 * Created by Raziuddin.Shaikh on 12/15/2017.
 */

public class HistoryAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<ExpendableTransactionsModel> groupList;


    public HistoryAdapter(Context context, List<ExpendableTransactionsModel> groupList) {
        this.context = context;
        this.groupList = groupList;
    }

    @Override
    public Transactions getChild(int listPosition, int expandedListPosition) {

        return this.groupList.get(listPosition).tList.get(expandedListPosition);
        //return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String expandedListText = (String) getChild(listPosition, expandedListPosition).name;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.frag_transhistory_child_row, null);
        }

        TextView text_time = (TextView) convertView.findViewById(R.id.text_time);
        TextView text_desc = (TextView) convertView.findViewById(R.id.text_desc);
        TextView text_name = (TextView) convertView.findViewById(R.id.text_name);

        text_name.setText(getChild(listPosition, expandedListPosition).name);
        text_time.setText(CommonActions.dateToStringFormatWithSeconds(getChild(listPosition, expandedListPosition).datetime).split(" ",2)[1]);
        text_desc.setText(getChild(listPosition, expandedListPosition).desc);


        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.groupList.get(listPosition).tList.size();
       // return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
    }

    @Override
    public ExpendableTransactionsModel getGroup(int listPosition) {
        return this.groupList.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.groupList.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String listTitle = (String) getGroup(listPosition).title;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.frag_transhistory_group_row, null);
        }

        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.text_date);
        listTitleTextView.setText(listTitle);


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

}