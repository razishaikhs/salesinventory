package com.approsity.salesinventory;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;


import java.util.HashMap;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Raziuddin.Shaikh on 12/15/2017.
 */

public class MenuAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<MenuGroupModel> groupList;


    public MenuAdapter(Context context, List<MenuGroupModel> groupList) {
        this.context = context;
        this.groupList = groupList;
    }

    @Override
    public MenuItemsModel getChild(int listPosition, int expandedListPosition) {

        return this.groupList.get(listPosition).iList.get(expandedListPosition);
        //return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String expandedListText = (String) getChild(listPosition, expandedListPosition).title;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.sidemenulist_item, null);
        }

        TextView expandedListTextView = (TextView) convertView.findViewById(R.id.expandedListItem);
        expandedListTextView.setText(expandedListText);

        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.groupList.get(listPosition).iList.size();
       // return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
    }

    @Override
    public MenuGroupModel getGroup(int listPosition) {
        return this.groupList.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.groupList.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String listTitle = (String) getGroup(listPosition).title;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.sidemenulist_group, null);
        }

        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.listTitle);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);

        listTitleTextView.setCompoundDrawablesWithIntrinsicBounds(getGroup(listPosition).drawable, 0, 0, 0);


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

}