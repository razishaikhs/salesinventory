package com.approsity.salesinventory;

import android.support.v4.app.Fragment;

import java.util.List;

/**
 * Created by Raziuddin.Shaikh on 12/15/2017.
 */

public class MenuGroupModel {

    public String title;
    public int drawable;
    public List<MenuItemsModel> iList;
    public Fragment frag;

    public MenuGroupModel(String title, int drawable, List<MenuItemsModel> iList, Fragment frag) {
        this.title = title;
        this.drawable = drawable;
        this.iList = iList;
        this.frag = frag;
    }

    @Override
    public String toString() {
        return "MenuGroupModel{" +
                "title='" + title + '\'' +
                ", drawable=" + drawable +
                ", iList=" + iList +
                ", frag=" + frag +
                '}';
    }
}
