package com.approsity.salesinventory.utils;

import android.content.Context;
import android.os.Environment;
import android.view.View;

import com.approsity.salesinventory.notification.NotificationData;
import com.approsity.salesinventory.user.User;

import java.io.File;
import java.util.List;


public class CommonObjects {

    /////In-App Key ////
    public static String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAplkrBL9yNUDc7IaEoamFu8Jh56c6CPCjmbu2RbT/46kMDWlZt9gEmB8umhSHULyKQBBuWe+CIX4Zk7d/KLaN5TN1CZkFM4HcquCfPg9EbTf4fsbfm10moD6EXVGfIJZlJKspBcA3QVrSy8Bgzy/kmvtgcWrhga7tHaLGpqbfiScSLjNIBCS9ncEhL55LzOk43SMzX0uy0Vrq3tlpPEDqUkFl/8jCtBSRaxQrweb0LQo5PXGJWHfFu01SzHrCYE6Fpk05za5eGELQ9zrt1MnRNh9SK2OY3y5Va1nvRjud6iqmuDck2R576+aKJ1trl1w+qXCbl29AEbSUsyvsXrtKcQIDAQAB";
    // (arbitrary) request code for the purchase flow
    public static int RC_REQUEST = 10001;

    public static String SKUID = "android.test.purchased";
    ///////////////

    public static String pushNotificationToken;

    public static String ADS_APPID;
    public static String ADS_UNITID;

    public static User uObj;

    public interface RecyclerViewClickListener {
        public void recyclerViewListClicked(View v, int position, Context con);
    }

    public static int itemPos = 0;

    public static String appPath = Environment.getExternalStorageDirectory()+"/POS";

    public static String firebaseStoragePath = "pos/databbases/";
    public static String REALM_FILE_NAME = "pos.realm";

    public static boolean VendorPurchaseCheck;

    public enum OpenFragments {
        chargesale,
        saleHistory,
        saleHistoryDetail,
        barcode,
        profile
    }

    public static List<NotificationData> notiList;
    public static final int UNREAD = 0;
    public static final int READ = 1;


}
