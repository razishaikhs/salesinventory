package com.approsity.salesinventory.sale;


import com.approsity.salesinventory.history.model.Transactions;

import java.util.List;


/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class ExpendableSalesModel {

    public ExpendableSalesModel() {
    }


    public ExpendableSalesModel(String title, List<SaleModel> tList) {
        this.title = title;
        this.tList = tList;
    }

    public String title;
    public List<SaleModel> tList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SaleModel> gettList() {
        return tList;
    }

    public void settList(List<SaleModel> tList) {
        this.tList = tList;
    }

}
