package com.approsity.salesinventory.add.item;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.category.CategoryDialog;
import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.add.models.Product;
import com.approsity.salesinventory.add.models.SubCategory;
import com.approsity.salesinventory.add.product.ProductDialog;
import com.approsity.salesinventory.add.subcategory.SubCategoryDialog;
import com.approsity.salesinventory.history.model.Transactions;
import com.approsity.salesinventory.utils.CommonActions;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;


public class ItemDialog extends DialogFragment implements View.OnClickListener{

    View v;

    List<Category> cList;
    List<SubCategory> scList;
    List<Product> pList;

    Spinner spinner_cat,spinner_subcat,spinner_product;
    EditText et_name, et_alias, et_desc, et_type,et_size,et_quantity,et_costperunit,et_totalprice,et_saleprice,et_barcode;
    ImageView btn_add_cat,btn_add_subcat,btn_add_product;

    Button btn_add;
    
    ImageView iv_barcode;

    LinearLayout ll_spinner_cat,ll_spinner_subcat, ll_spinner_product;

    ScrollView sv;

    Activity act;

    Animation shake;

    ItemModel Obj;

    String bCode_cat="0000", bCode_subcat="0000", bCode_product="0000", bCode_item="0000";

    public static ItemDialog newInstance(Activity activity, ItemModel Obj) {

        ItemDialog frag = new ItemDialog();
        frag.act = activity;
        frag.Obj = Obj;
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setStyle(STYLE_NO_FRAME, R.style.DialogTheme);
        setCancelable(true);

    }



    public View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.dialog_item_add, container, false);

        shake = AnimationUtils.loadAnimation(act, R.anim.shake);

        btn_add = (Button) v.findViewById(R.id.btn_add);
        btn_add.setOnClickListener(this);
        btn_add.setText("Update");
        btn_add_cat = (ImageView) v.findViewById(R.id.btn_add_cat);
        btn_add_cat.setOnClickListener(this);
        btn_add_subcat = (ImageView) v.findViewById(R.id.btn_add_subcat);
        btn_add_subcat.setOnClickListener(this);
        btn_add_product = (ImageView) v.findViewById(R.id.btn_add_product);
        btn_add_product.setOnClickListener(this);

        et_name = (EditText) v.findViewById(R.id.et_name);
        et_alias = (EditText) v.findViewById(R.id.et_alias);
        et_desc = (EditText) v.findViewById(R.id.et_desc);
        et_type = (EditText) v.findViewById(R.id.et_type);
        et_size = (EditText) v.findViewById(R.id.et_size);
        et_quantity = (EditText) v.findViewById(R.id.et_quantity);
        et_costperunit = (EditText) v.findViewById(R.id.et_costperunit);
        et_totalprice = (EditText) v.findViewById(R.id.et_totalprice);
        et_saleprice = (EditText) v.findViewById(R.id.et_saleprice);
        et_barcode = (EditText) v.findViewById(R.id.et_barcode);
        
        spinner_cat = (Spinner) v.findViewById(R.id.spinner_cat);
        spinner_subcat = (Spinner) v.findViewById(R.id.spinner_subcat);
        spinner_product = (Spinner) v.findViewById(R.id.spinner_product);

        iv_barcode = (ImageView) v.findViewById(R.id.iv_barcode);

        ll_spinner_cat = v.findViewById(R.id.ll_spinner_cat);
        ll_spinner_subcat = v.findViewById(R.id.ll_spinner_subcat);
        ll_spinner_product = v.findViewById(R.id.ll_spinner_product);

        sv = (ScrollView) v.findViewById(R.id.sv);

        et_name.setText(Obj.name);
        et_alias.setText(Obj.alias);
        et_desc.setText(Obj.desc);
        et_type.setText(Obj.type);
        et_size.setText(Obj.size);
        et_quantity.setText(Obj.quantity);
        et_costperunit.setText(Obj.costperunit);
        et_totalprice.setText(Obj.totalprice);
        et_saleprice.setText(Obj.saleprice);
        et_barcode.setText(Obj.barcode);

        bCode_item = String.format("%04d", Obj.id);

       // et_barcode.setText(Obj.barcode);


        setSpinnerData();

        setOperations();

        return v;
    }

    public void setSpinnerData() {
        getRealmCategory();
        getRealmSubCategory();
        getRealmProduct();
    }

    public void catDialog() {

        final CategoryDialog categoryDialog = CategoryDialog.newInstance(getActivity(), null);
        categoryDialog.setButtonClicked(new CategoryDialog.onButtonClicked() {
            @Override
            public void onClicked(final String name, final String alias, final String desc) {
                categoryDialog.dismiss();

                Realm db = Realm.getDefaultInstance();
                db.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        Number currentIdNum = realm.where(Category.class).max("id");
                        int nextId = 0;
                        if(currentIdNum == null) {
                            nextId = 1;
                        } else {
                            nextId = currentIdNum.intValue() + 1;
                        }

                        Category fvData = new Category(nextId, name, alias ,desc);
                        realm.insert(fvData);

                        Snackbar.make(v,"Category Added Successfully!",Snackbar.LENGTH_SHORT).show();

                        RealmList<Category> cl = new RealmList<>();
                        cl.add(fvData);
                        int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                        Transactions tObj = new Transactions(i,cl,null,null,null,null,null, CommonActions.currentDateTime(),"Category created",fvData.name);
                        realm.insertOrUpdate(tObj);

                        getRealmCategory();

                    }
                });
            }
        });

        categoryDialog.show(getFragmentManager(),"add");
    }

    public void subcatDialog() {

        final SubCategoryDialog addDialog = SubCategoryDialog.newInstance(getActivity(), null);
        addDialog.setButtonClicked(new SubCategoryDialog.onButtonClicked() {
            @Override
            public void onClicked(final String name, final String alias, final String desc) {
                addDialog.dismiss();

                Realm db = Realm.getDefaultInstance();
                db.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        Number currentIdNum = realm.where(SubCategory.class).max("id");
                        int nextId = 0;
                        if(currentIdNum == null) {
                            nextId = 1;
                        } else {
                            nextId = currentIdNum.intValue() + 1;
                        }

                        SubCategory fvData = new SubCategory(nextId, name, alias ,desc);
                        realm.insert(fvData);

                        Snackbar.make(v,"SubCategory Added Successfully!",Snackbar.LENGTH_SHORT).show();

                        RealmList<SubCategory> cl = new RealmList<>();
                        cl.add(fvData);
                        int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                        Transactions tObj = new Transactions(i,null,cl,null,null,null,null, CommonActions.currentDateTime(),"Sub category created",fvData.name);
                        realm.insertOrUpdate(tObj);

                        getRealmSubCategory();
                    }
                });
            }
        });

        addDialog.show(getFragmentManager(),"add");
    }

    public void productDialog() {

        final ProductDialog addDialog = ProductDialog.newInstance(getActivity(), null);
        addDialog.setButtonClicked(new ProductDialog.onButtonClicked() {
            @Override
            public void onClicked(final String name, final String alias, final String desc) {
                addDialog.dismiss();

                Realm db = Realm.getDefaultInstance();
                db.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        Number currentIdNum = realm.where(Product.class).max("id");
                        int nextId = 0;
                        if(currentIdNum == null) {
                            nextId = 1;
                        } else {
                            nextId = currentIdNum.intValue() + 1;
                        }

                        Product fvData = new Product(nextId, name, alias ,desc);
                        realm.insert(fvData);

                        Snackbar.make(v,"Product Added Successfully!",Snackbar.LENGTH_SHORT).show();

                        RealmList<Product> cl = new RealmList<>();
                        cl.add(fvData);
                        int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                        Transactions tObj = new Transactions(i,null,null,cl,null,null,null, CommonActions.currentDateTime(),"Product created",fvData.name);
                        realm.insertOrUpdate(tObj);

                        getRealmProduct();
                    }
                });
            }
        });

        addDialog.show(getFragmentManager(),"add");
    }

    public void getRealmCategory() {

        int pos=0;

        cList = new ArrayList<>();
        RealmResults<Category> c = Realm.getDefaultInstance().where(Category.class).findAllSortedAsync("name",  Sort.ASCENDING);
        cList = Realm.getDefaultInstance().copyFromRealm(c);

        List<String> slist = new ArrayList<>();
        for(int i=0;i<cList.size();i++) {

            slist.add(cList.get(i).name);

            try {
                if (Obj.catList != null)
                    if (Obj.catList.get(0).id == cList.get(i).id)
                        pos = i;
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        ArrayAdapter<String> spinner_catAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,slist);
        spinner_cat.setAdapter(spinner_catAdap);

        spinner_cat.setSelection(pos);

        spinner_cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                bCode_cat = String.format("%04d", cList.get(position).id);
                barCodeGenerator();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

    }

    public void getRealmSubCategory() {

        int pos = 0;

        scList = new ArrayList<>();
        RealmResults<SubCategory> sc =  Realm.getDefaultInstance().where(SubCategory.class).findAllSortedAsync("name",  Sort.ASCENDING);
        scList =  Realm.getDefaultInstance().copyFromRealm(sc);

        List<String> sclist = new ArrayList<>();
        for(int i=0;i<scList.size();i++) {

            sclist.add(scList.get(i).name);

            try{
            if(Obj.subcatList != null)
            if(Obj.subcatList.get(0).id == scList.get(i).id)
                pos = i;
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        ArrayAdapter<String> spinner_scatAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,sclist);
        spinner_subcat.setAdapter(spinner_scatAdap);
        spinner_subcat.setSelection(pos);
        spinner_subcat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                bCode_subcat = String.format("%04d", scList.get(position).id);
                barCodeGenerator();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

    }

    public void getRealmProduct() {

        int pos = 0;

        pList = new ArrayList<>();
        RealmResults<Product> pObj = Realm.getDefaultInstance().where(Product.class).sort("name",  Sort.ASCENDING).findAllAsync();
        pList = Realm.getDefaultInstance().copyFromRealm(pObj);

        List<String> plist = new ArrayList<>();
        for(int i=0;i<pList.size();i++) {

            plist.add(pList.get(i).name);

            try {
                if (Obj.pList != null)
                    if (Obj.pList.get(0).id == pList.get(i).id)
                        pos = i;
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        ArrayAdapter<String> spinner_productAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,plist);
        spinner_product.setAdapter(spinner_productAdap);
        spinner_product.setSelection(pos);
        spinner_product.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                bCode_product = String.format("%04d", pList.get(position).id);
                barCodeGenerator();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

    }

    public void setOperations() {


        et_quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(!TextUtils.isEmpty(et_quantity.getText().toString()) && !TextUtils.isEmpty(et_costperunit.getText().toString())) {
                    et_totalprice.setText(calculateBigDecimal(et_quantity.getText().toString(),et_costperunit.getText().toString())+"");
                }
            }
        });

        et_quantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b) {
                    if(TextUtils.isEmpty(et_quantity.getText().toString())){
                        et_quantity.setText(Obj.quantity);
                    }

                    BigDecimal bd = new BigDecimal(et_quantity.getText().toString());

                    if(bd.doubleValue() <= 0){
                        et_quantity.setText(Obj.quantity);
                    }
                }
            }
        });

        et_costperunit.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(!TextUtils.isEmpty(et_quantity.getText().toString()) && !TextUtils.isEmpty(et_costperunit.getText().toString())) {
                    BigDecimal b1 = new BigDecimal(et_quantity.getText().toString());
                    BigDecimal b2 = new BigDecimal(et_costperunit.getText().toString());
                    BigDecimal b = b1.multiply(b2);
                    et_totalprice.setText(b+"");
                }
            }
        });

        et_costperunit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b) {

                    if(TextUtils.isEmpty(et_costperunit.getText().toString())){
                        et_costperunit.setText(Obj.costperunit);
                    }

                    BigDecimal bd = new BigDecimal(et_costperunit.getText().toString());

                    if(bd.doubleValue() <= 0){
                        et_costperunit.setText(Obj.costperunit);
                    }
                }
            }
        });

        et_saleprice.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b) {

                    if(TextUtils.isEmpty(et_saleprice.getText().toString())){
                        et_saleprice.setText(Obj.saleprice);
                    }

                    BigDecimal bd = new BigDecimal(et_saleprice.getText().toString());

                    if(bd.doubleValue() <= 0){
                        et_saleprice.setText(Obj.saleprice);
                    }
                }
            }
        });

    }

    public BigDecimal calculateBigDecimal(String s1, String s2) {
        BigDecimal b1 = new BigDecimal(s1);
        BigDecimal b2 = new BigDecimal(s2);
        BigDecimal b = b1.multiply(b2);

        return b;
    }

    public void barCodeGenerator() {

        et_barcode.setText(bCode_cat +bCode_subcat +bCode_product+bCode_item);

        String text= et_barcode.getText().toString();

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.CODE_128,600,150);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            iv_barcode.setImageBitmap(bitmap);

//            bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE,100,100);
//            barcodeEncoder = new BarcodeEncoder();
//            bitmap = barcodeEncoder.createBitmap(bitMatrix);
//            BitmapDrawable drawableEnd = new BitmapDrawable(getResources(), bitmap);
//            et_barcode.setCompoundDrawablesWithIntrinsicBounds(null,null, drawableEnd,null);

        } catch (WriterException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {

        switch(view.getId()) {
            case R.id.btn_add_cat:
                catDialog();
                break;
            case R.id.btn_add_subcat:
                subcatDialog();
                break;
            case R.id.btn_add_product:
                productDialog();
                break;
            case R.id.btn_add:
                UpdateButton();
                break;
            default:
        }
    }

    public void UpdateButton() {

        if(cList.size() == 0)
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, ll_spinner_cat.getTop());
                }
            });

            Snackbar.make(v,"Please add Category",Snackbar.LENGTH_SHORT).show();
            ll_spinner_cat.startAnimation(shake);
        }
        else if(scList.size() == 0)
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, ll_spinner_subcat.getTop());
                }
            });
            Snackbar.make(v,"Please add Sub Category",Snackbar.LENGTH_SHORT).show();
            ll_spinner_subcat.startAnimation(shake);
        }
        else if(pList.size() == 0)
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, ll_spinner_product.getTop());
                }
            });
            Snackbar.make(v,"Please add Product",Snackbar.LENGTH_SHORT).show();
            ll_spinner_product.startAnimation(shake);
        }
        else if(TextUtils.isEmpty(et_name.getText().toString()))
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, et_name.getTop());
                }
            });
            et_name.requestFocus();
            et_name.setError("Please add the name");
            et_name.startAnimation(shake);
        }
        /*else if(TextUtils.isEmpty(et_barcode.getText().toString()))
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, et_barcode.getTop());
                }
            });
            et_barcode.requestFocus();
            et_barcode.setError("Please add the barcode");
            et_barcode.startAnimation(shake);
        }
        else if(checkBarcode(et_barcode.getText().toString()))
        {
            sv.post(new Runnable() {
                @Override
                public void run() {
                    sv.scrollTo(0, et_barcode.getTop());
                }
            });
            et_barcode.requestFocus();
            et_barcode.setError("Already taken");
            et_barcode.startAnimation(shake);
        }*/
        else
        {
            Realm db = Realm.getDefaultInstance();
            db.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    Category cObj = cList.get(spinner_cat.getSelectedItemPosition());
                    SubCategory scObj = scList.get(spinner_subcat.getSelectedItemPosition());
                    Product pObj = pList.get(spinner_product.getSelectedItemPosition());

                    String name = et_name.getText().toString().toUpperCase();
                    String alis = et_alias.getText().toString().toUpperCase();
                    String desc = et_desc.getText().toString();
                    String type = et_type.getText().toString();
                    String size = et_size.getText().toString();

                    String quantity = et_quantity.getText().toString();
                    String cpu = et_costperunit.getText().toString();
                    String tp = et_totalprice.getText().toString();
                    String sp = et_saleprice.getText().toString();
                    String barCode = et_barcode.getText().toString();

                    ItemModel toEdit = realm.where(ItemModel.class).equalTo("id", Obj.id).findFirst();

                    RealmList<Category> clist = new RealmList<>();
                    clist.add(cObj);

                    RealmList<SubCategory> sclist = new RealmList<>();
                    sclist.add(scObj);

                    RealmList<Product> plist = new RealmList<>();
                    plist.add(pObj);

                    if(toEdit != null) {

                        toEdit.catList.addAll(clist);
                        toEdit.subcatList.addAll(sclist);
                        toEdit.pList.addAll(plist);

                        toEdit.name = name;
                        toEdit.alias = alis;
                        toEdit.desc = desc;
                        toEdit.type = type;
                        toEdit.size = size;
                        toEdit.quantity = quantity;
                        toEdit.costperunit = cpu;
                        toEdit.totalprice = tp;
                        toEdit.saleprice = sp;
                        toEdit.barcode = barCode;
                    }

                    realm.insertOrUpdate(toEdit);

                    Snackbar.make(v,name + " item updated successfully!",Snackbar.LENGTH_SHORT).show();

                    bCode_item = String.format("%04d", Integer.parseInt(bCode_item) + 1);
                    barCodeGenerator();

                    RealmList<ItemModel> cl = new RealmList<>();
                    cl.add(toEdit);
                    int i = Integer.parseInt(System.currentTimeMillis()/1000+"");
                    Transactions tObj = new Transactions(i,toEdit.catList,toEdit.subcatList,toEdit.pList,null,cl,null, CommonActions.currentDateTime(),"Item updated by editing "+et_quantity.getText().toString()+" qty @"+et_totalprice.getText().toString() +" rs",toEdit.name);
                    realm.insertOrUpdate(tObj);

                    et_name.setText("");
                    et_alias.setText("");
                    et_desc.setText("");
                    et_type.setText("");
                    et_size.setText("");

                    et_quantity.setText("1");
                    et_costperunit.setText("1.0");
                    et_totalprice.setText("1.0");
                    et_saleprice.setText("1.0");

                    dismiss();

                }
            });
        }

    }

    /*public boolean checkBarcode(String s){

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        ItemModel i = realm.where(ItemModel.class).equalTo("barcode", s).findFirst();

        if(i != null){
            realm.commitTransaction();
            realm.close();

            return true;
        }

        return false;

    }*/

}

