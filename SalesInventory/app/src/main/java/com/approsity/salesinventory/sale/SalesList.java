package com.approsity.salesinventory.sale;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.approsity.salesinventory.NavigationActivity;
import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.models.ItemModel;
import com.approsity.salesinventory.utils.CommonActions;
import com.approsity.salesinventory.utils.CommonObjects;
import com.approsity.salesinventory.utils.FreeActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmResults;


/**
 * Created by Raziuddin.Shaikh on 12/11/2017.
 */

public class SalesList extends Fragment{

    View mView;

    RecyclerView rc;
    SalesAdap adap;
    LinearLayoutManager mLayoutManager;

    public static SaleModel saleObj;
    List<SaleItemModel> saleitemList;

    EditText et_barcode,et_total,et_discount;
    ImageView iv_barcode,iv_clearbarcode;
    Button btn_barcodesearch,btn_preview;

    public  static final int RequestPermissionCode  = 1;
    String[] PERMISSIONS = {Manifest.permission.CAMERA};
    int PERMISSION  = 1;

    Animation shake;

    boolean barcodeCheck;
    public static boolean saleChargedSuccessfully;

    String barcodeTempText="0";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.frag_sales, container, false);

        shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake);

        rc = (RecyclerView) mView.findViewById(R.id.rc);

        et_barcode = (EditText) mView.findViewById(R.id.et_barcode);
        et_discount = (EditText) mView.findViewById(R.id.et_discount);
        et_total = (EditText) mView.findViewById(R.id.et_total);
        et_total.setText("0.0");

        iv_barcode = (ImageView) mView.findViewById(R.id.iv_barcode);
        iv_clearbarcode = (ImageView) mView.findViewById(R.id.iv_clearbarcode);

        btn_barcodesearch = (Button) mView.findViewById(R.id.btn_barcodesearch);
        btn_preview = (Button) mView.findViewById(R.id.btn_preview);

        saleitemList = new ArrayList<>();

        setOperations();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideKeyboard();
            }
        }, 250);

        return mView;
    }

    public void setOperations() {

        iv_barcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
                    requestPermissions(PERMISSIONS, PERMISSION);
                }
                else {

                    barcodeCheck = true;
                    openBarCode();
                }

                /*IntentIntegrator integrator = new IntentIntegrator(getActivity());
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setPrompt("Scan a barcode");
                integrator.setCameraId(0);  // Use a specific camera of the device
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(true);
                integrator.initiateScan();*/
            }
        });

        btn_barcodesearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(TextUtils.isEmpty(et_barcode.getText().toString())) {

                    et_barcode.requestFocus();
                    et_barcode.setError("Please enter barcode");
                    et_barcode.startAnimation(shake);

                }else{

                    barcodeCheck = false;
                    getData();
                }
            }
        });

        btn_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(saleitemList.size() > 0) {
                    RealmList<SaleItemModel> silist = new RealmList<>();
                    silist.addAll(saleitemList);

                    saleObj = new SaleModel(0, silist, null,null, et_discount.getText().toString(), et_total.getText().toString(), "", "", "",calculateProfit(silist),"");

                    Intent i = new Intent(getActivity(), FreeActivity.class);
                    i.putExtra("frag", CommonObjects.OpenFragments.chargesale);
                    getActivity().startActivity(i);

                }else{
                    Snackbar.make(mView,"Atleast add an item",Snackbar.LENGTH_SHORT).show();
                }

            }
        });

        iv_clearbarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                et_barcode.setText("");
            }
        });


        /*et_barcode.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode != KeyEvent.KEYCODE_DEL) {
                    if(et_barcode.getText().toString().length() == 4 || et_barcode.getText().toString().length() == 10 || et_barcode.getText().toString().length() == 16){
                        et_barcode.setText(et_barcode.getText().toString()+"  ");
                        et_barcode.setSelection(et_barcode.getText().length());
                    }

                   // return true;
                }
                return false;
            }
        });*/


        et_discount.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                setTotal();
                discount();
            }
        });

        if(!CommonActions.hasPermissions(getActivity(), PERMISSIONS)){
            requestPermissions(PERMISSIONS, PERMISSION);
        }
    }

    private String calculateProfit(RealmList<SaleItemModel> silist) {

        //// TOTAL PROFIT /////
        BigDecimal totalProfit = BigDecimal.ZERO;
        for(int i=0;i<silist.size();i++){
            totalProfit = totalProfit.add(new BigDecimal(silist.get(i).itemprofit));
        }

        //// TOTAL DISCOUNT /////
        BigDecimal temp;
        if(!TextUtils.isEmpty(et_discount.getText().toString())){

            temp = new BigDecimal(et_discount.getText().toString());
        }
        else
            temp = new BigDecimal("0");

        BigDecimal bdtotal=totalProfit;
        if(!TextUtils.isEmpty(et_discount.getText().toString()) && temp.intValue() > 0){

            BigDecimal bd = totalProfit;
            BigDecimal pct = new BigDecimal(et_discount.getText().toString());

            BigDecimal hundred = new BigDecimal("100");

            BigDecimal bdpercent = pct.divide(hundred);

            BigDecimal bdtip = bd.multiply(bdpercent);
            bdtotal = bd.subtract(bdtip);
        }
        ////////////////////////////////////

        return bdtotal+"";
    }

    private void openBarCode() {

        IntentIntegrator
                .forSupportFragment(SalesList.this)
                .setOrientationLocked(false)
                .setBeepEnabled(false)
                .initiateScan();
    }


    public void getData() {

       /* Realm realm = Realm.getDefaultInstance();
        ItemModel item = realm.where(ItemModel.class).equalTo("barcode",et_barcode.getText().toString()).findFirst();

        if(item != null) {
            ItemModel tempItem = realm.copyFromRealm(item);

            if (Double.parseDouble(tempItem.quantity) > 0) {*/
                Realm.getDefaultInstance().where(ItemModel.class)
                        .equalTo("barcode", et_barcode.getText().toString())
                        .findAllAsync()
                        .addChangeListener(new RealmChangeListener<RealmResults<ItemModel>>() {
                            @Override
                            public void onChange(RealmResults<ItemModel> notiList) {

                                hideKeyboard();

                                boolean qtyCheck = false;
                                if (notiList.size() > 0) {

                                    if (Double.parseDouble(notiList.get(0).quantity) > 0) {

                                        for (int z = 0; z < saleitemList.size(); z++) {
                                            if (saleitemList.get(z).itemList.get(0).id == notiList.get(0).id) {

                                                BigDecimal bd = new BigDecimal(notiList.get(0).quantity).subtract(new BigDecimal(saleitemList.get(z).qty));

                                                if (Double.parseDouble(bd + "") <= 0.0) {

                                                    hideKeyboard();
                                                    Snackbar.make(mView, "Item quantity has finished!", Snackbar.LENGTH_SHORT).show();

                                                    qtyCheck = true;
                                                }
                                            }
                                        }

                                        if (!qtyCheck) {
                                            Long tsLong = System.currentTimeMillis() / 1000;

                                            RealmList<ItemModel> itemlist = new RealmList<>();
                                            itemlist.addAll(notiList);

                                            BigDecimal bd = new BigDecimal(notiList.get(0).saleprice).subtract(new BigDecimal(notiList.get(0).costperunit));

                                            SaleItemModel saleItemObj = new SaleItemModel(Integer.parseInt(tsLong.toString()), itemlist, notiList.get(0).name, new BigDecimal("1.0") + "", notiList.get(0).saleprice, bd + "");

                                            boolean check = false;
                                            for (int i = 0; i < saleitemList.size(); i++) {

                                                if (saleitemList.get(i).itemList.get(0).id == notiList.get(0).id) {

                                                    saleitemList.get(i).qty = new BigDecimal(saleitemList.get(i).qty).add(new BigDecimal(saleItemObj.qty)) + "";

                                                    saleitemList.get(i).price = new BigDecimal(saleitemList.get(i).price).add(new BigDecimal(notiList.get(0).saleprice)) + "";

                                                    saleitemList.get(i).itemprofit = new BigDecimal(saleitemList.get(i).itemprofit).add(new BigDecimal(saleItemObj.itemprofit)) + "";

                                                    check = true;
                                                }
                                            }

                                            if (!check)
                                                saleitemList.add(saleItemObj);

                                            setRc(saleitemList);
                                        } /*else {
                                            hideKeyboard();
                                            Snackbar.make(mView, "No item found!", Snackbar.LENGTH_SHORT).show();
                                        }*/
                                    } else {

                                        hideKeyboard();
                                        Snackbar.make(mView, "Item quantity has finished!", Snackbar.LENGTH_SHORT).show();

                                    }

                                }
                                else {

                                    hideKeyboard();
                                    Snackbar.make(mView, "No item found!", Snackbar.LENGTH_SHORT).show();

                                }
                            }
                        });
            /*} else {
                hideKeyboard();
                Snackbar.make(mView, "Quantity has finished!", Snackbar.LENGTH_SHORT).show();
            }
        }
        else {
            hideKeyboard();
            Snackbar.make(mView, "No item found!", Snackbar.LENGTH_SHORT).show();
        }*/
    }

    public void setRc(List<SaleItemModel> list){
        adap = new SalesAdap(getActivity(), list,true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rc.setLayoutManager(mLayoutManager);
        rc.setAdapter(adap);

        setTotal();
        discount();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(barcodeCheck)
                    openBarCode();
            }
        }, 500);
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    //CommonActions.snackMsgs(mView,"Permission Granted");

                } else {

                    CommonActions.snackMsgs(mView,"Permission Canceled");
                }
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if(result != null) {
            if(result.getContents() == null) {
              //  Snackbar.make(mView,"",Snackbar.LENGTH_SHORT).show();
              //  Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                et_barcode.setText(result.getContents()+"");
                et_barcode.setSelection(et_barcode.getText().length());

                getData();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void hideKeyboard(){
        try {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mView.getWindowToken(), 0);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setTotal(){
        et_total.setText("0.0");

        for(int i=0;i<saleitemList.size();i++){

            BigDecimal bd = new BigDecimal(et_total.getText().toString());
            BigDecimal bd1 = new BigDecimal(saleitemList.get(i).price.toString());

            et_total.setText(bd.add(bd1) + "");
        }
    }

    public void discount(){

        BigDecimal temp;
        if(!TextUtils.isEmpty(et_discount.getText().toString())){

            temp = new BigDecimal(et_discount.getText().toString());
        }
        else
            temp = new BigDecimal("0");

        if(!TextUtils.isEmpty(et_discount.getText().toString()) && temp.intValue() > 0){

            BigDecimal bd = new BigDecimal(et_total.getText().toString());
            BigDecimal pct = new BigDecimal(et_discount.getText().toString());

            BigDecimal hundred = new BigDecimal("100");

            BigDecimal bdpercent = pct.divide(hundred);

            BigDecimal bdtip = bd.multiply(bdpercent);
            BigDecimal bdtotal = bd.subtract(bdtip);

            et_total.setText(bdtotal + "");
        }
        else{
            setTotal();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(saleChargedSuccessfully){

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    et_barcode.setText("");
                    et_discount.setText("");
                    et_total.setText("");

                    saleitemList = new ArrayList<>();
                    setRc(saleitemList);
                }
            }, 1000);

            saleChargedSuccessfully = false;
        }
    }



     /* if (Double.parseDouble(notiList.get(0).quantity) > 0) {

                                        for(int z=0;z<saleitemList.size();z++){

                                            Log.e("ID 1",saleitemList.get(z).itemList.get(0).id+" ");
                                            Log.e("ID 2",notiList.get(0).id+" ");

                                           if(saleitemList.get(z).itemList.get(0).id == notiList.get(0).id){

                                               BigDecimal bd = new BigDecimal(notiList.get(0).quantity).subtract(new BigDecimal(saleitemList.get(z).qty));

                                               Log.e("QTY",saleitemList.get(z).qty+" ");
                                               Log.e("BD",bd+" ");

                                               if(Double.parseDouble(bd+"") < 0.0){

                                                   hideKeyboard();
                                                   Snackbar.make(mView, "Item quantity has finished!", Snackbar.LENGTH_SHORT).show();

                                                   qtyCheck = true;
                                               }
                                           }
                                        }*/

}
