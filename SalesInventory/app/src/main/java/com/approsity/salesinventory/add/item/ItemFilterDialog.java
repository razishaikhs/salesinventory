package com.approsity.salesinventory.add.item;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.models.Category;
import com.approsity.salesinventory.add.models.Product;
import com.approsity.salesinventory.add.models.SubCategory;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;


public class ItemFilterDialog extends DialogFragment implements View.OnClickListener{

    View v;

    List<Category> cList;
    List<SubCategory> scList;
    List<Product> pList;

    Spinner spinner_cat,spinner_subcat,spinner_product;
    EditText et_type,et_size;

    Button btn_add;

    Activity act;

    private onButtonClicked buttonClicked;

    public static ItemFilterDialog newInstance(Activity activity) {

        ItemFilterDialog frag = new ItemFilterDialog();
        frag.act = activity;
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setStyle(0,R.style.DialogTheme);
        setCancelable(true);

    }



    public View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.dialog_item_filter, container, false);

        btn_add = (Button) v.findViewById(R.id.btn_add);
        btn_add.setOnClickListener(this);
        btn_add.setText("Filter");

        et_type = (EditText) v.findViewById(R.id.et_type);
        et_size = (EditText) v.findViewById(R.id.et_size);
        
        spinner_cat = (Spinner) v.findViewById(R.id.spinner_cat);
        spinner_subcat = (Spinner) v.findViewById(R.id.spinner_subcat);
        spinner_product = (Spinner) v.findViewById(R.id.spinner_product);

        setSpinnerData();

        return v;
    }

    public void setSpinnerData() {
        getRealmCategory();
        getRealmSubCategory();
        getRealmProduct();
    }

    public void getRealmCategory() {

        cList = new ArrayList<>();
        RealmResults<Category> c = Realm.getDefaultInstance().where(Category.class).findAllSortedAsync("name",  Sort.ASCENDING);
        cList = Realm.getDefaultInstance().copyFromRealm(c);

        List<String> slist = new ArrayList<>();
        slist.add("No Selection");
        for(int i=0;i<cList.size();i++) {
            slist.add(cList.get(i).name);
        }

        ArrayAdapter<String> spinner_catAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,slist);
        spinner_cat.setAdapter(spinner_catAdap);

    }

    public void getRealmSubCategory() {

        scList = new ArrayList<>();
        RealmResults<SubCategory> sc =  Realm.getDefaultInstance().where(SubCategory.class).findAllSortedAsync("name",  Sort.ASCENDING);
        scList =  Realm.getDefaultInstance().copyFromRealm(sc);

        List<String> sclist = new ArrayList<>();
        sclist.add("No Selection");
        for(int i=0;i<scList.size();i++) {
            sclist.add(scList.get(i).name);
        }

        ArrayAdapter<String> spinner_scatAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,sclist);
        spinner_subcat.setAdapter(spinner_scatAdap);
    }

    public void getRealmProduct() {
        pList = new ArrayList<>();
        RealmResults<Product> pObj = Realm.getDefaultInstance().where(Product.class).findAllSortedAsync("name",  Sort.ASCENDING);
        pList = Realm.getDefaultInstance().copyFromRealm(pObj);

        List<String> plist = new ArrayList<>();
        plist.add("No Selection");
        for(int i=0;i<pList.size();i++) {

            plist.add(pList.get(i).name);
        }

        ArrayAdapter<String> spinner_productAdap = new ArrayAdapter<String>(getActivity(),R.layout.spinner_row,plist);
        spinner_product.setAdapter(spinner_productAdap);

    }

    @Override
    public void onClick(View view) {

        switch(view.getId()) {
            case R.id.btn_add:
                FilterButton();
                break;
            default:
        }
    }

    public void FilterButton() {

        int c,s,p;

        if(spinner_cat.getSelectedItemPosition() == 0)
            c = -1;
        else
            c = cList.get(spinner_cat.getSelectedItemPosition() - 1).getId();

        if(spinner_subcat.getSelectedItemPosition() == 0)
            s = -1;
        else
            s = scList.get(spinner_subcat.getSelectedItemPosition() - 1).getId();


        if(spinner_product.getSelectedItemPosition() == 0)
            p = -1;
        else
            p = pList.get(spinner_product.getSelectedItemPosition() - 1).getId();


        buttonClicked.onClicked(c,s,p,et_type.getText().toString(),et_size.getText().toString());
    }

    public void setButtonClicked(onButtonClicked buttonClicked) {
        this.buttonClicked = buttonClicked;
    }


    public interface onButtonClicked {
        public void onClicked(int catId, int subcatId,int productId,String type, String size);
    }

}

