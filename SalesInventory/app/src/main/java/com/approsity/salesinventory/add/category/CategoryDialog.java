package com.approsity.salesinventory.add.category;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import com.approsity.salesinventory.R;
import com.approsity.salesinventory.add.models.Category;


public class CategoryDialog extends DialogFragment {

    EditText et_name, et_alias, et_desc;
    Button btn_add;
    private onButtonClicked buttonClicked;
    Activity act;

    Animation shake;

    Category Obj;

    public static CategoryDialog newInstance(Activity activity, Category Obj) {

        CategoryDialog frag = new CategoryDialog();
        frag.act = activity;
        frag.Obj = Obj;
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setStyle(STYLE_NO_FRAME, R.style.DialogTheme);
        setCancelable(true);

    }



    public View onCreateView(android.view.LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.dialog_category_add, container, false);

        shake = AnimationUtils.loadAnimation(act, R.anim.shake);

        et_name = (EditText) v.findViewById(R.id.et_name);
        et_alias = (EditText) v.findViewById(R.id.et_alias);
        et_desc = (EditText) v.findViewById(R.id.et_desc);

        btn_add = (Button) v.findViewById(R.id.btn_add);

        if(getTag().equalsIgnoreCase("add")){
            et_name.setText("");
            et_alias.setText("");
            et_desc.setText("");

            btn_add.setText("Add");
        }
        else {

            et_name.setText(Obj.name);
            et_alias.setText(Obj.alias);
            et_desc.setText(Obj.desc);

            btn_add.setText("Update");
        }

        setListener();

        return v;
    }

    private void setListener() {

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(TextUtils.isEmpty(et_name.getText().toString())) {
                    et_name.requestFocus();
                    et_name.setError("Please fill the name");

                    et_name.startAnimation(shake);
                }
                else
                    buttonClicked.onClicked(et_name.getText().toString().toUpperCase(), et_alias.getText().toString().toUpperCase(),et_desc.getText().toString());

            }
        });
    }

    public void setButtonClicked(onButtonClicked buttonClicked) {
        this.buttonClicked = buttonClicked;
    }



    public interface onButtonClicked {
        public void onClicked(String name, String alias,String desc);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (getActivity() != null)
            getActivity().setTheme(R.style.AppTheme);
    }

}

